[[_TOC_]]

# Sudbury

__UPDATE 1/1/2024__: Sudbury is no longer in active development. While the
project may be revived at some point, it should be considered a proof of
concept. Attempts will be made to keep Sudbury updated to the latest Smithay.

A Wayland compositor built on top of [Smithay](https://smithay.github.io/)
designed for ChromeOS.

The name is from a town in Massachusetts. It is west of Wayland (and _wester_ of
Weston).

## Anti-Goals

1. __NOT__: Sudbury is a general purpose desktop compositor.

Sudbury can ultimately remove much general purpose functionality that many
expect. Some such things __may be convenient to keep for development and debug
and so where possible it can be kept behind a feature flag__. Internal
abstractions to support such feature will be squinted at _real hard_. Some
features that would normally appear but do not here:
- DRM/KMS backend exclusively no
  [XWayland](https://wayland.freedesktop.org/xserver.html) or
  [winit](https://rust-windowing.github.io/winit/winit/) support
- systemd/seatd integration.
- Alternate windowing layouts (tiling, etc.)
- [WLR protocols](https://gitlab.freedesktop.org/wlroots/wlr-protocols)
- Multigpu. Chromebooks currently target lower end hardware. Supporting
  multigpu is not required.

2. __NOT__: Sudbury has a lot of rich value-add features that other compositors do
   not have.

Sudbury shall maintain the minimal set of functionality needed which cannot be
added to Smithay directly, for good reason.

3. __NOT__: Sudbury will build a rich ecosystem of its own styles and idioms.

Sudbury leverages Rust and Smithay idioms.

## Features and Implentation Details

### Feature Flags

The following feature flags are directly supported by the build system and can
be enabled with typical [cargo
semantics](https://doc.rust-lang.org/cargo/reference/features.html)

- `chromeos` - Enable support for running within ChromeOS. This (in addition
  using the cros branch) are a required feature for ChromeOS.
- `disable-logging` - Disable all logging for maximum rice. This can yield a
  few percent performance improvements, but leaves no breadcrumbs if there are
  issues. Mostly useful to estimate maximal performance.
- `dumb-allocator` - Use dumb bo for buffers instead of GBM. This option has no
  practical use other than when debugging, or running WLCS.
- `fps` - Emit the instantaneous FPS to console. Generally, tracy profiling can
  get you the same information and so this only has value when fps info is
  desired while maximizing performance. May be deprecated going forward.
- `libinput` - Enable support for libinput. There's no other input support. It
  is useful to disable for WLCS only.
- `libseat` Use libseat support. This should be disabled for ChromeOS, and
  enabled for all other uses.
- `pixman-renderer` - Use pixman for rendering. This option has no practical
  use other than when debugging or running WLCS.
- `profiler` - Enable tracy profiling. This has performance impact but
  generates excellent profiling information.
- `wlcs-stub` - Enable WLCS support. This feature doesn't effect functionality.
  Enabling it may slow down build time. This may be deprecated and always
  enabled in the future.

### Composition Split

Compositors have an implicit policy decision around when they composite and
when they give the signal to clients to resume their render loop. Some timing
diagrams are included in the repository
![here](resources/damage_with_render.svg) and
![here](resources/damage_no_render.svg). A majority of the description for how
this works can be found in the Rustdoc within the project.

This split can be controlled at program start time via the command line
argument `--compositor-split` or the environment variable
`SBRY_COMPOSITION_SPLIT`. Valid values are between 0 and 1. A value of 0.75 for
instance would leave 3/4 of the refresh time available for clients to update,
and 1/4 available for the compositor. Setting this value too high will result
in the compositor not hitting the scanout deadline. Setting this value too low
will possibly increase the client's latency.

### Frame Callbacks

The composition split above is controlled via frame callbacks. Well behaved
Wayland clients will request (`wl_surface::frame`) the compositor to be
informed when is a good time to start on the next frame (`wl_callback::done`).
In the uncomplicated case frame callbacks are sent to all clients which have
submitted buffer updates. Those clients draw the next frame, submit it, and the
process continues. Things get more complicated when the client is fully
occluded. According to the Wayland spec, "A server should avoid signaling the
frame callbacks if the surface is not visible in any way, e.g. the surface is
off-screen, or completely obscured by other opaque surfaces." However, not all
clients behave well with this logic.

To tweak this behavior a command line argument exists.
`--callback-timeout=<ms>` (`env SBRY_FRAME_THROTTLE_MS`) will send frame
callbacks on occluded frames if the amount of the specified amount of time has
elapsed since the last frame callback was sent. The default if nothing is
specified is that frame callbacks will only ever be sent if the surface is
visible. Specifying 0 will send frame callbacks whenever Sudbury sends frame
callbacks (this policy has changed so is specifically not specified here).
Specifying a number greater than 0 will send the callbacks if that number of ms
has elapsed since the last one was sent.

### WLCS Integration

Sudbury includes support for WLCS and it's tightly integrated within the gitlab
CI infrastructure. WLCS will be run by default when pushing branches to gitlab.
If running locally, the same infrastructure may be used. Assuming WLCS is in
the path, simply run `./.gitlab-ci/run-wlcs.sh local`

### Tuning / Debugging

#### Primary Plane Tinting

When a plane is directly scanned out by hardware (either primary or overlay) no
copy happens from the backbuffer to the composited frame. On the other hand,
when a copy happens, a performance penalty is incurred for the case where
direct scan out could have occurred. To visualize when the copy happens, a
green tint is applied as part of the shader for debugging. This can be enabled
by the key combination: `mod4 + shift + t`

#### Tracy Profiling

[`tracy`](https://github.com/wolfpld/tracy) integration is available and
enabled with the `profiler` feature. See the following target from the
project's Makefile.

```make
run-profile: build-profile xfer-profile-build
	$(eval TMPFILE := $(shell mktemp /tmp/XXXXX.tracy))
	(${TRACY_BIN} -f -o ${TMPFILE} -a ${DUT_IP} &)
	ssh -t -p ${DUT_PORT} ${DUT_IP} ${PROFILE_RUN}
	@echo Trace saved to $(TMPFILE)
```

#### RON based config

While Sudbury does support commandline arguments via the
[bpaf](https://docs.rs/bpaf/latest/bpaf/) many configuration options belong in
a config file. The config populates the `SudburyPolicy` structure and may be
observed or modified in src/policy.rs

## TODO

### XDG Popups

XDG popup support is incomplete as of this time. They are the primary mechanism
many clients use for popup needs. Since XDG is not part of core protocol, the
lack of support shouldn't entirely disable clients ability to display, but it
may severly limit. Popup support is currently in progress in #1+
 
### WLCS failures

Find them and fix them.

### Test and stabilize multiple output

While multiple output support has existed since the beginning of development,
it has never been tested and almost ceratinly doesn't work as is.

### Test and stabilize multi-desk

Same as multiple output, except even less attention was paid to supporting this
properly during development. For example, there is no current way to even
change the current desk within Sudbury.

### Implement run time configuration

For debug and configuration (ie. output resize) it will be useful to have a
mechanism to configure the compositor. The policy is already structures in
policy.rs and it should be fairly straightforward to add support for changing
that policy at runtime.

### Utilize blit engine

Blit engines, while not necessarily available on all hardware, provide a low
overhead copy that typically has no side-effects with regard to GPU state.

### Chrome Integration

#### Aura + Augmenter Protocol

Implementing parts of these protocols require modifications to the rendering
pipeline. Currently Sudbury uses the default pipeline. Note that several of
these protocols are likely to conflict with blit engine utilization.

#### Chrome Session Management

The below instructions for running on ChromeOS are hacky. A well defined
mechanism should be implemented to allow Sudbury to run independently of
commandline intervention.

# Build / Run
```shell
# If trying to run from a seatd session, the following should be sufficient:
cargo run --release -F libseat

# To generate a Tracy profile with no messages:
RUST_LOG=info cargo profile-run -- -s /dev/null
```

## ChromeOS deployment

A script is included to make building Sudbury relatively easily:
`cros/sudbury-bootstrap.sh`. Running that in the ChromeOS chroot with nothing
else will do a clean and full build.

### Prerequisites

```shell
mkdir ~/chromiumos/src/third_party/chromiumos-overlay/chromeos-base/sudbury
mkdir ~/chromiumos/src/third_party/rust_crates/projects/platform/sudbury
mkdir ~/chromiumos/src/third_party/rust_crates/projects/platform/smithay
git clone -b cros https://gitlab.freedesktop.org/bwidawsk/sudbury.git ~/chromiumos/src/platform/sudbury
cp ~/chromiumos/src/platform/sudbury/cros/sudbury-9999.ebuild ~/chromiumos/src/third_party/chromiumos-overlay/chromeos-base/sudbury/
cp ~/chromiumos/src/platform/sudbury/cros/Sudbury.toml ~/chromiumos/src/platform/sudbury/Cargo.toml
```

And go through the revendoring steps below.

In order to support local change deployment, the current ebuild is expecting
sources to be under platform, so a clone of gitlab repository needs to be added
there.

Note that it is possible to modify the ebuild to get the sources directly from
git and avoid the local changes, see the following for an example of how this
would be done: [`https://crrev.com/c/4148079`](https://crrev.com/c/4148079)

### Updating dependencies/revendoring

If you changed some dependencies, `rust_crates` will need to be updated for
emerge to work. The following should be done entirely within the chrome chroot.

```shell
#  Update the Sudbury Cargo.toml. For example, using one from gitlab:
curl -o Cargo.toml --output-dir ~/chromiumos/src/third_party/rust_crates/projects/platform/sudbury/ https://gitlab.freedesktop.org/bwidawsk/sudbury/-/raw/cros/cros/Sudbury.toml
#  Update the Smithay Cargo.toml. For example, using one from gitlab:
curl -o Cargo.toml --output-dir ~/chromiumos/src/third_party/rust_crates/projects/platform/smithay/ https://gitlab.freedesktop.org/bwidawsk/sudbury/-/raw/cros/cros/Smithay.toml
#  Add any necessary patches.
mkdir ~/chromiumos/src/third_party/rust_crates/patches/smithay/
curl -O --output-dir  ~/chromiumos/src/third_party/rust_crates/patches/smithay/ https://gitlab.freedesktop.org/bwidawsk/sudbury/-/raw/cros/cros/disable-modifiers2.patch
#  Regenerate the workspace
cd ~/chromiumos/src/third_party/rust_crates/
projects/populate-workspace.py
# Revendor crate dependencies. This will populate ~/.cargo with crate dependencies.
python vendor.py -s drm-ffi -s drm-fourcc -s drm-sys -s gbm-sys -s input-sys -s wayland-backend -s wayland-protocols-misc -s wayland-protocols-wlr
# Use live changes
cros-workon-${BOARD} start dev-rust/third-party-crates-src
# emerge-${BOARD} third-party-crates-src
```

### Regular deployment flow

Once dependencies have been updated a regular `emerge` & `cros deploy --deep`
should work (`--deep` is needed for libinput).

```shell
emerge-${BOARD} sudbury
cros deploy $DUT sudbury
```

### Running Chrome on Sudbury

#### Build it

Follow the [Getting
Started](https://chromium.googlesource.com/chromiumos/docs/+/HEAD/simple_chrome_workflow.md#Getting-started)
and patch chrome with [native display delegate](https://crrev.com/c/3272705).
Enter the [Simple Chrome
environment](https://chromium.googlesource.com/chromiumos/docs/+/HEAD/simple_chrome_workflow.md#Enter-the-Simple-Chrome-environment)

Enable ozone/wayland backend in the `gn args` for the build.

Traditional:
```shell
cros chrome-sdk --board=$BOARD --log-level=info --build-label=Sudbury --gn-extra-args='ozone_platform_wayland=true ozone_platform="wayland" use_wayland_egl=false use_wayland_gbm=true'
```

Shell-less, add the following args:
```shell
target_os = ["chromeos"]
ozone_platform_wayland = "true"
ozone_platform = "wayland"
use_wayland_egl = "false"
use_wayland_gbm = "true"
```
Build and deploy:

```shell
autoninja -C out_$BOARD/Release chrome nacl_helper
deploy_chrome --build-dir=out_$BOARD/Sudbury --device=$DUT --nostartui
```

#### Run it
Start sudbury (must be wayland-0 socket):

```shell
XDG_RUNTIME_DIR=/run/chrome sudbury -w wayland-0
chmod a+w /run/chrome/wayland-0
```

Launch ash chrome:

```shell
XDG_RUNTIME_DIR=/run/chrome session_manager
```

#### Profiling

Smithay and Sudbury have support for [Tracy](https://github.com/wolfpld/tracy).
Tracy does have performance impact, though relatively minimal. Therefore it
needs to be built in. The following patch is all that is needed.

```diff
diff --git a/cros/Sudbury.toml b/cros/Sudbury.toml
index 60e2f2eac835..9388acbf9045 100644
--- a/cros/Sudbury.toml
+++ b/cros/Sudbury.toml
@@ -30,7 +30,7 @@ smithay = { git = "https://github.com/bwidawsk/smithay.git", branch = "reckless"
 fps_ticker = { version = "1.0.0", optional = true }

 [features]
-default = ["libinput", "chromeos"] # Default features should work for ChromeOS
+default = ["libinput", "chromeos", "profiler"] # Default features should work for ChromeOS
 chromeos = ["minigbm"]
 disable-logging = []
 libinput = ["smithay/backend_libinput"]
diff --git a/cros/sudbury-9999.ebuild b/cros/sudbury-9999.ebuild
index e4b09594f37e..47d8f568e757 100644
--- a/cros/sudbury-9999.ebuild
+++ b/cros/sudbury-9999.ebuild
@@ -54,7 +54,7 @@ src_configure() {
 }

 src_compile() {
-       cros-rust_src_compile
+       CXXSTDLIB=c++ cros-rust_src_compile
 }

 src_install() {
 ```

ChromeOS has a firewall that blocks Tracy connections. Tracy wants port 8086 by
default, so first it must be opened

```shell
iptables -A INPUT -p tcp --dport 8086 -j ACCEPT
```

Once Sudbury is running, from another machine, you can start capturing the trace:
```shell
tracy-capture -f -o sudbury.tracy -a $DUT_IP
```

### Running Borealis on Sudbury

Have ash-chrome running.

```shell
# Until fixed, run the ImageLoader
imageloader
```

insert coin from the developer shell: `alt+shift+t`
```shell
insert_coin
```
