# Example client run:
# WAYLAND_DEBUG=1  WESTON_DATA_DIR=/home/bwidawsk/dev/comp/weston/data WAYLAND_DISPLAY=wayland-2 ./weston-simple-egl &
#
# Example to generate a flame graph on the client
# `WESTON_DATA_DIR=/home/bwidawsk/dev/comp/weston/data WAYLAND_DISPLAY=wayland-1 perf record --call-graph dwarf ./weston-simple-egl`
# `perf script | stackcollapse-perf.pl |flamegraph.pl > perf.svg`
#

DUT_IP ?= saberhagen
DUT_PORT ?= 22
TTY ?= tty1
SMITHAY_TTY = SMITHAY_DIRECT_TTY=/dev/${TTY}
TARGET ?= x86_64-unknown-linux-gnu
NORMAL_RUN = ${SMITHAY_TTY} RUST_LOG=error ./sudbury -d card0
DEBUG_RUN = ${SMITHAY_TTY} RUST_BACKTRACE=1 RUST_LOG=trace ./sudbury -d card0
# TODO: Why does RUST_LOG=debug kill tracy?
PROFILE_RUN = ${SMITHAY_TTY} RUST_LOG=info ./sudbury -d card0 --fps -s /dev/null
BUILD_CMD = cargo build --release
DEBUG_BUILD_CMD = cargo build
PROFILE_BUILD_CMD = cargo profile-build
TRACY_BIN ?= /usr/bin/tracy-capture

build-debug:
	${DEBUG_BUILD_CMD}

build-release:
	${BUILD_CMD}

build-profile:
	${PROFILE_BUILD_CMD}

build-memsan:
	RUSTFLAGS='-Zsanitizer=memory -Zsanitizer-memory-track-origins' ${BUILD_CMD} -Zbuild-std --target ${TARGET}

build-asan: FORCE
	RUSTFLAGS=-Zsanitizer=address ${BUILD_CMD} -Zbuild-std --target ${TARGET}

gdb-local: build-debug
	cargo with "rust-gdb --args {bin} {args}" -- run --bin sudbury --

lldb-local: build-debug
	cargo with "rust-lldb -- {bin} {args}" -- run --bin sudbury --

xfer-release-build: FORCE
	rsync --checksum -e "ssh -p ${DUT_PORT}" target/release/sudbury ${DUT_IP}:

xfer-debug-build: FORCE
	rsync --checksum -e "ssh -p ${DUT_PORT}" target/debug/sudbury ${DUT_IP}:

xfer-profile-build: FORCE
	rsync --checksum -e "ssh -p ${DUT_PORT}" target/release-with-debug/sudbury ${DUT_IP}:

xfer-san-build: FORCE
	rsync --checksum -e "ssh -p ${DUT_PORT}" target/${TARGET}/debug/sudbury ${DUT_IP}:

run-local:
	cargo run --release

run-local-debug: build-debug
	cargo run

run-local-flame:
	CARGO_PROFILE_RELEASE_DEBUG=true cargo flamegraph -- -d card0

run: build-release xfer-release-build
	ssh -t -p ${DUT_PORT} ${DUT_IP} ${NORMAL_RUN}

run-debug: build-debug xfer-debug-build
	ssh -t -p "${DUT_PORT}" ${DUT_IP} ${DEBUG_RUN}

run-profile: build-profile xfer-profile-build
	$(eval TMPFILE := $(shell mktemp /tmp/XXXXX.tracy))
	(${TRACY_BIN} -f -o ${TMPFILE} -a ${DUT_IP} &)
	ssh -t -p ${DUT_PORT} ${DUT_IP} ${PROFILE_RUN}
	@echo Trace saved to $(TMPFILE)

# Example: debug input: `make run-Xdebug XDEBUG=sudbury::input`
run-Xdebug: build-debug xfer-debug-build
	ssh -t -p "${DUT_PORT}" ${DUT_IP} RUST_LOG=${XDEBUG} ${NORMAL_RUN}

run-heaptrack: build-release xfer-release-build
	ssh -t -p "${DUT_PORT}" ${DUT_IP} "${SMITHAY_TTY}" heaptrack ./sudbury -d card0

run-memsan: build-memsan xfer-san-build run-debug

run-asan: build-asan xfer-debug-build
	ssh -t -p "${DUT_PORT}" ${DUT_IP} RUST_LOG=debug ASAN_OPTIONS=detect_leaks=1,detect_stack_use_after_return=1 ${SMITHAY_TTY} ./sudbury -d card0

run-malloc-debug: build-debug xfer-debug-build
	ssh -t -p "${DUT_PORT}" ${DUT_IP} MALLOC_CHECK_=3 RUST_LOG=debug ${SMITHAY_TTY} ./sudbury -d card0

FORCE:
