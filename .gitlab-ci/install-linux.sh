#!/usr/bin/env bash

LINUX_VER=6.6.6

# Build a Linux kernel for use in testing. We enable the VKMS module so we can
# predictably test the DRM backend in the absence of real hardware. We lock the
# version here so we see predictable results.
curl -O https://cdn.kernel.org/pub/linux/kernel/v6.x/linux-"$LINUX_VER".tar.xz
tar xaf linux-"$LINUX_VER".tar.xz
mv linux-"$LINUX_VER" linux
cd linux || exit

vng --kconfig
./scripts/config \
	--disable CONFIG_DEBUG_MM \
	--enable CONFIG_DRM \
	--enable CONFIG_DRM_KMS_HELPER \
	--enable CONFIG_DRM_KMS_FB_HELPER \
	--enable CONFIG_DRM_VKMS

make -j"${FDO_CI_CONCURRENT:-8}" ARCH="${LINUX_ARCH}" "${LLVM_BUILD}"
#vng --build
