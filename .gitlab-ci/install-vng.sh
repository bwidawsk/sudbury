#!/usr/bin/env bash

VIRTME_VER=1.15
VIRTME_INIT_VER=af4cfe12036168c6909e41d5c1e494d3cb795ca2

curl -L https://github.com/arighi/virtme-ng/archive/refs/tags/v${VIRTME_VER}.tar.gz -o vng.tar.gz
tar xaf vng.tar.gz
mv virtme-ng-${VIRTME_VER} /virtme-ng
cd /virtme-ng || exit
curl -L https://github.com/arighi/virtme-ng-init/archive/${VIRTME_INIT_VER}.tar.gz -o vngi.tar.gz
tar -xaf ./vngi.tar.gz -C virtme_ng_init --strip-components=1

pip install argcomplete
activate-global-python-argcomplete
BUILD_VIRTME_NG_INIT=1 pip3 install --verbose -r requirements.txt .
