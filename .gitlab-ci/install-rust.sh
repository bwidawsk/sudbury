#!/usr/bin/env bash

set -e -o xtrace -o errexit
export CARGO_HOME='/usr/local/cargo'

RUSTUP_VERSION=1.26.0
RUST_VERSION=$1
RUST_ARCH="${BUILD_ARCH}-unknown-linux-gnu"

RUSTUP_URL=https://static.rust-lang.org/rustup/archive/$RUSTUP_VERSION/$RUST_ARCH/rustup-init
curl -O $RUSTUP_URL

chmod +x rustup-init
#/tmp/clone/rustup-init -v -y --no-modify-path --profile minimal --default-toolchain $RUST_VERSION;
curl https://sh.rustup.rs -sSf | sh -s -- -y
rm rustup-init
chmod -R a+w $RUSTUP_HOME $CARGO_HOME

source "/usr/local/cargo/env"

rustup toolchain install $RUST_VERSION
rustup default $RUST_VERSION

rustup --version
cargo --version
rustc --version

curl -L --proto '=https' --tlsv1.2 -sSf https://raw.githubusercontent.com/cargo-bins/cargo-binstall/main/install-from-binstall-release.sh | bash

cargo binstall -y --force cargo-tarpaulin
cargo binstall -y --force cargo-hack
cargo binstall -y --force grcov

rustup component add rustfmt
rustup component add clippy
rustup component add llvm-tools

if [ "$RUST_VERSION" = "nightly" ]; then
	rustup component add rustfmt --toolchain nightly

	# Documentation tools
	cargo binstall --force rustdoc-stripper
fi
