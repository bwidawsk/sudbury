#!/bin/bash
#

# Find all tests that are failures but not skips
function real_fails() {
	local json=${1-$(ls -t report* | head -1)}

	jq '.testsuites[].testsuite[] | select(has("failures")) | select(has("wlcs-skip-test") != true ) | "\(.classname).\(.name)"' "$json"
}

# Find all passing tests
function real_pass() {
	local json=${1-$(ls -t report* | head -1)}

	jq '.testsuites[].testsuite[] | select(has("failures") != true) | "\(.classname).\(.name)"' "$json"
}

# Find test classes where all subtests fail
function all_fails() {
	local json=${1-$(ls -t report* | head -1)}

	jq '.testsuites[] | select(.tests == (.failures + .errors)).testsuite[] | select(has("wlcs-skip-test") == false).classname' "$json" | uniq
}
#
# Find test classes where nothing passed
function no_pass() {
	local json=${1-$(ls -t report* | head -1)}

	jq '.testsuites[] | select(.tests == (.failures + .disabled + .errors)).testsuite[] | select(has("wlcs-skip-test") == false).classname' "$json" | uniq
}

# Example find touch panics:
# $(find_panic wlcs.txt "panic in touch")
function find_panic() {
	local output="${1-wlcs.txt}"
	local panic_str="${2-panic in}"
	tac "$output" | grep -Pzo "$panic_str(?:.*\n)*?\K\[ RUN      \] .*"  | sed 's/\[ RUN      \] /\n/g'
}


# When run from virtme, this script runs with a shared mapping in /vng-sudbury/.
function execute_wlcs() {
	local wlcs_path=${1:-/vng-sudbury}
	local o_path=${2:-/vng-sudbury}
	local ci_run=${3:-true}

	expected_fail_classes_list expected_fail_classes
	expected_fail_list expected_fails
	fail_list fails


	if [ "$ci_run" == "true" ]; then
		mkdir -p /tmp/tests
		chmod -R 0700 /tmp
		export HOME=/root
		export PATH=/usr/local/cargo/bin/:/usr/local/bin/:/usr/libexec/wlcs/:$PATH
		export XDG_RUNTIME_DIR=/tmp

		# run the tests and save the exit status
		echo 0x1f >/sys/module/drm/parameters/debug
		dmesg &>/vng-sudbury/dmesg.log
		export SBRY_DRM_DEV=/dev/dri/card0
		export GTEST_OUTPUT="xml:${o_path}/report.xml"
	else
		when="$(/usr/bin/date --iso=seconds)"
		report="$o_path/report-$when.json"
		export SBRY_DRM_DEV=/dev/dri/by-path/platform-vkms-card
		export GTEST_OUTPUT="json:$report"
	fi

	export LLVM_PROFILE_FILE="${o_path}/%p-%m.profraw"
	wlcs "${wlcs_path}"/libwlcs.so --gtest_filter="-${expected_fail_classes}:${expected_fails}:${fails}" >"${o_path}"/wlcs.txt 2>&1
	TEST_RES=$?

	if [ "$ci_run" == "true" ]; then
		echo 0x00 >/sys/module/drm/parameters/debug
	fi

	# create a file to keep the result of this script:
	echo "WLCS exit status: ${TEST_RES}"
	echo $TEST_RES >"${o_path}"/exit_status

	if [ "$ci_run" == "true" ]; then
		# shutdown virtme
		sync
		poweroff -f
	fi
}

function expected_fail_classes_list() {
	declare -n f=$1

	# Popups (https://wayland.app/protocols/xdg-shell#xdg_popup) are
	# currently unimplemented:
	#	XdgPopupTest"
	#	"XdgPopupStable/XdgPopupTest"
	#	"Default/XdgPopupPositionerTest"
	#	"Anchor/XdgPopupPositionerTest"
	#	"Gravity/XdgPopupPositionerTest"
	#	"AnchorRect/XdgPopupPositionerTest"
	#	"ConstraintAdjustmentNone/XdgPopupPositionerTest"
	#	"ConstraintAdjustmentSlide/XdgPopupPositionerTest"
	#	"ConstraintAdjustmentFlip/XdgPopupPositionerTest"
	#	"ConstraintAdjustmentResize/XdgPopupPositionerTest"
	f="*XdgPopupTest*"
	f="${f}:*/XdgPopupPositionerTest*"

	# When popups are re-enabled:
	# See https://github.com/MirServer/mir/issues/2324
	#XdgPopupStable/XdgPopupTest.grabbed_popup_gets_keyboard_focus/0
	# Not yet implemented, see https://github.com/MirServer/mir/issues/2861
	#XdgPopupTest.when_parent_surface_is_moved_a_reactive_popup_is_moved
}

function expected_fail_list() {
	declare -n f=$1

	# These fail because popups and text input are missing, but there is
	# also a MIR bug on them: https://github.com/MirServer/mir/issues/2324
	f="${f}TextInputV3WithInputMethodV2Test.text_input_does_not_enter_non_grabbing_popup"
	f="${f}:TextInputV3WithInputMethodV2Test.text_input_enters_grabbing_popup"

	# Touch is unimplemented
	f="${f}:AllSurfaceTypes/TouchTest.*"
	#AllSurfaceTypes/TouchTest.sends_touch_up_on_surface_destroy/4 # Fixed by https://github.com/MirServer/wlcs/pull/199
	#AllSurfaceTypes/TouchTest.sends_touch_up_on_surface_destroy/5 # Fixed by https://github.com/MirServer/wlcs/pull/199
	f="${f}:ClippedLargerRegion/RegionSurfaceInputCombinations.input_inside_region_seen/1"
	f="${f}:ClippedLargerRegion/RegionSurfaceInputCombinations.input_inside_region_seen/3"
	f="${f}:ClippedLargerRegion/RegionSurfaceInputCombinations.input_inside_region_seen/5"
	f="${f}:ClippedLargerRegion/RegionSurfaceInputCombinations.input_inside_region_seen/7"
	f="${f}:ClippedLargerRegion/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/1"
	f="${f}:ClippedLargerRegion/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/3"
	f="${f}:ClippedLargerRegion/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/5"
	f="${f}:ClippedLargerRegion/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/7"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_inside_region_seen/5"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_inside_region_seen/7"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_inside_region_seen/9"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_inside_region_seen/11"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_inside_region_seen/17"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_inside_region_seen/19"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_inside_region_seen/21"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_inside_region_seen/23"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_inside_region_seen/29"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_inside_region_seen/31"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_inside_region_seen/33"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_inside_region_seen/35"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_inside_region_seen/41"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_inside_region_seen/43"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_inside_region_seen/45"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_inside_region_seen/47"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/5"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/7"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/9"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/11"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/17"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/19"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/21"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/23"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/29"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/31"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/33"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/35"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/41"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/43"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/45"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/47"
	f="${f}:FullSurface/RegionSurfaceInputCombinations.input_inside_region_seen/1"
	f="${f}:FullSurface/RegionSurfaceInputCombinations.input_inside_region_seen/3"
	f="${f}:FullSurface/RegionSurfaceInputCombinations.input_inside_region_seen/5"
	f="${f}:FullSurface/RegionSurfaceInputCombinations.input_inside_region_seen/7"
	f="${f}:FullSurface/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/1"
	f="${f}:FullSurface/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/3"
	f="${f}:FullSurface/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/5"
	f="${f}:FullSurface/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/7"
	f="${f}:MultiRectCorners/RegionSurfaceInputCombinations.input_inside_region_seen/1"
	f="${f}:MultiRectCorners/RegionSurfaceInputCombinations.input_inside_region_seen/3"
	f="${f}:MultiRectCorners/RegionSurfaceInputCombinations.input_inside_region_seen/5"
	f="${f}:MultiRectCorners/RegionSurfaceInputCombinations.input_inside_region_seen/7"
	f="${f}:MultiRectCorners/RegionSurfaceInputCombinations.input_inside_region_seen/9"
	f="${f}:MultiRectCorners/RegionSurfaceInputCombinations.input_inside_region_seen/11"
	f="${f}:MultiRectCorners/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/1"
	f="${f}:MultiRectCorners/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/3"
	f="${f}:MultiRectCorners/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/5"
	f="${f}:MultiRectCorners/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/7"
	f="${f}:MultiRectCorners/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/9"
	f="${f}:MultiRectCorners/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/11"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/5"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/7"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/9"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/11"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/17"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/19"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/21"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/23"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/29"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/31"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/33"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/35"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/41"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/43"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/45"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/47"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/53"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/55"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/57"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_inside_region_seen/59"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/5"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/7"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/9"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/11"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/17"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/19"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/21"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/23"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/29"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/31"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/33"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/35"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/41"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/43"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/45"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/47"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/53"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/55"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/57"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/59"
	f="${f}:SmallerRegion/RegionSurfaceInputCombinations.input_inside_region_seen/1"
	f="${f}:SmallerRegion/RegionSurfaceInputCombinations.input_inside_region_seen/3"
	f="${f}:SmallerRegion/RegionSurfaceInputCombinations.input_inside_region_seen/5"
	f="${f}:SmallerRegion/RegionSurfaceInputCombinations.input_inside_region_seen/7"
	f="${f}:SmallerRegion/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/1"
	f="${f}:SmallerRegion/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/3"
	f="${f}:SmallerRegion/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/5"
	f="${f}:SmallerRegion/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/7"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_falls_through_subsurface_when_parent_unmapped/5"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_falls_through_subsurface_when_parent_unmapped/7"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_falls_through_subsurface_when_parent_unmapped/9"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_falls_through_subsurface_when_parent_unmapped/11"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_falls_through_subsurface_when_unmapped/5"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_falls_through_subsurface_when_unmapped/7"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_falls_through_subsurface_when_unmapped/9"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_falls_through_subsurface_when_unmapped/11"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_hits_parent_after_falling_through_subsurface/5"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_hits_parent_after_falling_through_subsurface/7"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_hits_parent_after_falling_through_subsurface/9"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_hits_parent_after_falling_through_subsurface/11"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_not_seen_in_region_after_null_buffer_committed/5"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_not_seen_in_region_after_null_buffer_committed/7"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_not_seen_in_region_after_null_buffer_committed/9"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_not_seen_in_region_after_null_buffer_committed/11"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_not_seen_in_surface_without_region_after_null_buffer_committed/5"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_not_seen_in_surface_without_region_after_null_buffer_committed/7"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_not_seen_in_surface_without_region_after_null_buffer_committed/9"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_not_seen_in_surface_without_region_after_null_buffer_committed/11"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_not_seen_over_empty_region/5"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_not_seen_over_empty_region/7"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_not_seen_over_empty_region/9"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_not_seen_over_empty_region/11"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_after_dragged_off_surface/5"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_after_dragged_off_surface/7"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_after_dragged_off_surface/9"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_after_dragged_off_surface/11"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_after_surface_unmapped_and_remapped/5"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_after_surface_unmapped_and_remapped/7"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_after_surface_unmapped_and_remapped/9"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_after_surface_unmapped_and_remapped/11"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_by_second_surface_after_drag_off_first_and_up/5"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_by_second_surface_after_drag_off_first_and_up/9"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_by_second_surface_after_drag_off_first_and_up/11"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_by_subsurface_after_parent_unmapped_and_remapped/5"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_by_subsurface_after_parent_unmapped_and_remapped/7"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_by_subsurface_after_parent_unmapped_and_remapped/9"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_by_subsurface_after_parent_unmapped_and_remapped/11"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.unmapping_parent_stops_subsurface_getting_input/5"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.unmapping_parent_stops_subsurface_getting_input/7"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.unmapping_parent_stops_subsurface_getting_input/9"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.unmapping_parent_stops_subsurface_getting_input/11"
	f="${f}:ToplevelInputRegions/ToplevelInputCombinations.input_falls_through_surface_without_region_after_null_buffer_committed/5"
	f="${f}:XdgToplevelStableTest.touch_can_not_steal_pointer_based_move"
	f="${f}:XdgToplevelStableTest.touch_respects_window_geom_offset"

	# MIR skips these. Some are duplicates above. Debug at lower priority
	# https://github.com/MirServer/mir/blob/main/tests/acceptance-tests/wayland/CMakeLists.txt
	f="${f}:TouchInputSubsurfaces/SubsurfaceMultilevelTest.subsurface_moves_after_both_sync_parent_and_grandparent_commit/0"
	f="${f}:TouchInputSubsurfaces/SubsurfaceMultilevelTest.subsurface_with_desync_parent_moves_when_only_parent_committed/0"
	f="${f}:TouchInputSubsurfaces/SubsurfaceTest.desync_subsurface_moves_when_only_parent_committed/0"
	f="${f}:TouchInputSubsurfaces/SubsurfaceTest.one_subsurface_to_another_fallthrough/0"
	f="${f}:TouchInputSubsurfaces/SubsurfaceTest.place_above_simple/0"
	f="${f}:TouchInputSubsurfaces/SubsurfaceTest.place_below_simple/0"
	f="${f}:TouchInputSubsurfaces/SubsurfaceTest.pointer_input_correctly_offset_for_subsurface/0"
	f="${f}:TouchInputSubsurfaces/SubsurfaceTest.subsurface_does_not_move_when_parent_not_committed/0"
	f="${f}:TouchInputSubsurfaces/SubsurfaceTest.subsurface_extends_parent_input_region/0"
	f="${f}:TouchInputSubsurfaces/SubsurfaceTest.subsurface_moves_out_from_under_input_device/0"
	f="${f}:TouchInputSubsurfaces/SubsurfaceTest.subsurface_moves_under_input_device_once/0"
	f="${f}:TouchInputSubsurfaces/SubsurfaceTest.subsurface_moves_under_input_device_twice/0"
	f="${f}:TouchInputSubsurfaces/SubsurfaceTest.sync_subsurface_moves_when_only_parent_committed/0"
}

function fail_list() {
	declare -n f=$1

	f="${f}ClientSurfaceEventsTest.surface_moves_over_surface_under_pointer"
	f="${f}:ClientSurfaceEventsTest.surface_moves_under_pointer"
	f="${f}:ClientSurfaceEventsTest.surface_moves_while_under_pointer"
	f="${f}:ClientSurfaceEventsTest.surface_resizes_under_pointer"
	f="${f}:CopyCutPaste.given_sink_has_focus_when_source_makes_offer_sink_sees_offer"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/8"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/10"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/20"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/22"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/32"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/34"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/44"
	f="${f}:DefaultEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/46"
	f="${f}:MultiRectCorners/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/0"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/8"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/10"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/20"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/22"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/32"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/34"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/44"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/46"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/56"
	f="${f}:MultiRectEdges/RegionSurfaceInputCombinations.input_not_seen_after_leaving_region/58"
	f="${f}:PrimarySelection.sink_can_listen"
	f="${f}:PrimarySelection.sink_can_request"
	f="${f}:PrimarySelection.source_can_supply_request"
	f="${f}:PrimarySelection.source_sees_request"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_after_dragged_off_surface/4"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_after_dragged_off_surface/6"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_after_dragged_off_surface/8"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_after_dragged_off_surface/10"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_by_second_surface_after_drag_off_first_and_up/4"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_by_second_surface_after_drag_off_first_and_up/6"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_by_second_surface_after_drag_off_first_and_up/7"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_by_second_surface_after_drag_off_first_and_up/8"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_by_second_surface_after_drag_off_first_and_up/10"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_by_subsurface_after_parent_unmapped_and_remapped/8"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_by_subsurface_after_parent_unmapped_and_remapped/10"
	f="${f}:TextInputV3WithInputMethodV2Test.input_method_can_be_disabled"
	f="${f}:TextInputV3WithInputMethodV2Test.input_method_can_be_enabled"
	f="${f}:TextInputV3WithInputMethodV2Test.input_method_disabled_when_text_input_destroyed"
	f="${f}:TextInputV3WithInputMethodV2Test.text_field_state_can_be_set"
	f="${f}:TextInputV3WithInputMethodV2Test.text_input_enters_parent_surface_after_child_destroyed"
	f="${f}:TextInputV3WithInputMethodV2Test.text_input_enters_surface_on_focus"
	f="${f}:TextInputV3WithInputMethodV2Test.text_input_leaves_surface_on_unfocus"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceMultilevelTest.by_default_subsurface_is_sync/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceMultilevelTest.subsurface_can_be_set_to_sync/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceMultilevelTest.subsurface_does_not_move_when_grandparent_commit_is_before_sync_parent_commit/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceMultilevelTest.subsurface_with_desync_parent_does_not_move_when_only_grandparent_committed/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceMultilevelTest.subsurface_with_sync_parent_does_not_move_when_only_grandparent_committed/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceMultilevelTest.subsurface_with_sync_parent_does_not_move_when_only_parent_committed/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceTest.gets_input_over_surface_with_empty_region/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceTest.input_falls_through_empty_subsurface_input_region/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceTest.subsurface_moves_out_from_under_input_device/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceTest.subsurface_moves_under_input_device_once/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceTest.subsurface_moves_under_input_device_twice/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceTest.subsurface_of_a_subsurface_handled/0"
	f="${f}:XdgToplevelStableConfigurationTest.window_can_maximize_itself"
	f="${f}:XdgToplevelStableConfigurationTest.window_can_unfullscreen_itself"
	f="${f}:XdgToplevelStableConfigurationTest.window_can_unmaximize_itself"
	f="${f}:XdgToplevelStableTest.surface_can_be_moved_interactively"
	f="${f}:XdgToplevelStableTest.surface_can_be_resized_interactively"
	f="${f}:XdgToplevelStableTest.when_parent_is_set_to_child_descendant_error_is_raised"
	f="${f}:XdgToplevelStableTest.when_parent_is_set_to_self_error_is_raised"
	f="${f}:XdgToplevelStableTest.wm_capabilities_are_sent"

	# MIR skips these. Debug at lower priority
	# https://github.com/MirServer/mir/blob/main/tests/acceptance-tests/wayland/CMakeLists.txt
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceMultilevelTest.subsurface_moves_after_both_sync_parent_and_grandparent_commit/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceMultilevelTest.subsurface_with_desync_parent_moves_when_only_parent_committed/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceTest.desync_subsurface_moves_when_only_parent_committed/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceTest.one_subsurface_to_another_fallthrough/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceTest.place_above_simple/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceTest.place_below_simple/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceTest.place_above_below/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceTest.pointer_input_correctly_offset_for_subsurface/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceTest.subsurface_extends_parent_input_region/0"
	f="${f}:XdgShellStableSubsurfaces/SubsurfaceTest.sync_subsurface_moves_when_only_parent_committed/0"
	f="${f}:XdgSurfaceStableTest.attaching_buffer_to_unconfigured_xdg_surface_is_an_error"
	f="${f}:XdgSurfaceStableTest.creating_xdg_surface_from_wl_surface_with_attached_buffer_is_an_error"
	f="${f}:XdgSurfaceStableTest.creating_xdg_surface_from_wl_surface_with_committed_buffer_is_an_error"
	f="${f}:XdgSurfaceStableTest.creating_xdg_surface_from_wl_surface_with_existing_role_is_an_error"
	f="${f}:XdgToplevelStableTest.pointer_leaves_surface_during_interactive_move"
	f="${f}:XdgToplevelStableTest.pointer_leaves_surface_during_interactive_resize"

	# Fails in VM, not locally
	f="${f}:XdgToplevelStableConfigurationTest.activated_state_follows_pointer"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_after_surface_unmapped_and_remapped/8"
	f="${f}:SurfaceInputRegions/SurfaceInputCombinations.input_seen_after_surface_unmapped_and_remapped/10"
}

if [[ ${BASH_SOURCE[0]} == "$0" ]]; then
	if [ "$1" == "local" ]; then
		execute_wlcs ./target/debug/ ./ false
	else
		execute_wlcs
	fi
fi
