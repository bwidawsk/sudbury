#!/usr/bin/env bash

# Constructs the base container image used to Sudbury Weston within CI. Any
# changes in this file must bump the $FDO_DISTRIBUTION_TAG variable so we know
# the container has to be rebuilt.
#
# Based off of Weston's `debian-install.sh`

set -o xtrace -o errexit

function bootstrap_container() {
	export EPHEMERAL=(
		bc
		bison
		bzip2
		cargo
		cmake
		diffutils
		findutils
		flex
		make
		meson
		perl
		py3-pip
		py3-setuptools
		py3-virtualenv
	)

	export DEPS=(
		bash
		boost-dev
		ccache
		clang-dev
		coreutils
		curl
		elfutils-dev
		eudev-dev
		file
		gcc
		g++
		git
		gtest-dev
		jq
		libinput-dev
		libressl-dev
		libseat-dev
		libxkbcommon-dev
		libxml2-utils
		linux-headers
		lld
		llvm17-dev
		mesa
		mesa-dbg
		mesa-dev
		mesa-gbm
		mesa-dri-gallium
		pixman
		pixman-dev
		python3-dev
		qemu-system-aarch64
		qemu-system-x86_64
		virtiofsd
		wayland-dev
		wayland-protocols
		zlib
	)
	apk add "${DEPS[@]}" "${EPHEMERAL[@]}"
}

function setup_env() {
	ccache --show-stats

	#TODO: Write why this is needed
	export RUSTFLAGS="-C target-feature=-crt-static"

	export CCACHE_COMPILERCHECK=content
	export CCACHE_COMPRESS=true
	export CCACHE_DIR=/cache/$CI_PROJECT_NAME/ccache
	export PATH=/usr/lib/ccache/bin/:$PATH

	# The below is the CMake not understanding ccache
	export CC="/usr/lib/ccache/bin/gcc"
	export CXX="/usr/lib/ccache/bin/g++"

	if [[ "$BUILD_ARCH" = "x86_64" ]]; then
		export LINUX_ARCH=x86
		export	KERNEL_DEFCONFIG=x86_64_defconfig
		export LLVM_BUILD=
	elif [[ "$BUILD_ARCH" = "aarch64" ]]; then
		export LINUX_ARCH=arm64
		export KERNEL_DEFCONFIG=defconfig
		LLVM_BUILD="CC=clang
		LLVM_IAS=1 LD=ld.lld NM=llvm-nm AR=llvm-ar STRIP=llvm-strip
		OBJCOPY=llvm-objcopy OBJDUMP=llvm-objdump OBJSIZE=llvm-size READELF=llvm-readelf
		HOSTCC=clang HOSTCXX=clang++ HOSTAR=llvm-ar HOSTLD=ld.lld"
		# LLVM is broken
		export LLVM_BUILD=
	else
		echo "Invalid or missing \$BUILD_ARCH ($BUILD_ARCH)"
		exit 1
	fi

	virtualenv /venv
	source /venv/bin/activate
	pip install --upgrade pip
}

function build_other() {
	pip install yq

	./.gitlab-ci/install-rust.sh 1.73.0

	./.gitlab-ci/install-vng.sh
	./.gitlab-ci/install-linux.sh
	./.gitlab-ci/install-wlcs.sh
}

function populate_virtme() {
	mkdir /sudbury-virtme
	mkdir -p /root/.cache/mesa_shader_cache
	mv linux/arch/"${LINUX_ARCH}"/boot/"${KERNEL_IMAGE}" /sudbury-virtme/sImage
	mv linux/.config /sudbury-virtme/config
	rm -rf linux
}

function cleanup() {
	deactivate
	rm -rf /.cargo
	rm -rf ./linux*
	rm ./*tar.gz
	apk del "${EPHEMERAL[@]}"
}

bootstrap_container
setup_env
build_other
populate_virtme
cleanup
