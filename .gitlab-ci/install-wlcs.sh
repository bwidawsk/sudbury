#!/bin/sh

set -e -o xtrace -o errexit

WLCS_SHA=2a9809e5d038935784b8421766f0bb58fd326bc9
mkdir wlcs
cd wlcs
git init
git remote add origin https://github.com/MirServer/wlcs.git
git fetch origin "${WLCS_SHA}"
git reset --hard FETCH_HEAD
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr -DCMAKE_INSTALL_LIBDIR="lib/" -DWLCS_BUILD_ASAN=False -DWLCS_BUILD_TSAN=False -DWLCS_BUILD_UBSAN=False -G Ninja ..
ninja
ninja install
cd
rm -rf wlcs
