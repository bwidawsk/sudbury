#!/bin/sh

set -eu

# Override BRANCH variable to use a different branch
BRANCH=${BRANCH:=cros}
# Override UPSTREAM variable to use a different repo
UPSTREAM=${UPSTREAM:=https://gitlab.freedesktop.org/bwidawsk/sudbury}

CROS_RUST_SUDBURY="$HOME"/chromiumos/src/third_party/rust_crates/projects/platform/sudbury
readonly CROS_RUST_SUDBURY
CROS_RUST_SMITHAY="$HOME"/chromiumos/src/third_party/rust_crates/projects/platform/smithay
readonly CROS_RUST_SMITHAY
CROS_PLATFORM_SUDBURY="$HOME"/chromiumos/src/platform/sudbury
readonly CROS_PLATFORM_SUDBURY

# This will fail if BOARD is not set
echo "${BOARD:?BOARD}" > /dev/null

mkdirs()
{
	mkdir -p "$CROS_RUST_SUDBURY"
	mkdir -p "$CROS_RUST_SMITHAY"
	mkdir -p "$CROS_PLATFORM_SUDBURY"
	mkdir "$HOME"/chromiumos/src/third_party/chromiumos-overlay/chromeos-base/sudbury
	mkdir "$HOME"/chromiumos/src/third_party/rust_crates/patches/smithay/
}

clean()
{
	rm -rf "$CROS_RUST_SUDBURY"
	rm -rf "$CROS_RUST_SMITHAY"
	rm -rf "$CROS_PLATFORM_SUDBURY"
	rm -rf "$HOME"/chromiumos/src/third_party/chromiumos-overlay/chromeos-base/sudbury
	rm -rf "$HOME"/chromiumos/src/third_party/rust_crates/patches/smithay/

	# Remove all vendor stuff
	rm -rf /build/"$BOARD"/use/lib/cros_rust_registry

	# Remove sudbury incremental files
	rm -rf /build/"$BOARD"/tmp/portage/chromeos-base/sudbury-9999
}

fetch()
{
	git clone -b "$BRANCH" "$UPSTREAM".git "$CROS_PLATFORM_SUDBURY"

	# Put files in place. This will eventually be a CL
	cp "$CROS_PLATFORM_SUDBURY"/cros/Sudbury.toml "$CROS_RUST_SUDBURY"/Cargo.toml
	cp "$CROS_PLATFORM_SUDBURY"/cros/Smithay.toml "$CROS_RUST_SMITHAY"/Cargo.toml
	cp "$CROS_PLATFORM_SUDBURY"/cros/sudbury-9999.ebuild "$HOME"/chromiumos/src/third_party/chromiumos-overlay/chromeos-base/sudbury/
	cp "$CROS_PLATFORM_SUDBURY"/cros/disable-modifiers2.patch "$HOME"/chromiumos/src/third_party/rust_crates/patches/smithay/

	# Use the correct Cargo.toml for the build
	cp "$CROS_PLATFORM_SUDBURY"/cros/Sudbury.toml "$CROS_PLATFORM_SUDBURY"/Cargo.toml
}

vendor()
{
	cd "$HOME"/chromiumos/src/third_party/rust_crates/ || exit
	projects/populate-workspace.py

	# FIXME: Vendoring won't succeed until audits are performed. This is
	# not great because if vendoring actually fails (not just audits), the
	# script won't catch it.
	python vendor.py -s drm-ffi \
		-s drm-fourcc \
		-s drm-sys \
		-s gbm-sys \
		-s input-sys \
		-s wayland-backend \
		-s wayland-protocols-misc \
		-s wayland-protocols-wlr || true
	cd - || exit
}

sudbury_rust_env()
{
	cros-workon-"$BOARD" start dev-rust/third-party-crates-src
	emerge-"$BOARD" third-party-crates-src
}

sudbury_rust_unenv()
{
	cros-workon-"$BOARD" stop dev-rust/third-party-crates-src
}

if [ "$(basename -- "$0")" = "sudbury-bootstrap.sh" ]; then
	sudbury_rust_unenv
	clean

	mkdirs
	fetch
	vendor

	sudbury_rust_env
	emerge-"$BOARD" sudbury
fi
