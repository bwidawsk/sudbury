# Copyright 2022 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CROS_WORKON_LOCALNAME="platform/sudbury"
CROS_WORKON_PROJECT="chromiumos/platform/sudbury"
CROS_WORKON_EGIT_BRANCH="cros"
CROS_RUST_SUBDIR="sudbury"
#CROS_WORON_SUBDIRS_TO_COPY=("${CROS_RUST_SUBDIR}" .cargo)
#CROS_WORKON_SUBTREE="${CROS_WORKON_SUBDIRS_TO_COPY[*]}"

inherit git-r3 cros-workon cros-rust

DESCRIPTION="ChromeOS System Compositor"
HOMEPAGE="https://gitlab.freedesktop.org/bwidawsk/sudbury/"
#EGIT_REPO_URI="https://gitlab.freedesktop.org/bwidawsk/sudbury.git"
#EGIT_BRANCH="cros"

S="${WORKDIR}/sudbury-${PV}"
EGIT_CHECKOUT_DIR=${S}

LICENSE="MIT"
SLOT="0"
KEYWORDS="*"

COMMON_DEPEND="
	media-libs/minigbm:=
	x11-libs/libdrm:=
	x11-libs/libxkbcommon:=
	dev-libs/expat:=
	dev-libs/libevdev:=
	dev-libs/libinput:=
	dev-libs/wayland:=
	sys-fs/udev:="

DEPEND="${COMMON_DEPEND} dev-rust/third-party-crates-src:="

RDEPEND="${COMMON_DEPEND} virtual/opengles"

src_unpack() {
	#git-r3_src_unpack
	cros-workon_src_unpack
	cros-rust_src_unpack
}

src_prepare() {
	# This needs to be called manually because of unpacking?
	cros-rust_src_prepare
}

src_configure() {
	# This needs to be called manually because of unpacking?
	cros-rust_src_configure
}

src_compile() {
	cros-rust_src_compile
}

src_install() {
	local build_dir="$(cros-rust_get_build_dir)"

	dobin "${build_dir}/sudbury"
}
