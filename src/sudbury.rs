//! Sudbury compositor for Chrome OS

use std::{
    cell::RefCell,
    collections::HashMap,
    fs::{create_dir_all, OpenOptions},
    os::unix::net::UnixStream,
    rc::Rc,
    sync::{Arc, Mutex, OnceLock},
    time::Duration,
};

use smithay::{
    desktop::{PopupManager, Space, Window},
    input::{Seat, SeatState},
    output::Scale,
    reexports::{
        calloop::{self, generic::Generic, Interest, LoopHandle, LoopSignal, PostAction},
        wayland_server::DisplayHandle,
    },
    utils::{Clock, Monotonic},
    wayland::{
        compositor::CompositorState,
        input_method::InputMethodManagerState,
        output::OutputManagerState,
        presentation::PresentationState,
        relative_pointer::RelativePointerManagerState,
        selection::{data_device::DataDeviceState, primary_selection::PrimarySelectionState},
        shell::xdg::XdgShellState,
        shm::ShmState,
        socket::ListeningSocketSource,
        text_input::TextInputManagerState,
        viewporter::ViewporterState,
        virtual_keyboard::VirtualKeyboardManagerState,
    },
};

use std::fmt::Debug;
use tracing::{debug, debug_span, error, trace};
use wayland_server::{backend::ObjectId, Client, Display};

use super::args::SudburyArgs;
use crate::{
    drm_kms::SudburyDrmKms,
    input::{SudburyAction, SudburyInput},
    policy::feed_policy,
    shell::SudburyWindow,
    wayland::client::WaylandClient,
    wayland::external_interfaces::croscomp::CroscompState,
};

thread_local!(pub static MONO_CLOCK: Clock<Monotonic> = Clock::new());

#[derive(Clone, Debug, Default)]
pub struct OutputScales(pub HashMap<String, f32>);

impl std::fmt::Display for OutputScales {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.0)
    }
}

#[derive(Copy, Clone, Debug, Default)]
pub struct ClearColor {
    pub r: f32,
    pub g: f32,
    pub b: f32,
    pub a: f32,
}

impl std::fmt::Display for ClearColor {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {} {} {} (ARGB8)", self.a, self.r, self.g, self.b)
    }
}

impl From<&ClearColor> for [f32; 4] {
    fn from(value: &ClearColor) -> Self {
        [value.r, value.g, value.b, value.a]
    }
}

static mut CLIENT_LOCK: Mutex<Option<Client>> = Mutex::new(None);
/// # Safety
///
/// This function references a static mut, so must be unsafe
pub unsafe fn last_client() -> Option<Client> {
    CLIENT_LOCK.lock().unwrap().take()
}

/// Default clear color for new surfaces.
static CLEAR_COLOR: OnceLock<ClearColor> = OnceLock::new();
pub fn clear_color() -> [f32; 4] {
    <[f32; 4]>::from(CLEAR_COLOR.get().unwrap())
}

static FRAME_THROTTLE: OnceLock<Option<Duration>> = OnceLock::new();
pub fn frame_throttle() -> Option<Duration> {
    *FRAME_THROTTLE.get().unwrap()
}

static COMPOSITION_SPLIT: OnceLock<f32> = OnceLock::new();
pub fn composition_split() -> f32 {
    *COMPOSITION_SPLIT.get().unwrap()
}

static SCALING: OnceLock<Option<OutputScales>> = OnceLock::new();
pub fn scale_override() -> bool {
    SCALING.get().unwrap().is_some()
}
pub fn output_scale(output: &str) -> Option<Scale> {
    let Some(scales) = SCALING.get().unwrap() else {
        return None;
    };
    let scale = scales.0.get(output)?;
    Some(match scale.fract() == 0f32 {
        true => Scale::Integer(*scale as i32),
        false => Scale::Custom {
            advertised_integer: scale.round() as i32,
            fractional: *scale as f64,
        },
    })
}

/// Static global state for Sudbury, the binary.
///
/// Calloop state is the true top level state of this application. All events are dispatched via this
/// structure and the connection to the main [`Display`] cannot be severed.
#[derive(Debug)]
pub struct GlobalState {
    pub dh: DisplayHandle,
    pub sudbury: Sudbury,
}

pub fn attach_input(sudbury: &mut Sudbury, input: SudburyInput) {
    let input = Rc::new(RefCell::new(input));
    sudbury.input = Some(Rc::clone(&input));
    sudbury.drm_kms.set_input(Rc::clone(&input));
}

/// Static global state for the compositor.
#[derive(Debug)]
pub struct Sudbury {
    /// A handle to the global `display_handle`
    ///
    /// Since [`GlobalState`] owns the actual [`Display`], the handle is kept for all display
    /// related things. Display in this context means the [Wayland global singleton](https://wayland.freedesktop.org/docs/html/apa.html#protocol-spec-wl_display).
    /// This display handle is cheaply cloneable.
    display_handle: DisplayHandle,

    /// Handle do the main Calloop
    /// [LoopHandle](https://docs.rs/calloop/latest/calloop/struct.LoopHandle.html). The EventLoop
    /// handle is cheaply cloneable.
    eventloop_handle: (LoopHandle<'static, GlobalState>, LoopSignal),

    /// The Wayland socket being used by the compositor.
    socket: String,

    /// Represents a Chrome [desk](https://support.google.com/chromebook/answer/9594869)
    ///
    /// A [`Space`] is an abstract concept representing just a 2d space of pixels.
    ///
    /// Some examples:
    /// 1: provide a traditional desktop with no extra workspaces
    /// N: provide a traditional desktop where each space is its own workspace.
    /// M x N: provide a tiled desktop with M windows/tiles over N workspaces
    ///
    /// Sudbury opts for the middle example from above.
    pub spaces: Vec<Space<Window>>,

    /// Popup manager
    pub popups: PopupManager,

    /// Windows maintained by Sudbury's window manager
    ///
    /// Smithay's notion of a Window implies there is an XDG Toplevel, and so this is implicit here
    /// as well.
    pub windows: HashMap<ObjectId, SudburyWindow>,
    pub active_window: Option<Window>,

    /// Sudbury only ever has one [`Seat`]. This is it.
    ///
    /// The seat arbitrates access over client focus and is used to obtain handles to the keyboard
    /// and mouse pointer.
    pub seat: Seat<Sudbury>,

    /// Rendering, modesetting, and other related functionality
    ///
    /// Currently DRM/KMS only supports GLES/Egl rendering.
    pub drm_kms: SudburyDrmKms,

    /// The entity that handles inputs
    pub input: Option<Rc<RefCell<SudburyInput>>>,

    // State maintained by Smithay goes below here.
    pub seat_state: SeatState<Sudbury>,

    /// Everything needs Shm
    pub shm_state: ShmState,

    /// Mandatory compositor state
    ///
    /// See [`core_interfaces::compositor`]
    pub comp_state: CompositorState,

    /// Utilize the `xdg_shell` protocol
    ///
    /// See [`stable_interfaces::xdg_shell`]
    pub xdg_state: XdgShellState,

    ///
    ///
    ///
    pub data_device_state: DataDeviceState,

    /// Utilize the Primary Selection protocol
    ///
    /// https://wayland.app/protocols/primary-selection-unstable-v1
    pub primary_selection_state: PrimarySelectionState,
}

impl Sudbury {
    /// Initialize the Wayland socket
    ///
    /// # Errors
    ///
    /// This function will return an error if socket creation fails.

    fn init_socket(path: Option<&str>) -> anyhow::Result<(ListeningSocketSource, String)> {
        let source = match path {
            Some(p) => ListeningSocketSource::with_name(p)?,
            None => ListeningSocketSource::new_auto()?,
        };

        let name = source.socket_name().to_string_lossy().into_owned();

        debug!(socket = name);

        Ok((source, name))
    }

    /// Returns the current space number of this [`Sudbury`].
    pub fn current_space_no(&self) -> usize {
        *self.seat.user_data().get::<usize>().unwrap_or(&0)
    }

    /// Returns a reference to the current space of this [`Sudbury`].
    ///
    /// Operation is technically infallible but in case of issue, the default (0th) space will be
    /// returned. Sudbury policy is such that only one space can be active at a time.
    pub fn current_space(&self) -> &Space<Window> {
        &self.spaces[self.current_space_no()]
    }

    pub fn current_space_mut(&mut self) -> &mut Space<Window> {
        let no = self.current_space_no();
        &mut self.spaces[no]
    }

    fn init_args(args: &SudburyArgs) {
        FRAME_THROTTLE.get_or_init(|| args.callback_timeout.map(Duration::from_millis));

        COMPOSITION_SPLIT.get_or_init(|| args.compositor_split);

        CLEAR_COLOR.get_or_init(|| args.clear_color);

        SCALING.get_or_init(|| {
            if args.scaling.is_some() {
                error!("Scaling argument is deprecated");
            }
            args.scaling.clone().map(|scaling| {
                let mut copy_scale = HashMap::new();
                copy_scale.clear();
                copy_scale.extend(scaling.0);
                OutputScales(copy_scale)
            })
        });

        let _ = create_dir_all(args.sudbury_cfg.parent().unwrap());
        let file = OpenOptions::new()
            .read(true)
            .open(args.sudbury_cfg.clone())
            .ok();
        feed_policy(file)
    }

    /// Create a new Sudbury compositor.
    ///
    /// Returns None if critical initialization fails.
    pub fn new(
        dh: DisplayHandle,
        eventloop: (LoopHandle<'static, GlobalState>, LoopSignal),
        args: &SudburyArgs,
        pixman: bool,
    ) -> Self {
        Self::init_args(args);
        // TODO: Should socket creation happen last?
        let (sock, socket) = match Self::init_socket(args.wayland_display.as_deref()) {
            Ok((so, sok)) => (so, sok),
            Err(e) => {
                panic!("Failed to create Wayland socket {e}");
            }
        };

        let dh = &dh;
        let comp_state = CompositorState::new::<Self>(dh);
        let _croscomp = CroscompState::new(dh);
        let data_device_state = DataDeviceState::new::<Self>(dh);
        InputMethodManagerState::new::<Self, _>(dh, |_| true);
        let _output_manager_state = OutputManagerState::new_with_xdg_output::<Self>(dh);
        let _pres_feed = PresentationState::new::<Self>(dh, Clock::<Monotonic>::new().id() as u32);
        let primary_selection_state = PrimarySelectionState::new::<Self>(dh);
        let mut seat_state = SeatState::new();
        let shm_state = ShmState::new::<Self>(dh, vec![]);
        RelativePointerManagerState::new::<Self>(dh);
        TextInputManagerState::new::<Self>(dh);
        let _vp_state = ViewporterState::new::<Self>(dh);
        VirtualKeyboardManagerState::new::<Self, _>(dh, |_client| true);
        let xdg_state = XdgShellState::new::<Self>(dh);

        let seat = seat_state.new_wl_seat(dh, "big nickel");

        // Set the current space for the seat.
        assert!(seat.user_data().insert_if_missing(|| 0_usize));

        // Add Wayland socket connection events to the event loop
        if let Err(e) = eventloop
            .0
            .insert_source(sock, Self::handle_socket_connection)
        {
            panic!("Couldn't create socket handler {e}");
        }

        // Always starts up with one space, for now.
        let mut first_space = Space::default();

        let drm_kms = SudburyDrmKms::new(
            dh,
            &eventloop.0,
            args.drm_dev.clone().into(),
            &mut first_space,
            pixman,
        );

        Self {
            eventloop_handle: eventloop,
            display_handle: dh.clone(),
            socket,

            spaces: vec![first_space],
            popups: PopupManager::default(),
            windows: HashMap::default(),
            active_window: Default::default(),

            seat,
            drm_kms,
            input: None,

            shm_state,
            comp_state,
            xdg_state,
            data_device_state,
            seat_state,
            primary_selection_state,
        }
    }

    /// Callback for socket connections
    ///
    /// Wayland protocol is all socket based. The only use of this function is to add the new
    /// Wayland client to the Smithay Display
    pub fn handle_socket_connection(event: UnixStream, _meta: &mut (), data: &mut GlobalState) {
        trace!("Socket connection");
        // Add the new client to the display server
        match data
            .dh
            .insert_client(event, Arc::new(WaylandClient::default()))
        {
            Ok(client) => {
                unsafe {
                    CLIENT_LOCK.lock().unwrap().replace(client);
                };
            }
            Err(e) => error!("Failed to add new client {e}"),
        }
    }

    /// Returns a reference to the EventLoop handle of this [`Sudbury`].
    pub fn eventloop_handle(&self) -> &LoopHandle<'static, GlobalState> {
        &self.eventloop_handle.0
    }

    /// Returns a reference to the EventLoop handle of this [`Sudbury`].
    pub fn eventloop_signal(&self) -> &LoopSignal {
        &self.eventloop_handle.1
    }

    pub fn display_handle(&self) -> &DisplayHandle {
        &self.display_handle
    }

    pub fn socket(&self) -> &str {
        self.socket.as_ref()
    }

    pub fn init_dispatch(&self, display: Display<Sudbury>) {
        if let Err(e) = self.eventloop_handle().insert_source(
            Generic::new(display, Interest::READ, calloop::Mode::Level),
            |_, display, data| {
                profiling::scope!("dispatch_clients");
                debug_span!("client_dispatch");
                trace!("Dispatching client requests");

                // `dispatch_clients`() will block if there are no messages
                unsafe {
                    display.get_mut().dispatch_clients(&mut data.sudbury)?;
                }

                Ok(PostAction::Continue)
            },
        ) {
            panic!("Couldn't initialize handler for wayland event loop {e}");
        }
    }

    #[profiling::function]
    pub(crate) fn compositor_action(&mut self, action: SudburyAction) {
        match action {
            SudburyAction::Inaction => (),
            SudburyAction::Exit => self.eventloop_signal().stop(),
            SudburyAction::CycleFocus => todo!(),
            SudburyAction::Tint => {
                _ = self
                    .eventloop_handle()
                    .insert_idle(|data| data.sudbury.drm_kms.tint())
            }
            SudburyAction::FullscreenToggle => self.toggle_fullscreen_current(),
        }
    }

    pub fn now_us(&self) -> u128 {
        MONO_CLOCK.with(|clock| Duration::from(clock.now()).as_micros())
    }
}
