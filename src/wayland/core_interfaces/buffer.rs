use smithay::wayland::buffer::BufferHandler;

use crate::sudbury::Sudbury;

impl BufferHandler for Sudbury {
    fn buffer_destroyed(
        &mut self,
        _buffer: &smithay::reexports::wayland_server::protocol::wl_buffer::WlBuffer,
    ) {
    }
}
