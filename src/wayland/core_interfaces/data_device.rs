use smithay::{
    delegate_data_device,
    wayland::selection::{
        data_device::{
            ClientDndGrabHandler, DataDeviceHandler, DataDeviceState, ServerDndGrabHandler,
        },
        SelectionHandler,
    },
};

use crate::sudbury::Sudbury;

impl DataDeviceHandler for Sudbury {
    fn data_device_state(&self) -> &DataDeviceState {
        &self.data_device_state
    }
}

impl SelectionHandler for Sudbury {
    type SelectionUserData = ();
}

impl ClientDndGrabHandler for Sudbury {}
impl ServerDndGrabHandler for Sudbury {}

delegate_data_device!(Sudbury);
