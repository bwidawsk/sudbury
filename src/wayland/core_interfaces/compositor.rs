use std::io::ErrorKind;

use smithay::{
    backend::renderer::utils::on_commit_buffer_handler,
    delegate_compositor,
    input::pointer::RelativeMotionEvent,
    output::Output,
    reexports::calloop::{Error, Interest},
    utils::{Logical, Point, SERIAL_COUNTER},
    wayland::{
        compositor::{
            add_blocker, add_pre_commit_hook, get_parent, is_sync_subsurface, with_states,
            BufferAssignment, CompositorClientState, CompositorHandler, CompositorState,
            SurfaceAttributes,
        },
        dmabuf::get_dmabuf,
    },
};
use tracing::{debug, error, instrument};
use wayland_server::{protocol::wl_surface::WlSurface, Client, DisplayHandle, Resource};

use crate::{
    policy::focus_new_windows,
    shell::{ResizeTarget, SudburySurfaceState},
    sudbury::Sudbury,
    wayland::{
        client::WaylandClient,
        stable_interfaces::xdg_shell::{xdg_clear_first_ack, xdg_initial_configure},
    },
};

impl Sudbury {
    fn precommit(&mut self, dh: &DisplayHandle, surface: &WlSurface) {
        let Some(dmabuf) = with_states(surface, |data| {
            data.cached_state
                .pending::<SurfaceAttributes>()
                .buffer
                .as_ref()
                .and_then(|ass| match ass {
                    BufferAssignment::Removed => None,
                    BufferAssignment::NewBuffer(b) => get_dmabuf(b).ok(),
                })
        }) else {
            return;
        };
        // Create a sync point on the dmabuf fd.
        let (fence, source) = match dmabuf.generate_blocker(Interest::READ) {
            Ok((blocker, source)) => (blocker, source),
            Err(_) => {
                // No need to wait
                return;
            }
        };
        let client = surface.client().unwrap();
        let dh = dh.clone();
        if let Err(e) = self
            .eventloop_handle()
            .insert_source(source, move |_, _, data| {
                data.sudbury
                    .client_compositor_state(&client)
                    .blocker_cleared(&mut data.sudbury, &dh);
                Ok(())
            })
        {
            if let Error::IoError(io) = e.error {
                // A buffer can be reused and there is no need to generate a new blocker because
                // the fd will only become readable when all work is complete.
                if io.kind() != ErrorKind::AlreadyExists {
                    error!("{io:?}")
                }
            } else {
                error!("{e:?}")
            }
        } else {
            add_blocker(surface, fence)
        }
    }
}

impl CompositorHandler for Sudbury {
    fn compositor_state(&mut self) -> &mut CompositorState {
        &mut self.comp_state
    }

    fn client_compositor_state<'a>(&self, client: &'a Client) -> &'a CompositorClientState {
        &client
            .get_data::<WaylandClient>()
            .expect("Missing client data")
            .compositor_state
    }

    fn new_surface(&mut self, surface: &WlSurface) {
        // This copies the way that Anvil handles implicit synchronization for composition without
        // blocking using the pre-commit hook.
        //
        // For blocking on dmabufs, new attached dmabufs would become the current buffer returned
        // by Smithay's abstractions on commit. So you would not render the old buffer, while the
        // new dmabuf is not yet ready (like you want to), but try to render the new buffer, which
        // will block the render command until it is ready to be used. The pre-commit hook allows
        // attaching the blocker to the current buffer to block on since state is management
        // atomically and merged at the commit dispatch.
        //
        // Extra context: Because all these updates are meant to be made atomically all together,
        // you can't just hold back the buffer, but you need to block the whole state. (e.g.
        // buffers dictate size of a surface, which clients might want to update in lock step with
        // geometry or viewporter attributes.) (So if you render an old buffer, you really need to
        // render the complete old state.)

        add_pre_commit_hook(surface, Self::precommit);
    }

    #[instrument(level = "trace", skip(self), fields(surface = %surface.id()))]
    #[profiling::function]
    fn commit(&mut self, surface: &WlSurface) {
        let (no_buf, removed_buf, new_buf) = with_states(surface, |data| {
            let buf = &data.cached_state.current::<SurfaceAttributes>().buffer;
            (
                buf.is_none(),
                matches!(buf, Some(BufferAssignment::Removed)),
                matches!(buf, Some(BufferAssignment::NewBuffer(_))),
            )
        });
        debug!("Commit: Buffer unattached={no_buf} removed={removed_buf} new={new_buf}");

        on_commit_buffer_handler::<Self>(surface);

        // The following not only progresses the state machine, but it is the best opportunity to
        // infer proper semantics with buffer attachment and removal.
        match self.get_surface_state(surface) {
            SudburySurfaceState::Plastic => {
                // This should be the first commit a client makes. It should have no attached
                // buffer.
                if let Some(window) =
                    self.find_window_with_state(surface, SudburySurfaceState::Plastic)
                {
                    if new_buf {
                        debug!("Poorly behaved Wayland client (commit with buffer before configure). Not XDG?");
                    }

                    let window = window.clone(); // Needed because of immutable borrow

                    self.set_surface_state(surface, SudburySurfaceState::InitialCommitted, false);

                    // This will find the first commit for an XDG surface
                    // Orphaned windows need to be mapped into the Space. XDG should not yet have attached
                    // a buffer, so it will look fine. If however, XDG protocol isn't being followed, or
                    // there is a bug, and there is a buffer attached, graphical glitches may occur.
                    let (pos, output): (Point<i32, Logical>, Output) =
                        if let Some(resize_info) = self.get_window_resize(&window) {
                            match resize_info {
                                ResizeTarget::Fullscreen(f) => f.upgrade().map_or_else(
                                    || {
                                        (
                                            Point::<i32, Logical>::from((0, 0)),
                                            self.pointer_output_or_default().clone(),
                                        )
                                    },
                                    |out| (out.current_location(), out),
                                ),
                                ResizeTarget::Windowed(w) => {
                                    (*w, self.pointer_output_or_default().clone())
                                }
                            }
                        } else {
                            (
                                self.input
                                    .as_ref()
                                    .map(|i| i.as_ref().borrow().pointer.loc().0.to_i32_round())
                                    .unwrap_or(Point::<i32, Logical>::from((0, 0))),
                                self.pointer_output_or_default().clone(),
                            )
                        };

                    // If this is the first commit, the server is expected to send a configure event so
                    // that subsequent commits are made aware of things like full screen, desired size, max
                    // extents, etc.
                    //
                    //  https://wayland.app/protocols/xdg-shell#xdg_surface
                    //
                    //  "After creating a role-specific object and setting it up, the client must perform
                    //  an initial commit without any buffer attached. The compositor will reply with an
                    //  `xdg_surface`.configure event. The client must acknowledge it and is then allowed to
                    //  attach a buffer to map the surface."
                    //
                    // TODO: What about non-XDG windows?
                    // TODO: Sending the output for bounds isn't what we should be doing for
                    // multi-monitor setups.
                    let serial = xdg_initial_configure(
                        &window,
                        output.preferred_mode().unwrap().size.to_logical(1),
                    );
                    self.set_surface_state(
                        surface,
                        SudburySurfaceState::ConfigureSent(serial),
                        false,
                    );
                    self.place_window(window.clone(), pos);
                }
                // If can't find window, it's not XDG
            }
            SudburySurfaceState::InitialCommitted => {
                // There are two possibilities, either the client is sending an empty
                // commit again for reasons unknown, or, the client is attempting to attach
                // a buffer before the configure handshake has taken place.
                //
                // Nothing to do.
                debug!("Poorly behaved Wayland client (second commit before configure). Not XDG?");
            }
            SudburySurfaceState::ConfigureSent(_) => {
                // "When a configure event is received, if a client commits the surface in
                // response to the configure event, then the client must make an ack_configure
                // request sometime before the commit request, passing along the serial of the
                // configure event."
                //
                // This is an error, but so what?
                debug!("Poorly behaved Wayland client (commit issued before ack). Not XDG?");
            }
            SudburySurfaceState::ConfigureAcked => {
                if new_buf {
                    self.set_surface_state(surface, SudburySurfaceState::BufferAttached, false);
                    if focus_new_windows() && xdg_clear_first_ack(surface) {
                        let window = self.to_window(surface).clone();
                        self.activate_window(Some(&window));

                        // Set keyboard focus to this new window
                        self.seat.get_keyboard().unwrap().set_focus(
                            self,
                            Some(window.clone().into()),
                            SERIAL_COUNTER.next_serial(),
                        );

                        // FIXME: Is this necessary
                        //let prev_loc = self.seat.get_pointer().unwrap().current_location();
                        self.seat.get_pointer().unwrap().relative_motion(
                            self,
                            Some((window.into(), (0, 0).into())),
                            &RelativeMotionEvent {
                                delta: (0.0, 0.0).into(),
                                delta_unaccel: (0.0, 0.0).into(),
                                utime: self.now_us() as u64,
                            },
                        );
                    }
                }
            }
            SudburySurfaceState::BufferAttached => {
                if no_buf || removed_buf {
                    self.set_surface_state(surface, SudburySurfaceState::ConfigureAcked, false);
                } else {
                    // Here a new buffer is being attached.
                    self.finish_resize(surface);
                }
            }
        }

        if self.popups.find_popup(surface).is_some() {
            todo!("Handle popups")
        }

        if !is_sync_subsurface(surface) {
            let mut root = surface.clone();
            while let Some(parent) = get_parent(&root) {
                root = parent;
            }
            if let Some(window) = self.find_window(&root) {
                window.0.on_commit();
            }
        }

        if let Some(_window) = self.find_window(surface) {
            for space in &self.spaces {
                /* FIXME: The following should work but returns an empty element list... It
                 * shouldn't be too bad performance to iterate over all the outputs since the
                 * DrmCompositor will track damage.
                   let window = window.0.clone();
                space
                    .outputs_for_element(&window)
                    .iter()
                    .for_each(|o| self.drm_kms.schedule_idle_composition(o));
                */
                space
                    .outputs()
                    .for_each(|o| self.drm_kms.schedule_idle_composition(o));
            }
        }
    }
}
delegate_compositor!(Sudbury);
