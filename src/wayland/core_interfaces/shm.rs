use smithay::{delegate_shm, wayland::shm::ShmHandler};

use crate::sudbury::Sudbury;

impl ShmHandler for Sudbury {
    fn shm_state(&self) -> &smithay::wayland::shm::ShmState {
        &self.shm_state
    }
}
delegate_shm!(Sudbury);
