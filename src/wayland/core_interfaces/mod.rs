//! Wayland core interfaces
//!
//! This module (mostly submodules) handle the core Wayland interface support.
//! [Smithay](https://smithay.github.io/smithay/smithay/index.html) itself handles a majority of
//! the core Wayland protocol implementation. What remains are callbacks that are implemented in
//! the following submodules.

// `wl_display`
// `wl_registry`
// `wl_callback`
mod buffer; //`wl_output`
            // This is pub only for Rustdoc.
pub mod compositor; //`wl_compositor`
                    // wl_shm_pool
mod shm; // wl_shm // `wl_buffer`
         // wl_data_offer
mod data_device;
// wl_data_device_manager
// "deprecated" `wl_shell`
// "deprecated" `wl_shell_surface`
// `wl_surface`
mod seat; // `wl_seat`
          // `wl_pointer`
          // `wl_keyboard`
          // `wl_touch`
mod output; // `wl_output`
            // `wl_region`
            // `wl_subcompositor`
            // `wl_subsurface`
