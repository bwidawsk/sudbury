use smithay::delegate_presentation;

use crate::sudbury::Sudbury;

delegate_presentation!(Sudbury);
