use smithay::delegate_viewporter;

use crate::sudbury::Sudbury;

delegate_viewporter!(Sudbury);
