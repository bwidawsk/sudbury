use crate::{
    shell::{grab::SurfaceMov, SudburySurfaceState},
    sudbury::Sudbury,
};
use smithay::{
    delegate_xdg_shell,
    desktop::Window,
    input::{
        pointer::{self, Focus, PointerHandle},
        Seat,
    },
    output::Output,
    reexports::{
        wayland_protocols::xdg::shell::server::xdg_toplevel,
        wayland_server::protocol::wl_seat::WlSeat,
    },
    utils::{Logical, Serial, Size},
    wayland::{
        compositor::with_states,
        seat::WaylandFocus,
        shell::xdg::{
            self, PopupSurface, PositionerState, ToplevelSurface, XdgShellHandler, XdgShellState,
            XdgToplevelSurfaceData, XdgToplevelSurfaceRoleAttributes,
        },
    },
};
use std::{cell::RefCell, sync::Mutex};
use tracing::{debug, error, instrument, trace};
use wayland_server::protocol::{wl_output::WlOutput, wl_surface::WlSurface};
use wayland_server::Resource;

#[derive(Clone, Default)]
struct FirstAck(bool);

impl FirstAck {
    pub fn clear(&mut self) -> bool {
        let ret = self.0;
        self.0 = false;
        ret
    }
}

impl Sudbury {
    pub fn title(&self, surface: &WlSurface) -> String {
        with_states(surface, |states| {
            states
                .data_map
                .get::<XdgToplevelSurfaceData>()
                .unwrap()
                .lock()
                .unwrap()
                .title
                .clone()
                .unwrap_or_default()
        })
    }

    #[allow(dead_code)]
    pub fn app_id(&self, surface: &WlSurface) -> String {
        with_states(surface, |states| {
            states
                .data_map
                .get::<XdgToplevelSurfaceData>()
                .unwrap()
                .lock()
                .unwrap()
                .app_id
                .clone()
                .unwrap_or_default()
        })
    }
}

impl XdgShellHandler for Sudbury {
    fn xdg_shell_state(&mut self) -> &mut XdgShellState {
        &mut self.xdg_state
    }

    fn new_toplevel(&mut self, surface: ToplevelSurface) {
        let id = surface.wl_surface().id();
        let window = Window::new(surface);
        self.windows.insert(id, Sudbury::create_window(window));
    }

    fn new_popup(&mut self, _surface: PopupSurface, _positioner: PositionerState) {
        todo!()
    }

    fn move_request(&mut self, toplevel: ToplevelSurface, wlseat: WlSeat, serial: Serial) {
        let Some((start_data, _, pointer)) = check_grab(&wlseat, toplevel.wl_surface(), serial)
        else {
            return;
        };

        let window = self.to_window(toplevel.wl_surface());

        // TODO: panic if Window is fullscreen
        // TODO: unmaximize first if needed

        let pos = self
            .current_space()
            .element_location(self.to_window(toplevel.wl_surface()))
            .unwrap();

        let smove = SurfaceMov::new(
            start_data,
            window.clone(),
            self.pointer_output_or_default(),
            pos,
        );

        // Grab will be unset when the button is released.
        pointer.set_grab(self, smove, serial, Focus::Clear);
    }

    fn grab(&mut self, _surface: PopupSurface, _seat: WlSeat, _serial: Serial) {
        todo!()
    }

    // Client request to fullscreen the surface on the given output.
    //
    // The output is optional, and None allows the compositor to choose.
    fn fullscreen_request(&mut self, surface: xdg::ToplevelSurface, output: Option<WlOutput>) {
        // Client is allowed to pass NULL for output which means the compositor chooses.
        let output: Option<Output> = match &output {
            Some(o) => Output::from_resource(o),
            None => self.current_space().outputs().last().cloned(),
        };

        let Some(output) = output else {
            error!("Failed to find output {output:?}");
            return;
        };

        self.fullscreen(surface, &output);
    }

    fn unfullscreen_request(&mut self, surface: xdg::ToplevelSurface) {
        self.unfullscreen(&surface);
    }

    #[instrument(level = "trace", skip(self))]
    fn ack_configure(&mut self, surface: WlSurface, configure: xdg::Configure) {
        let prev_state = self.get_surface_state(&surface);

        let SudburySurfaceState::ConfigureSent(serial) = prev_state else {
            debug!("Lost track of serial?");
            return;
        };
        let xdg::Configure::Toplevel(configure) = configure else {
            debug!("No configure?");
            return;
        };

        with_states(&surface, |states| {
            states
                .data_map
                .insert_if_missing(|| RefCell::new(FirstAck(true)))
        });

        let window = self
            .find_window(&surface)
            .expect("Missing window during resize")
            .0
            .clone();

        let state = window.toplevel().current_state();
        let (is_resizing, to_fullscreen) = match (
            state.states.contains(xdg_toplevel::State::Resizing),
            state.states.contains(xdg_toplevel::State::Fullscreen),
            configure
                .state
                .states
                .contains(xdg_toplevel::State::Resizing),
            configure
                .state
                .states
                .contains(xdg_toplevel::State::Fullscreen),
        ) {
            // Breaking out these states is useful for gdb debugging
            (false, false, false, false) => {
                trace!("No resize");
                (false, false)
            }
            (_, false, _, true) => {
                trace!("Acked fullscreen transition");
                (false, true)
            }
            (_, true, _, true) => {
                trace!("Staying in fullscreen");
                (false, true)
            }
            (_, true, _, false) => {
                trace!("Acking unfullscreen");
                (true, false)
            }
            (false, _, true, _) => {
                trace!("Acking resize");
                (true, false)
            }
            (true, _, true, _) => {
                trace!("Still resizing");
                (true, false)
            }
            (cr, cf, pr, pf) => {
                error!("Unexpected pattern {cr} {cf} {pr} {pf}");
                (false, false)
            }
        };

        // In a simple case, every ConfigureSent is matched by ConfigureAcked. However, if Sudbury
        // sends multiple Configures, it will receive multiple acks where only the one whose serial
        // number is high enough.
        if configure.serial >= serial {
            self.set_surface_state(&surface, SudburySurfaceState::ConfigureAcked, false);
            if is_resizing || to_fullscreen {
                self.ack_window_resize(&window);
                debug_assert_eq!(
                    configure.serial, serial,
                    "Sudbury lost track of configure event serial {0:?} {serial:?}",
                    configure.serial
                );
            }
        }
    }

    fn reposition_request(
        &mut self,
        _surface: PopupSurface,
        _positioner: PositionerState,
        _token: u32,
    ) {
        todo!("popups")
    }

    fn toplevel_destroyed(&mut self, surface: ToplevelSurface) {
        self.windows.remove(&surface.wl_surface().id());
    }
}
delegate_xdg_shell!(Sudbury);

fn check_grab(
    seat: &WlSeat,
    surface: &WlSurface,
    serial: Serial,
) -> Option<(
    pointer::GrabStartData<Sudbury>,
    Seat<Sudbury>,
    PointerHandle<Sudbury>,
)> {
    let Some(seat) = Seat::<Sudbury>::from_resource(seat) else {
        error!("Couldn't find a seat");
        return None;
    };

    let pointer = seat.get_pointer()?;

    if !pointer.has_grab(serial) {
        return None;
    }

    let start_data = pointer.grab_start_data()?;

    // If the focus was for a different surface, ignore the request.
    if start_data.focus.is_none() || !start_data.clone().focus?.0.same_client_as(&surface.id()) {
        return None;
    }

    Some((start_data, seat, pointer))
}

fn xdg_configure_state(toplevel: &ToplevelSurface, state: xdg_toplevel::State) -> Option<Serial> {
    toplevel.with_pending_state(|pstate| {
        pstate.states.set(state);
    });
    toplevel.send_pending_configure()
}

fn xdg_unconfigure_state(toplevel: &ToplevelSurface, state: xdg_toplevel::State) -> Option<Serial> {
    toplevel.with_pending_state(|pstate| {
        pstate.states.unset(state);
    });
    toplevel.send_pending_configure()
}

pub fn xdg_activate(toplevel: &ToplevelSurface) -> Option<Serial> {
    xdg_configure_state(toplevel, xdg_toplevel::State::Activated)
}

pub fn xdg_deactivate(toplevel: &ToplevelSurface) -> Option<Serial> {
    xdg_unconfigure_state(toplevel, xdg_toplevel::State::Activated)
}

/// Returns if this was the first ack, clearing it if so
pub fn xdg_clear_first_ack(surface: &WlSurface) -> bool {
    with_states(surface, |states| {
        states
            .data_map
            .get::<RefCell<FirstAck>>()
            .unwrap()
            .borrow_mut()
            .clear()
    })
}

/// Configure an XDG window for the first time. This is
///
/// # Panics
///
/// Panics if Smithay's [`XdgToplevelSurfaceRoleAttributes`] is Null.
pub fn xdg_initial_configure(window: &Window, size: Size<i32, Logical>) -> Serial {
    let toplevel = &window.toplevel();
    // the `data_map` is used to store the initial configuration status. This is copied from
    // anvil.
    let initial_configure_sent = with_states(window.toplevel().wl_surface(), |states| {
        states
            .data_map
            .get::<Mutex<XdgToplevelSurfaceRoleAttributes>>()
            .unwrap()
            .lock()
            .unwrap()
            .initial_configure_sent
    });

    if !initial_configure_sent {
        // Window will start on the first output
        toplevel.with_pending_state(|state| {
            // If the size was already set due to maxmize or fullscreen, leave it. If not, let the
            // client decide its size and clamp the bounds.
            if state.size.is_none() {
                // "If the width or height arguments are zero, it means the client should decide its
                //  own window dimension. This may happen when the compositor needs to configure
                //  the state of the surface but doesn't have any information about any previous or
                //  expected dimension."
                //
                //  The below doesn't do anything but is here only for explicitness in how we're
                //  setting up the initial surface.
                state.size = None;
            }
            // "The bounds can for example correspond to the size of a monitor excluding any panels
            // 	or other shell components, so that a surface isn't created in a way that it cannot
            // 	fit."
            state.bounds = Some(size);
        });
        toplevel.send_configure()
    } else {
        debug_assert!(false, "Invalid conifgure state");
        Serial::from(0)
    }
}

#[allow(unused)]
pub fn xdg_configure_popup() {
    todo!()
}
