use smithay::{
    delegate_input_method_manager,
    utils::Rectangle,
    wayland::{
        input_method::{InputMethodHandler, PopupSurface},
        seat::WaylandFocus,
    },
};
use wayland_server::protocol::wl_surface::WlSurface;

use crate::sudbury::Sudbury;

impl InputMethodHandler for Sudbury {
    fn new_popup(&mut self, _surface: PopupSurface) {
        todo!()
    }

    fn parent_geometry(&self, parent: &WlSurface) -> Rectangle<i32, smithay::utils::Logical> {
        let Some(window) = self
            .current_space()
            .elements()
            .find(|window| window.wl_surface().as_ref() == Some(parent))
        else {
            return Rectangle::default();
        };

        window.geometry()
    }

    fn dismiss_popup(&mut self, _surface: PopupSurface) {
        todo!()
    }
}

delegate_input_method_manager!(Sudbury);
