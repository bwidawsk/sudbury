use smithay::delegate_relative_pointer;

use crate::sudbury::Sudbury;

delegate_relative_pointer!(Sudbury);
