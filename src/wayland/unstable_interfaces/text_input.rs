use smithay::delegate_text_input_manager;

use crate::sudbury::Sudbury;

delegate_text_input_manager!(Sudbury);
