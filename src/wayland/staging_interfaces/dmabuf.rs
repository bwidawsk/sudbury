use smithay::{
    backend::allocator::dmabuf::Dmabuf,
    delegate_dmabuf,
    wayland::dmabuf::{DmabufGlobal, DmabufHandler, DmabufState, ImportNotifier},
};
use tracing::error;

use crate::sudbury::Sudbury;

impl DmabufHandler for Sudbury {
    fn dmabuf_state(&mut self) -> &mut DmabufState {
        self.drm_kms.dmabuf_state_mut()
    }

    fn dmabuf_imported(
        &mut self,
        _global: &DmabufGlobal,
        dmabuf: Dmabuf,
        notifier: ImportNotifier,
    ) {
        if let Err(e) = self.drm_kms.import_dmabuf(&dmabuf) {
            error!("{:?}", e);
            notifier.failed()
        } else {
            notifier
                .successful::<Self>()
                .expect("Issue with DMABUF import");
        }
    }
}
delegate_dmabuf!(Sudbury);
