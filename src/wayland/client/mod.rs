//! Handles Wayland Client specifics.

use smithay::{reexports::wayland_server, wayland::compositor::CompositorClientState};
use tracing::{instrument, trace};
use wayland_server::backend::ClientData;

#[derive(Debug, Default)]
pub struct WaylandClient {
    pub compositor_state: CompositorClientState,
}

impl ClientData for WaylandClient {
    fn initialized(&self, client_id: wayland_server::backend::ClientId) {
        trace!("Initialized {client_id:?}");
    }
    #[instrument(level = "debug")]
    fn disconnected(
        &self,
        client_id: wayland_server::backend::ClientId,
        reason: wayland_server::backend::DisconnectReason,
    ) {
        trace!("{client_id:?} {reason:?}");
    }
}
