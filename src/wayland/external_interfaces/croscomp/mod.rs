use wayland_server::DisplayHandle;

use crate::wayland::external_interfaces::croscomp::{
    display::CroscompDisplayState, splash::CroscompSplashState,
};

// https://docs.rs/wayland-scanner/0.30.0-beta.12/wayland_scanner/index.html
#[allow(non_camel_case_types, non_snake_case, non_upper_case_globals)]
pub mod sudbury {
    use wayland_server;
    use wayland_server::protocol::*;

    pub mod __interfaces {
        use wayland_server::protocol::__interfaces::*;
        wayland_scanner::generate_interfaces!("protocol/sudbury/sudbury.xml");
    }
    use self::__interfaces::*;

    wayland_scanner::generate_server_code!("protocol/sudbury/sudbury.xml");
}

pub mod display;
pub mod splash;

#[allow(dead_code)]
pub struct CroscompState {
    splash: CroscompSplashState,
    display: CroscompDisplayState,
}

impl CroscompState {
    pub fn new(dh: &DisplayHandle) -> Self {
        Self {
            splash: splash::CroscompSplashState::new(dh),
            display: display::CroscompDisplayState::new(dh),
        }
    }
}
