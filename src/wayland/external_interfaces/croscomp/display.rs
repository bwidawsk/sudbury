use smithay::output::Output;
use tracing::debug;
use wayland_server::{backend::GlobalId, Dispatch, DisplayHandle, GlobalDispatch};

use crate::{
    sudbury::Sudbury,
    wayland::external_interfaces::croscomp::sudbury::{
        croscomp_display::{self, CroscompDisplay},
        croscomp_output,
    },
};

const GLOBAL_VERSION: u32 = 1;

pub struct CroscompDisplayState {
    _global: GlobalId,
}

impl CroscompDisplayState {
    pub fn new(dh: &DisplayHandle) -> Self {
        Self {
            _global: dh.create_global::<Sudbury, CroscompDisplay, _>(GLOBAL_VERSION, ()),
        }
    }
}

impl Dispatch<croscomp_output::CroscompOutput, ()> for Sudbury {
    fn request(
        _: &mut Sudbury,
        _: &wayland_server::Client,
        _: &croscomp_output::CroscompOutput,
        _: <croscomp_output::CroscompOutput as wayland_server::Resource>::Request,
        _: &(),
        _: &wayland_server::DisplayHandle,
        _: &mut wayland_server::DataInit<'_, Sudbury>,
    ) {
        todo!();
    }
}

fn send_output_event(
    dh: &DisplayHandle,
    client: &wayland_server::Client,
    display: &croscomp_display::CroscompDisplay,
    output: &Output,
) {
    match client.create_resource::<croscomp_output::CroscompOutput, (), Sudbury>(
        dh,
        GLOBAL_VERSION,
        (),
    ) {
        Ok(o) => {
            display.output(&o);

            let pp = output.physical_properties();
            o.geometry(
                pp.size.w,
                pp.size.h,
                pp.subpixel.into(),
                pp.make,
                pp.model,
                output.current_transform().into(),
            );
            for mode in output.modes() {
                let mut flags: croscomp_output::Mode = croscomp_output::Mode::empty();
                if Some(mode) == output.current_mode() {
                    flags.set(croscomp_output::Mode::Current, true);
                }
                if Some(mode) == output.preferred_mode() {
                    flags.set(croscomp_output::Mode::Preferred, true);
                }
                o.mode(flags, mode.size.w, mode.size.h, mode.refresh);
                o.done();
            }
        }
        Err(_) => todo!(),
    };
}

impl GlobalDispatch<croscomp_display::CroscompDisplay, ()> for Sudbury {
    fn bind(
        sudbury: &mut Self,
        handle: &DisplayHandle,
        client: &wayland_server::Client,
        resource: wayland_server::New<croscomp_display::CroscompDisplay>,
        _global_data: &(),
        data_init: &mut wayland_server::DataInit<'_, Self>,
    ) {
        debug!("Croscomp Display Global");
        let crosdisp = data_init.init(resource, ());
        for output in sudbury.drm_kms.outputs() {
            send_output_event(handle, client, &crosdisp, &output)
        }
    }
}

impl Dispatch<croscomp_display::CroscompDisplay, ()> for Sudbury {
    fn request(
        _state: &mut Self,
        _client: &wayland_server::Client,
        _resource: &croscomp_display::CroscompDisplay,
        request: <croscomp_display::CroscompDisplay as wayland_server::Resource>::Request,
        _data: &(),
        _dhandle: &DisplayHandle,
        _data_init: &mut wayland_server::DataInit<'_, Self>,
    ) {
        match request {
            croscomp_display::Request::Destroy => {}
        };
    }
}
