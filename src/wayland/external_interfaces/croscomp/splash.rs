use std::collections::HashMap;

use crate::{
    sudbury::Sudbury,
    wayland::external_interfaces::croscomp::sudbury::croscomp_splash::{self, CroscompSplash},
};
use smithay::output::Output;
use tracing::debug;
use wayland_server::{
    backend::GlobalId, protocol::wl_surface::WlSurface, Dispatch, DisplayHandle, GlobalDispatch,
};

#[allow(dead_code)]
pub struct CroscompSplashState {
    global: GlobalId,
    splashes: HashMap<Output, WlSurface>,
}

impl CroscompSplashState {
    pub fn new(dh: &DisplayHandle) -> Self {
        Self {
            global: dh.create_global::<Sudbury, CroscompSplash, _>(1, ()),
            splashes: HashMap::new(),
        }
    }
}

impl Dispatch<croscomp_splash::CroscompSplash, ()> for Sudbury {
    fn request(
        _: &mut Sudbury,
        _: &wayland_server::Client,
        _: &croscomp_splash::CroscompSplash,
        request: <croscomp_splash::CroscompSplash as wayland_server::Resource>::Request,
        _: &(),
        _: &wayland_server::DisplayHandle,
        _: &mut wayland_server::DataInit<'_, Sudbury>,
    ) {
        match request {
            croscomp_splash::Request::Set {
                output: _,
                surface: _,
            } => todo!(),
        };
    }
}

impl GlobalDispatch<croscomp_splash::CroscompSplash, ()> for Sudbury {
    fn bind(
        _: &mut Sudbury,
        _: &wayland_server::DisplayHandle,
        _: &wayland_server::Client,
        resource: wayland_server::New<croscomp_splash::CroscompSplash>,
        _: &(),
        data_init: &mut wayland_server::DataInit<'_, Sudbury>,
    ) {
        debug!("Croscomp Splash Global");
        data_init.init(resource, ());
    }
}
