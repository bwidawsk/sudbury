use smithay::delegate_virtual_keyboard_manager;

use crate::sudbury::Sudbury;

delegate_virtual_keyboard_manager!(Sudbury);
