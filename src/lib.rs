// WLCS bindings should always use a subset of functionality. As such, we must squelch certain
// warnings.
#![allow(unused_mut)]
#![allow(dead_code)]

// WLCS must always have a software renderer (for now)
#[cfg(not(any(
    not(feature = "wlcs-stub"),
    all(feature = "wlcs-stub", feature = "pixman-renderer"),
)))]
compile_error!("wlcs-stub needs a software renderer");

#[cfg(feature = "wlcs-stub")]
mod args;
#[cfg(feature = "wlcs-stub")]
mod drm_kms;
#[cfg(feature = "wlcs-stub")]
mod input;
#[cfg(feature = "wlcs-stub")]
mod policy;
#[cfg(feature = "wlcs-stub")]
mod session;
#[cfg(feature = "wlcs-stub")]
mod shell;
#[cfg(feature = "wlcs-stub")]
mod sudbury;
#[cfg(feature = "wlcs-stub")]
mod wayland;
#[cfg(feature = "wlcs-stub")]
mod wlcs;
