use ron::from_str;
use serde::Deserialize;
use smithay::output;
use std::{
    fs::File,
    io::{BufReader, Read},
    sync::{Mutex, OnceLock},
};
use tracing::debug;

static POLICY: OnceLock<Mutex<SudburyPolicy>> = OnceLock::new();
fn policy() -> &'static Mutex<SudburyPolicy> {
    POLICY.get_or_init(|| Mutex::new(SudburyPolicy::default()))
}
fn set_policy(policy: SudburyPolicy) {
    POLICY.get_or_init(|| Mutex::new(policy));
}

#[derive(Debug, Deserialize, Default)]
#[serde(default)]
struct SudburyPolicy {
    sloppy_focus: bool,
    retain_window_focus: bool,
    outputs: Vec<SuggestedOutput>,
}

#[derive(Debug, Deserialize, Default)]
#[serde(default)]
struct SuggestedOutput {
    name: String,
    resx: usize,
    resy: usize,
    refresh: f32,
    scale: f32,
}

pub struct OutputPropertyRequest {
    resx: usize,
    resy: usize,
    refresh: f32,
    scale: f32,
}

impl OutputPropertyRequest {
    pub fn is_match(&self, mode: output::Mode) -> bool {
        (mode.size.w == self.resx as i32 || self.resx == 0)
            && (mode.size.h == self.resy as i32 || self.resy == 0)
            && (mode.refresh == (self.refresh * 1000.0) as i32 || self.refresh == 0.0)
    }

    pub fn scale(&self) -> f32 {
        self.scale
    }
}

impl From<&SuggestedOutput> for OutputPropertyRequest {
    fn from(value: &SuggestedOutput) -> Self {
        Self {
            resx: value.resx,
            resy: value.resy,
            refresh: value.refresh,
            scale: value.scale,
        }
    }
}

impl std::fmt::Display for OutputPropertyRequest {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}x{} -- {}Hz x{}",
            self.resx, self.resy, self.refresh, self.scale
        )
    }
}

impl PartialEq for OutputPropertyRequest {
    fn eq(&self, other: &Self) -> bool {
        (self.resx == 0 || other.resy == 0 || self.resx == other.resx)
            && (self.resy == 0 || other.resy == 0 || self.resy == other.resy)
            && (self.refresh == 0.0 || other.refresh == 0.0 || self.refresh == other.refresh)
    }
}

pub fn sloppy_focus() -> bool {
    policy().lock().unwrap().sloppy_focus
}

pub fn set_sloppy_focus(slop: bool) {
    policy().lock().unwrap().sloppy_focus = slop;
}

pub fn focus_new_windows() -> bool {
    !policy().lock().unwrap().retain_window_focus
}

pub fn set_focus_new_windows(foc: bool) {
    policy().lock().unwrap().retain_window_focus = !foc;
}

pub fn output_preference(name: &str) -> Option<OutputPropertyRequest> {
    let binding = policy().lock().unwrap();
    let Some(output) = binding.outputs.iter().find(|o| o.name == name) else {
        return None;
    };
    Some(output.into())
}

pub fn feed_policy(cfg: Option<File>) {
    let Some(cfg) = cfg else { return };
    let mut policy = String::new();
    BufReader::new(&cfg).read_to_string(&mut policy).unwrap();
    match from_str(&policy) {
        Ok(cfg) => set_policy(cfg),
        Err(e) => debug!("Cannot use {cfg:?} for policy ({e})"),
    }
}
