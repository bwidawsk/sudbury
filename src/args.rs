use std::{collections::HashMap, env, fmt, path::PathBuf};

use bpaf::Bpaf;

use crate::sudbury::{ClearColor, OutputScales};

fn is_normal(number: &f32) -> bool {
    (0f32..1f32).contains(number)
}

#[derive(Debug, PartialEq, Eq)]
pub struct ParseColorError;

impl std::str::FromStr for ClearColor {
    type Err = ParseColorError;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let data = u32::from_str_radix(s, 16).map_err(|_| ParseColorError)?;
        let argb = data.to_le_bytes();
        Ok(ClearColor {
            r: normalize(argb[2]),
            g: normalize(argb[1]),
            b: normalize(argb[0]),
            a: normalize(argb[3]),
        })
    }
}

impl fmt::Display for ParseColorError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Couldn't parse color string")
    }
}

fn normalize(val: u8) -> f32 {
    (1f32 - 0f32) / 255_f32 * (val as f32) + 0f32
}

#[derive(Debug, PartialEq, Eq)]
pub struct ParseScaleError;

impl fmt::Display for ParseScaleError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Couldn't parse scale")
    }
}

impl std::str::FromStr for OutputScales {
    type Err = ParseScaleError;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let map: HashMap<String, f32> = s
            .split(',')
            .map(|s| s.split_at(s.find('=').unwrap()))
            .map(|(key, val)| (key.to_string(), val[1..].parse::<f32>().unwrap()))
            .collect();
        Ok(OutputScales(map))
    }
}

fn try_to_get_config() -> Result<PathBuf, &'static str> {
    if let Ok(xdg_config_dir) = env::var("XDG_CONFIG_HOME") {
        Ok(PathBuf::from(format!(
            "{xdg_config_dir}/sudbury/config.ron"
        )))
    } else {
        Ok(PathBuf::from("~/.config/sudbury/config.ron"))
    }
}

/// Sudbury Display server
///
/// Wayland server for use in ChromeOS.
#[derive(Debug, Bpaf)]
#[bpaf(options, version)]
pub struct SudburyArgs {
    /// File for Sudbury's tracing output
    #[bpaf(short('s'), long, env("SBRY_LOG"), argument("FILE"))]
    pub(crate) sudbury_log: Option<PathBuf>,

    /// Name of the DRM device node.
    #[bpaf(short, long, env("SBRY_DRM_DEV"), argument("cardX"), fallback("card0".to_string()), display_fallback)]
    pub(crate) drm_dev: String,

    #[bpaf(short('w'), long, env("WAYLAND_DISPLAY"), argument("wayland-X"))]
    pub(crate) wayland_display: Option<String>,

    /// Display FPS on command line.
    #[cfg(feature = "fps")]
    #[bpaf(long, flag(true, false))]
    pub(crate) fps: bool,

    /// Composition split during raster period
    ///
    /// Raster period is split into two components. The beginning period is left
    /// for clients to update their buffers and commit. The end period is for the
    /// compositor to composite the frame.
    ///
    /// - A value of 0.5 would split this evenly.
    ///  - A value of 0.75 with 60 Hz refresh would give the client 12.5 ms
    #[bpaf(
        long,
        env("SBRY_COMPOSITION_SPLIT"),
        fallback(0.65),
        display_fallback,
        guard(is_normal, "Compositor split must be 0.00 < SPLIT < 1.00")
    )]
    pub(crate) compositor_split: f32,

    /// "Timeout" for when to send a frame callback.
    ///
    /// Certain clients wish to receive frame callbacks even if they aren't visible (occluded).
    ///   This value specifies what the timeout value for when a callback should be sent even when
    ///   occluded.
    ///
    /// If this value is unspecified, frame callbacks will never be sent unless part of the surface
    /// is visible.
    ///   If this value is 0, frame callbacks will always be sent immediate.
    ///   If this value is greater than 0, frame callbacks will be sent after the elapsed time,
    ///   even when fully occluded.
    #[bpaf(long, env("SBRY_FRAME_THROTTLE_MS"), argument("ms"))]
    pub(crate) callback_timeout: Option<u64>,

    #[bpaf(argument::<ClearColor>, env("SBRY_CLEAR_COLOR"), fallback(ClearColor::default()), display_fallback)]
    pub(crate) clear_color: ClearColor,

    /// Scaling values for outputs
    ///
    /// Example: In order to have eDP-1 scaled to 2.0 and DP-1 to 1.0:
    ///   eDP-1=2.0,DP-4=1.0
    #[bpaf(argument::<OutputScales>, env("SBRY_SCALING"), help("DEPRECATED!!!"))]
    pub(crate) scaling: Option<OutputScales>,

    #[bpaf(
        short('c'),
        long,
        env("SBRY_CFG_PATH"),
        fallback_with(try_to_get_config)
    )]
    pub(crate) sudbury_cfg: PathBuf,
}
