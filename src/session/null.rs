use std::{
    cell::RefCell,
    collections::HashMap,
    os::fd::{AsRawFd, RawFd},
    path::{Path, PathBuf},
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
};

use smithay::{
    backend::session::{AsErrno, Session},
    reexports::rustix,
};
use tracing::{debug, error};

/// [`Session`] via the lack of session
#[derive(Clone, Debug)]
pub struct NullSession {
    active: Arc<AtomicBool>,
    devices: RefCell<HashMap<PathBuf, RawFd>>,
}

impl Drop for NullSession {
    fn drop(&mut self) {
        debug!("Null session dropped");
    }
}

impl Default for NullSession {
    fn default() -> Self {
        Self::new()
    }
}

impl Session for NullSession {
    type Error = Error;

    fn open(
        &mut self,
        path: &Path,
        flags: rustix::fs::OFlags,
    ) -> Result<rustix::fd::OwnedFd, Self::Error> {
        debug!("Opening device {path:?}");
        if self.devices.borrow().get(path).is_some() {
            return Err(Error::Busy(path.to_string_lossy().into()));
        }
        let fd = rustix::fs::open(path, flags, rustix::fs::Mode::empty())
            .map_err(|errno| Error::FailedToOpen(path.to_string_lossy().into(), errno))?;
        self.devices
            .borrow_mut()
            .insert(path.to_path_buf(), fd.as_raw_fd());
        Ok(fd)
    }

    fn close(&mut self, fd: rustix::fd::OwnedFd) -> Result<(), Self::Error> {
        debug!("Closing device {fd:?}");
        self.devices
            .borrow_mut()
            .retain(|_, v| v != &fd.as_raw_fd());
        Ok(())
    }

    fn change_vt(&mut self, _vt: i32) -> Result<(), Self::Error> {
        todo!("Switch to frecon")
    }

    fn is_active(&self) -> bool {
        self.active.load(Ordering::SeqCst)
    }

    fn seat(&self) -> String {
        // Only ever one seat
        String::from("seat0")
    }
}

impl NullSession {
    pub fn new() -> NullSession {
        NullSession {
            active: Arc::new(AtomicBool::new(true)),
            devices: RefCell::new(HashMap::new()),
        }
    }
}

impl AsErrno for Error {
    fn as_errno(&self) -> Option<i32> {
        match self {
            Error::Busy(_) => todo!(),
            Error::FailedToOpen(a, b) => eprintln!("Device {a} failed to open ({b})"),
            Error::FailedToClone(_) => todo!(),
            Error::SessionLost => todo!(),
        }
        None
    }
}

/// Errors related to Null sessions
#[derive(thiserror::Error, Debug)]
pub enum Error {
    /// Session is already closed
    #[error("Session is already closed")]
    SessionLost,
    /// Session already created for this device.
    #[error("One file per session ")]
    Busy(String),
    /// Failed to open file for session
    #[error("Failed to open `{0}`")]
    FailedToOpen(String, #[source] rustix::io::Errno),
    /// Failed to dup fd
    #[error("Failed to clone")]
    FailedToClone(String),
}
