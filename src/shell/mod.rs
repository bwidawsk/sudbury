use std::{borrow::BorrowMut, fmt::Display, sync::Mutex};

use crate::{
    sudbury::Sudbury,
    wayland::stable_interfaces::xdg_shell::{xdg_activate, xdg_deactivate},
};

use smithay::{
    desktop::{utils::surface_primary_scanout_output, Space, Window},
    output::{Output, WeakOutput},
    reexports::wayland_protocols::xdg::shell::server::xdg_toplevel,
    utils::{Logical, Point, Serial},
    wayland::{compositor::with_states, seat::WaylandFocus, shell::xdg},
};
use tracing::{debug, error, trace};
use wayland_backend::server::ObjectId;
use wayland_server::{protocol::wl_surface::WlSurface, Resource};

pub(crate) mod focus;
pub(crate) mod grab;

/// Surface state tracking.
///
/// Most of the states are XDG related
#[derive(Copy, Clone, Debug, Default, PartialEq)]
pub enum SudburySurfaceState {
    /// A surface was created but has no buffer attached and hasn't been configured. This is
    /// generally a transient state.
    #[default]
    Plastic,

    /// A client has committed a surface with no buffer attached. "Creating an xdg_surface from a
    /// wl_surface which has a buffer attached or committed is a client error, and any attempts by a
    /// client to attach or manipulate a buffer prior to the first xdg_surface.configure call must
    /// also be treated as errors.
    ///
    /// After creating a role-specific object and setting it up, the client must perform an initial
    /// commit without any buffer attached. The compositor will reply with an xdg_surface.configure
    /// event. The client must acknowledge it and is then allowed to attach a buffer to map the
    /// surface."
    ///
    /// XXX: This state is no longer very relevant and shouldn't show up in logs.
    InitialCommitted,

    /// Sudbury has sent a configure event to tell the client about its dimensions. An Ack should
    /// be received before actually changing dimensions of the surface.
    ///
    /// The serial of the configure is stored so when Ack comes, it is possible to determine which
    /// configure it correlated to.
    ConfigureSent(Serial),

    /// The compositor has acked the configure. A commit with a new buffer that meets the resized
    /// amount is likely next.
    ConfigureAcked,

    /// A commit with a buffer attachment has been made. Generally this is the steady state where
    /// the client continues to attach buffers (and they get released by the compositor). When a
    /// client wishes to display nothing, it will attach a NULL `wl_buffer` and the state will go
    /// back to `ConfigureAcked`.
    BufferAttached,
}

impl Display for SudburySurfaceState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

/// Every output may have a single window that is fullscreen. That window and it's previous state
/// are stored here.
struct FullscreenSurface {
    /// The window that is going to be shrunk, or made fullscreen.
    pub window: Option<Window>,

    /// Previous window state prior to fullscreen.
    ///
    /// When a window is made fullscreen, `last_state` records it's position in the space so that
    /// it can be restored to it's size and position.
    ///
    /// TODO: Should `ToplevelState` be saved, or just `Size`?
    last_state: Mutex<Option<(xdg::ToplevelState, Point<i32, Logical>)>>,
}

impl FullscreenSurface {
    /// Takes last state of this [`FullscreenSurface`].
    ///
    /// Useful when a fullscreen surface is being shrunk from fullscreen.
    ///
    /// # Panics
    ///
    /// Panics if there was an error obtaining the mutex. This should not happen until/when
    /// multi-threading happens.
    pub fn take_last_state(&self) -> Option<(xdg::ToplevelState, Point<i32, Logical>)> {
        self.last_state.lock().unwrap().take()
    }

    /// Returns the last state of this [`FullscreenSurface`].
    ///
    /// # Panics
    ///
    /// Panics if there was an error obtaining the mutex. This should not happen until/when
    /// multi-threading happens.
    pub fn last_state(&self) -> Option<(xdg::ToplevelState, Point<i32, Logical>)> {
        self.last_state.lock().unwrap().clone()
    }

    /// Sets the last state of this [`FullscreenSurface`].
    ///
    /// Useful when fullscreen a window.
    ///
    /// # Panics
    ///
    /// Panics if there was an error obtaining the mutex. This should not happen until/when
    /// multi-threading happens.
    pub fn set_last_state(&self, last_state: xdg::ToplevelState, p: Point<i32, Logical>) {
        if self
            .last_state
            .lock()
            .unwrap()
            .replace((last_state, p))
            .is_some()
        {
            error!("Lost track of fullscreen state");
        }
    }
}

/// Information for the window being resized
///
/// Window resizing is a multistage process whereby the server tells the client about the resize and
/// the client needs time to accommodate. As a result, resize information needs to be stored at the
/// time of the decision to resize, and utilized later. This `enum` captures that information.
#[derive(Debug)]
pub enum ResizeTarget {
    /// Fullscreen resize with the associated output to fullscreen on
    Fullscreen(WeakOutput),
    /// Windowed resize with the point where the window will be placed.
    ///
    /// Window size is currently stored in output as it's only applicable for returning from
    /// fullscreen
    Windowed(Point<i32, Logical>),
}

#[derive(Debug)]
pub struct SudburyWindow(Window, SudburySurfaceState, Option<ResizeTarget>, bool);

impl Sudbury {
    /// Create a [`SudburyWindow`] from a [`Window`]
    ///
    /// Sudbury will own the window and give out clones for mapping into Space.
    pub fn create_window(window: Window) -> SudburyWindow {
        SudburyWindow(
            window,
            Default::default(),
            Default::default(),
            Default::default(),
        )
    }

    /// Obtains the Window and state that is represented by `surface`
    ///
    /// Non-XDG surfaces will not be found here and thus None will be returned.
    pub fn find_window(&self, surface: &WlSurface) -> Option<(&Window, &SudburySurfaceState)> {
        self.windows
            .get(&surface.id())
            .map(|window| (&window.0, &window.1))
    }

    /// Obtains the Window and state that is represented by `surface`
    pub fn to_window(&self, surface: &WlSurface) -> &Window {
        &self.windows.get(&surface.id()).unwrap().0
    }

    /// Find a [`Window`] with the specified [`SudburySurfaceState`]
    pub fn find_window_with_state(
        &self,
        surface: &WlSurface,
        state: SudburySurfaceState,
    ) -> Option<&Window> {
        self.windows.get(&surface.id()).and_then(|window| {
            if window.1 == state {
                Some(&window.0)
            } else {
                None
            }
        })
    }

    /// Mutable version of [`find_window`]
    pub fn find_window_mut(
        &mut self,
        surface: &WlSurface,
    ) -> Option<(&mut Window, &mut SudburySurfaceState)> {
        let id = surface.id();
        self.windows
            .get_mut(&id)
            .map(|window| (&mut window.0, &mut window.1))
    }

    /// Get the commit/configure/ack state of the surface
    ///
    /// Non-XDG windows are considered to always be [`SudburySurfaceState::Plastic`]
    pub fn get_surface_state(&self, surface: &WlSurface) -> SudburySurfaceState {
        self.find_window(surface)
            .map_or(SudburySurfaceState::Plastic, |(_, s)| *s)
    }

    /// Sets the commit/configure/ack state of the surface
    ///
    /// Additionally tries to maintain the sanctity of the state machine by checking for valid state
    /// transitions based on the current state.
    pub fn set_surface_state(
        &mut self,
        surface: &WlSurface,
        state: SudburySurfaceState,
        pending_configure: bool,
    ) {
        let prev_state = self.get_surface_state(surface);
        match (prev_state, state, pending_configure) {
           // First commit
            (SudburySurfaceState::Plastic, SudburySurfaceState::InitialCommitted, _)
                // Normal XDG configure sequence commit -> configure
            | (SudburySurfaceState::InitialCommitted, SudburySurfaceState::ConfigureSent(_), _)
                 // Normal XDG configure sequence, configure -> ack_configure
            | (SudburySurfaceState::ConfigureSent(_), SudburySurfaceState::ConfigureAcked, _)
                // A new configure was sent before a buffer was attached
            | (SudburySurfaceState::ConfigureAcked, SudburySurfaceState::ConfigureSent(_), _)
                // Normal XDG configure sequence, ack_configure -> attach buffer
            | (SudburySurfaceState::ConfigureAcked, SudburySurfaceState::BufferAttached, _)
                // Client sent multiple acks
            | (SudburySurfaceState::BufferAttached, SudburySurfaceState::ConfigureAcked, _)
                // Normal XDG sequence to resize
            | (SudburySurfaceState::BufferAttached, SudburySurfaceState::ConfigureSent(_), _)
            | (SudburySurfaceState::Plastic, _, true) => (),
                // Multiple configure events
            (SudburySurfaceState::ConfigureSent(s1), SudburySurfaceState::ConfigureSent(s2), _) => {
                if s2 <= s1 {
                    debug_assert!(false, "Bad serial state");
                    error!("Bad configure state transition {prev_state:?} -> {state:?}");  
                }
            },
            _ => error!("Unexpected state transition {prev_state:?} -> {state:?}\n{}", std::backtrace::Backtrace::force_capture()),
        };
        if let Some((_, window_state)) = self.find_window_mut(surface) {
            *window_state = state;
        }
        trace!("Set state to {state}")
    }

    /// Prepare a window to be resized
    ///
    /// Resizing windows in Wayland (XDG) require that the client potentially re-allocate buffers
    /// and reattach them to the `wl_surface`. This implies that the state machine of waiting for
    /// the client to `ack_configure` must now be adhered to.
    ///
    /// Internally this simply changes the state of the window.
    pub(crate) fn start_window_resize(&mut self, window: &Window, location: ResizeTarget) {
        if let Some(window) = self.windows.get_mut(&window.toplevel().wl_surface().id()) {
            window.2 = Some(location)
        }
    }

    pub(crate) fn get_window_resize(&self, window: &Window) -> Option<&ResizeTarget> {
        if let Some(window) = self.windows.get(&window.toplevel().wl_surface().id()) {
            return window.2.as_ref();
        }

        None
    }

    /// This completes windows resizing from the protocol perspective.
    ///
    /// As mentioned in [`start_window_resize`] resizing windows require going through the
    /// ack/commit/configure state machine again. When this function is called, the caller is
    /// signaling that the internal state shall allow the Window to be mapped back into the
    /// [`Space`] at its new size.
    pub(crate) fn ack_window_resize(&mut self, window: &Window) {
        if let Some(window) = self.windows.get_mut(&window.toplevel().wl_surface().id()) {
            window.3 = true;
        }
    }

    /// Actually resize the window when the state machine is ready.
    ///
    /// This should be called when all buffers are ready and the window should be resized. The
    /// result is the window will be updated after the next VBlank.
    pub(crate) fn finish_resize(&mut self, surface: &WlSurface) {
        let Some(sudbury_window) = self.windows.get_mut(&surface.id()) else {
            error!("Lost track of window for {surface:?}");
            debug_assert!(false, "Lost track of a window");
            return;
        };

        if !sudbury_window.3 {
            // Is ack set? If not, this window wasn't being resized.
            return;
        }

        let Some(resize) = sudbury_window.2.take() else {
            error!("Lost resize info for {surface:?}");
            debug_assert!(false, "Lost resize information");
            return;
        };

        // Clear the ack state for future resizes
        sudbury_window.3 = false;

        // Hand off the window clone to be resized.
        let window = sudbury_window.0.clone();
        self.resize_window(window, resize);
    }

    /// Maps a window to the position.
    ///
    /// Aside from the Smithay/Space calls to actually map the element and make it active, the
    /// window is also added to the list of Windows that are managed by the Sudbury window manager.
    fn map_window(
        &mut self,
        space: Option<&mut Space<Window>>,
        window: Window,
        pos: Point<i32, Logical>,
    ) {
        // map and activate the element
        let space = space.unwrap_or_else(|| self.current_space_mut());
        trace!("Mapped to {pos:?}");
        space.map_element(window, pos, true);
    }

    pub fn activate_window(&mut self, window: Option<&Window>) {
        if window == self.active_window.as_ref() {
            return;
        }

        // Store and Unset the previously activate window
        if let Some(window) = self.active_window.take() {
            trace!("Dectivating");
            let serial = xdg_deactivate(window.toplevel()).unwrap();
            self.set_surface_state(
                window.toplevel().wl_surface(),
                SudburySurfaceState::ConfigureSent(serial),
                false,
            )
        }

        if let Some(window) = window {
            trace!("Activating");
            self.active_window = Some(window.clone());
            let serial = xdg_activate(window.toplevel()).unwrap();
            self.set_surface_state(
                window.toplevel().wl_surface(),
                SudburySurfaceState::ConfigureSent(serial),
                false,
            )
        }
    }

    #[profiling::function]
    fn resize_window(&mut self, window: Window, resize: ResizeTarget) {
        match resize {
            ResizeTarget::Fullscreen(output) => {
                let output: Output = output
                    .upgrade()
                    .unwrap_or_else(|| self.drm_kms.outputs().first().unwrap().clone());
                let pos = self.current_space().output_geometry(&output).unwrap().loc;
                self.map_window(None, window, pos);
            }
            ResizeTarget::Windowed(pos) => self.map_window(None, window, pos),
        }
    }

    pub fn place_window(&mut self, window: Window, pos: Point<i32, Logical>) {
        self.map_window(None, window, pos);
    }

    #[profiling::function]
    pub(super) fn fullscreen(&mut self, surface: xdg::ToplevelSurface, output: &Output) {
        // Can we fullscreen to an output that isn't mapped with non-zero area?
        let Some(geom) = self.current_space().output_geometry(output) else {
            error!("{output:?} has no geometry");
            return;
        };

        let Some(window) = self.find_window(surface.wl_surface()) else {
            error!("No window for surface");
            return;
        };

        let window = window.0.clone();

        // Unfullscreen any previous fullscreen on this output
        if let Some(fss) = output.user_data().get::<FullscreenSurface>() {
            if let Some(w) = fss.window.as_ref() {
                self.unfullscreen(w.toplevel());
            }
        }
        assert_eq!(
            output
                .user_data()
                .get::<FullscreenSurface>()
                .and_then(|fss| fss.last_state()),
            None
        );

        // Record the current state so it can be restored.
        let current_state = surface.current_state();
        // Can an unmapped window be fullscreened?
        let p = self
            .current_space()
            .element_location(&window)
            .unwrap_or((0, 0).into());

        surface.with_pending_state(|state| {
            // The surface may be maximized, and if going from maximized -> fullscreen ->
            // unfullscreen, it should return to maximized.
            state.states.set(xdg_toplevel::State::Fullscreen);
            state.size = Some(geom.size);

            // `ToplevelState.fullscreen_output` wants a WlOutput. While a WlOutput corresponds to an
            // Output, WlOutput are instances based on the client using them. Therefore, the following
            // is needed to get the proper WlOutput.
            state.fullscreen_output = output
                .client_outputs(
                    &self
                        .display_handle()
                        .get_client(surface.wl_surface().id())
                        .unwrap(),
                )
                .iter()
                .find(|o| output.owns(o))
                .cloned();
        });

        // `Output` `UserDataMap` is used to store fullscreen state to avoid the re-entrant
        // `with_states`
        output.user_data().insert_if_missing(|| FullscreenSurface {
            window: Some(window.clone()),
            last_state: Mutex::default(),
        });

        output
            .user_data()
            .get::<FullscreenSurface>()
            .unwrap()
            .borrow_mut()
            .set_last_state(current_state, p);

        // If a surface was created, and fullscreened before commit, the following applies. Wayland
        // spec as of now implies the compositor must send a configure event in response, however,
        // discussion on #wayland indicate it's preferred to wait for the initial commit.
        if !matches!(
            self.get_surface_state(surface.wl_surface()),
            SudburySurfaceState::Plastic
        ) {
            if let Some(serial) = surface.send_pending_configure() {
                self.set_surface_state(
                    surface.wl_surface(),
                    SudburySurfaceState::ConfigureSent(serial),
                    true,
                );
            }
        }

        self.start_window_resize(&window, ResizeTarget::Fullscreen(output.downgrade()));

        // need to schedule a re-render? I don't think so
        trace!("Fullscreen {}", self.title(surface.wl_surface()));
    }

    pub fn output_for_fullscreen(&self, surface: &xdg::ToplevelSurface) -> Option<Output> {
        for output in self.drm_kms.outputs() {
            let found = output
                .user_data()
                .get::<FullscreenSurface>()
                .and_then(|fss| {
                    fss.window
                        .as_ref()
                        .map(|window| window.wl_surface().unwrap() == *surface.wl_surface())
                })
                .is_some();
            if found {
                return Some(output);
            }
        }
        None
    }

    pub fn window_for_fullscreen(&self, surface: &xdg::ToplevelSurface) -> Option<&Window> {
        self.output_for_fullscreen(surface).and_then(|o| {
            o.user_data()
                .get::<FullscreenSurface>()
                .and_then(|fss| fss.window.as_ref())
                .and_then(|win| self.find_window(win.toplevel().wl_surface()))
                .map(|x| x.0)
        })
    }

    pub fn output_and_window_for_fullscreen(
        &self,
        surface: &xdg::ToplevelSurface,
    ) -> Option<(Output, &Window)> {
        let output = self.output_for_fullscreen(surface)?;
        let window = self.window_for_fullscreen(surface)?;
        Some((output, window))
    }

    #[profiling::function]
    pub(super) fn unfullscreen(&mut self, surface: &xdg::ToplevelSurface) {
        let last_state = self
            .drm_kms
            .outputs()
            .iter()
            .find_map(|output| output.user_data().get::<FullscreenSurface>())
            .and_then(|fss| fss.take_last_state());

        let p = last_state.as_ref().map_or((0, 0).into(), |l| l.1);

        surface.with_pending_state(|state| {
            let _ = state.fullscreen_output.take();
            if let Some(last_state) = last_state {
                *state = last_state.0;
            } else {
                state.size = None;
            }
        });

        if !matches!(
            self.get_surface_state(surface.wl_surface()),
            SudburySurfaceState::Plastic
        ) {
            if let Some(serial) = surface.send_pending_configure() {
                self.set_surface_state(
                    surface.wl_surface(),
                    SudburySurfaceState::ConfigureSent(serial),
                    true,
                );
            }
        }

        if let Some(window) = self.find_window(surface.wl_surface()) {
            self.start_window_resize(&window.0.clone(), ResizeTarget::Windowed(p));
        }

        trace!("Unfullscreen {}", self.title(surface.wl_surface()));
    }

    pub fn toggle_fullscreen_current(&mut self) {
        let Some(keyboard) = self.seat.get_keyboard() else {
            debug!("Fullscreen without keyboard focus");
            return;
        };

        let Some(focus) = keyboard.current_focus() else {
            return;
        };
        let Some(surface) = focus.wl_surface() else {
            return;
        };

        let primary_output = with_states(&surface, |states| {
            surface_primary_scanout_output(&surface, states).unwrap()
        });

        let fid = focus.wl_surface().map_or_else(ObjectId::null, |s| s.id());

        // If the currently focused window is fullscreen, toggle should unfullscreen it.
        if let Some(fss) = primary_output.user_data().get::<FullscreenSurface>() {
            if fss.last_state().is_some()
                && fss
                    .window
                    .as_ref()
                    .is_some_and(|w| w.wl_surface().unwrap().id() == fid)
            {
                self.unfullscreen(
                    fss.window
                        .as_ref()
                        .expect("Lost track of fullscreen")
                        .toplevel(),
                );
                return;
            }
        }

        let window = self.to_window(&focus.toplevel().unwrap());
        self.fullscreen(window.toplevel().clone(), &primary_output)
    }
}
