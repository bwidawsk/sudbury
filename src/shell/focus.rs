//! Shell focus is a rudimentary wrapper around focused elements. This pattern exists in all/most
//! Smithay compositors to handle at least popups and windows, but would extend to layer shell,
//! lock screens, and other such uses.

use smithay::{
    backend::input::KeyState,
    desktop::{find_popup_root_surface, PopupKind, Window},
    input::{
        keyboard::{KeyboardTarget, KeysymHandle, ModifiersState},
        pointer::{
            AxisFrame, ButtonEvent, GestureHoldBeginEvent, GestureHoldEndEvent,
            GesturePinchBeginEvent, GesturePinchEndEvent, GesturePinchUpdateEvent,
            GestureSwipeBeginEvent, GestureSwipeEndEvent, GestureSwipeUpdateEvent, MotionEvent,
            PointerTarget, RelativeMotionEvent,
        },
        Seat,
    },
    utils::{IsAlive, Serial},
    wayland::seat::WaylandFocus,
};
use wayland_backend::server::ObjectId;
use wayland_server::{protocol::wl_surface::WlSurface, Resource};

use crate::sudbury::Sudbury;

#[derive(Clone, Debug, PartialEq)]
pub enum FocusSurface {
    Window(Window),
    Popup(PopupKind),
}

impl TryInto<Window> for FocusSurface {
    type Error = ();

    fn try_into(self) -> Result<Window, Self::Error> {
        match self {
            FocusSurface::Window(w) => Ok(w),
            FocusSurface::Popup(_p) => Err(()),
        }
    }
}

impl From<Window> for FocusSurface {
    fn from(val: Window) -> Self {
        FocusSurface::Window(val)
    }
}

impl WaylandFocus for FocusSurface {
    fn wl_surface(&self) -> Option<WlSurface> {
        match self {
            FocusSurface::Window(w) => w.wl_surface(),
            FocusSurface::Popup(p) => Some(p.wl_surface().clone()),
        }
    }

    fn same_client_as(&self, object_id: &ObjectId) -> bool {
        match self {
            FocusSurface::Window(w) => w.same_client_as(object_id),
            FocusSurface::Popup(p) => p.wl_surface().id().same_client_as(object_id),
        }
    }
}

impl IsAlive for FocusSurface {
    fn alive(&self) -> bool {
        match self {
            FocusSurface::Window(w) => w.alive(),
            FocusSurface::Popup(p) => p.alive(),
        }
    }
}

macro_rules! keyboard_focus {
    ($focus_surface:tt, $thing:ident, $seat:ident, $data:ident) => {
        match $focus_surface {
            FocusSurface::Window(w) => KeyboardTarget::$thing(w, $seat, $data),
            FocusSurface::Popup(p) => KeyboardTarget::$thing(p.wl_surface(), $seat, $data),
        }
    };
    ($focus_surface:tt, $thing:ident, $seat:ident, $data:ident, $arg0:ident) => {
        match $focus_surface {
            FocusSurface::Window(w) => KeyboardTarget::$thing(w, $seat, $data, $arg0),
            FocusSurface::Popup(p) => KeyboardTarget::$thing(p.wl_surface(), $seat, $data, $arg0),
        }
    };
    ($focus_surface:tt, $thing:ident, $seat:ident, $data:ident, $arg0:ident, $arg1:ident) => {
        match $focus_surface {
            FocusSurface::Window(w) => KeyboardTarget::$thing(w, $seat, $data, $arg0, $arg1),
            FocusSurface::Popup(p) => {
                KeyboardTarget::$thing(p.wl_surface(), $seat, $data, $arg0, $arg1)
            }
        }
    };
    ($focus_surface:tt, $thing:ident, $seat:ident, $data:ident, $arg0:ident, $arg1:ident, $arg2:ident, $arg3:ident) => {
        match $focus_surface {
            FocusSurface::Window(w) => {
                KeyboardTarget::$thing(w, $seat, $data, $arg0, $arg1, $arg2, $arg3)
            }
            FocusSurface::Popup(p) => {
                KeyboardTarget::$thing(p.wl_surface(), $seat, $data, $arg0, $arg1, $arg2, $arg3)
            }
        }
    };
}

impl KeyboardTarget<Sudbury> for FocusSurface {
    fn enter(
        &self,
        seat: &Seat<Sudbury>,
        data: &mut Sudbury,
        keys: Vec<KeysymHandle<'_>>,
        serial: Serial,
    ) {
        keyboard_focus!(self, enter, seat, data, keys, serial);
    }

    fn leave(&self, seat: &Seat<Sudbury>, data: &mut Sudbury, serial: Serial) {
        keyboard_focus!(self, leave, seat, data, serial);
    }

    fn key(
        &self,
        seat: &Seat<Sudbury>,
        data: &mut Sudbury,
        key: KeysymHandle<'_>,
        state: KeyState,
        serial: Serial,
        time: u32,
    ) {
        keyboard_focus!(self, key, seat, data, key, state, serial, time);
    }

    fn modifiers(
        &self,
        seat: &Seat<Sudbury>,
        data: &mut Sudbury,
        modifiers: ModifiersState,
        serial: Serial,
    ) {
        keyboard_focus!(self, modifiers, seat, data, modifiers, serial);
    }
}

macro_rules! pointer_focus {
    ($focus_surface:tt, $thing:ident, $seat:ident, $data:ident) => {
        match $focus_surface {
            FocusSurface::Window(w) => PointerTarget::$thing(w, $seat, $data),
            FocusSurface::Popup(p) => PointerTarget::$thing(p.wl_surface(), $seat, $data),
        }
    };
    ($focus_surface:tt, $thing:ident, $seat:ident, $data:ident, $arg0:ident) => {
        match $focus_surface {
            FocusSurface::Window(w) => PointerTarget::$thing(w, $seat, $data, $arg0),
            FocusSurface::Popup(p) => PointerTarget::$thing(p.wl_surface(), $seat, $data, $arg0),
        }
    };
    ($focus_surface:tt, $thing:ident, $seat:ident, $data:ident, $arg0:ident, $arg1:ident) => {
        match $focus_surface {
            FocusSurface::Window(w) => PointerTarget::$thing(w, $seat, $data, $arg0, $arg1),
            FocusSurface::Popup(p) => {
                PointerTarget::$thing(p.wl_surface(), $seat, $data, $arg0, $arg1)
            }
        }
    };
}

impl PointerTarget<Sudbury> for FocusSurface {
    fn enter(&self, seat: &Seat<Sudbury>, data: &mut Sudbury, event: &MotionEvent) {
        pointer_focus!(self, enter, seat, data, event);
    }

    fn motion(&self, seat: &Seat<Sudbury>, data: &mut Sudbury, event: &MotionEvent) {
        pointer_focus!(self, motion, seat, data, event);
    }

    fn relative_motion(
        &self,
        seat: &Seat<Sudbury>,
        data: &mut Sudbury,
        event: &RelativeMotionEvent,
    ) {
        pointer_focus!(self, relative_motion, seat, data, event);
    }

    fn button(&self, seat: &Seat<Sudbury>, data: &mut Sudbury, event: &ButtonEvent) {
        pointer_focus!(self, button, seat, data, event);
    }

    fn axis(&self, seat: &Seat<Sudbury>, data: &mut Sudbury, frame: AxisFrame) {
        pointer_focus!(self, axis, seat, data, frame);
    }

    fn frame(&self, seat: &Seat<Sudbury>, data: &mut Sudbury) {
        pointer_focus!(self, frame, seat, data);
    }

    fn gesture_swipe_begin(
        &self,
        seat: &Seat<Sudbury>,
        data: &mut Sudbury,
        event: &GestureSwipeBeginEvent,
    ) {
        pointer_focus!(self, gesture_swipe_begin, seat, data, event);
    }

    fn gesture_swipe_update(
        &self,
        seat: &Seat<Sudbury>,
        data: &mut Sudbury,
        event: &GestureSwipeUpdateEvent,
    ) {
        pointer_focus!(self, gesture_swipe_update, seat, data, event);
    }

    fn gesture_swipe_end(
        &self,
        seat: &Seat<Sudbury>,
        data: &mut Sudbury,
        event: &GestureSwipeEndEvent,
    ) {
        pointer_focus!(self, gesture_swipe_end, seat, data, event);
    }

    fn gesture_pinch_begin(
        &self,
        seat: &Seat<Sudbury>,
        data: &mut Sudbury,
        event: &GesturePinchBeginEvent,
    ) {
        pointer_focus!(self, gesture_pinch_begin, seat, data, event);
    }

    fn gesture_pinch_update(
        &self,
        seat: &Seat<Sudbury>,
        data: &mut Sudbury,
        event: &GesturePinchUpdateEvent,
    ) {
        pointer_focus!(self, gesture_pinch_update, seat, data, event);
    }

    fn gesture_pinch_end(
        &self,
        seat: &Seat<Sudbury>,
        data: &mut Sudbury,
        event: &GesturePinchEndEvent,
    ) {
        pointer_focus!(self, gesture_pinch_end, seat, data, event);
    }

    fn gesture_hold_begin(
        &self,
        seat: &Seat<Sudbury>,
        data: &mut Sudbury,
        event: &GestureHoldBeginEvent,
    ) {
        pointer_focus!(self, gesture_hold_begin, seat, data, event);
    }

    fn gesture_hold_end(
        &self,
        seat: &Seat<Sudbury>,
        data: &mut Sudbury,
        event: &GestureHoldEndEvent,
    ) {
        pointer_focus!(self, gesture_hold_end, seat, data, event);
    }

    fn leave(&self, seat: &Seat<Sudbury>, data: &mut Sudbury, serial: Serial, time: u32) {
        pointer_focus!(self, leave, seat, data, serial, time);
    }
}

impl FocusSurface {
    pub fn toplevel(&self) -> Option<WlSurface> {
        match self {
            FocusSurface::Window(w) => w.wl_surface(),
            FocusSurface::Popup(p @ PopupKind::Xdg(_)) => find_popup_root_surface(p).ok(),
            _ => todo!(),
        }
    }
}
