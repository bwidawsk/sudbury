use smithay::{
    desktop::Window,
    input::{
        pointer::{
            AxisFrame, ButtonEvent, GrabStartData, MotionEvent, PointerGrab, PointerInnerHandle,
        },
        SeatHandler,
    },
    output::{Output, WeakOutput},
    utils::{Logical, Point},
};

use crate::sudbury::Sudbury;

pub struct SurfaceMov {
    pub(crate) start_data: GrabStartData<Sudbury>,
    pub(crate) window: Window,
    #[allow(dead_code)]
    pub(crate) output: WeakOutput,
    pub(crate) window_pos: Point<i32, Logical>,
}

impl SurfaceMov {
    pub fn new(
        start_data: GrabStartData<Sudbury>,
        window: Window,
        output: &Output,
        window_pos: Point<i32, Logical>,
    ) -> Self {
        Self {
            start_data,
            window,
            output: output.downgrade(),
            window_pos,
        }
    }
}

impl PointerGrab<Sudbury> for SurfaceMov {
    fn motion(
        &mut self,
        sudbury: &mut Sudbury,
        handle: &mut PointerInnerHandle<'_, Sudbury>,
        _focus: Option<(<Sudbury as SeatHandler>::PointerFocus, Point<i32, Logical>)>,
        event: &MotionEvent,
    ) {
        // Why None focus:
        //  "Some grabs (such as drag'n'drop, shell resize and motion) unset the focus while they
        //  are active, this is achieved by just setting the focus to `None` when invoking
        //  `PointerInnerHandle::motion()`."
        handle.motion(sudbury, None, event);

        let delta = event.location - self.start_data.location;
        let new_location = self.window_pos.to_f64() + delta;

        sudbury.map_window(None, self.window.clone(), new_location.to_i32_round())
    }

    fn relative_motion(
        &mut self,
        data: &mut Sudbury,
        handle: &mut PointerInnerHandle<'_, Sudbury>,
        focus: Option<(<Sudbury as SeatHandler>::PointerFocus, Point<i32, Logical>)>,
        event: &smithay::input::pointer::RelativeMotionEvent,
    ) {
        handle.relative_motion(data, focus, event);
    }

    fn button(
        &mut self,
        sudbury: &mut Sudbury,
        handle: &mut PointerInnerHandle<'_, Sudbury>,
        event: &ButtonEvent,
    ) {
        handle.button(sudbury, event);

        if handle.current_pressed().is_empty() {
            handle.unset_grab(sudbury, event.serial, event.time, true);
        }
    }

    fn axis(
        &mut self,
        data: &mut Sudbury,
        handle: &mut PointerInnerHandle<'_, Sudbury>,
        details: AxisFrame,
    ) {
        handle.axis(data, details)
    }

    fn start_data(&self) -> &GrabStartData<Sudbury> {
        &self.start_data
    }

    fn gesture_swipe_begin(
        &mut self,
        _data: &mut Sudbury,
        _handle: &mut PointerInnerHandle<'_, Sudbury>,
        _event: &smithay::input::pointer::GestureSwipeBeginEvent,
    ) {
        todo!()
    }

    fn gesture_swipe_update(
        &mut self,
        _data: &mut Sudbury,
        _handle: &mut PointerInnerHandle<'_, Sudbury>,
        _event: &smithay::input::pointer::GestureSwipeUpdateEvent,
    ) {
        todo!()
    }

    fn gesture_swipe_end(
        &mut self,
        _data: &mut Sudbury,
        _handle: &mut PointerInnerHandle<'_, Sudbury>,
        _event: &smithay::input::pointer::GestureSwipeEndEvent,
    ) {
        todo!()
    }

    fn gesture_pinch_begin(
        &mut self,
        _data: &mut Sudbury,
        _handle: &mut PointerInnerHandle<'_, Sudbury>,
        _event: &smithay::input::pointer::GesturePinchBeginEvent,
    ) {
        todo!()
    }

    fn gesture_pinch_update(
        &mut self,
        _data: &mut Sudbury,
        _handle: &mut PointerInnerHandle<'_, Sudbury>,
        _event: &smithay::input::pointer::GesturePinchUpdateEvent,
    ) {
        todo!()
    }

    fn gesture_pinch_end(
        &mut self,
        _data: &mut Sudbury,
        _handle: &mut PointerInnerHandle<'_, Sudbury>,
        _event: &smithay::input::pointer::GesturePinchEndEvent,
    ) {
        todo!()
    }

    fn gesture_hold_begin(
        &mut self,
        _data: &mut Sudbury,
        _handle: &mut PointerInnerHandle<'_, Sudbury>,
        _event: &smithay::input::pointer::GestureHoldBeginEvent,
    ) {
        todo!()
    }

    fn gesture_hold_end(
        &mut self,
        _data: &mut Sudbury,
        _handle: &mut PointerInnerHandle<'_, Sudbury>,
        _event: &smithay::input::pointer::GestureHoldEndEvent,
    ) {
        todo!()
    }

    fn frame(&mut self, data: &mut Sudbury, handle: &mut PointerInnerHandle<'_, Sudbury>) {
        handle.frame(data)
    }
}
