//! Sudbury compositor for Chrome OS

#[cfg(feature = "fps")]
use std::time::Duration;
use std::{fs::File, io::stdout, path::PathBuf};

use anyhow::{Context, Result};

#[cfg(feature = "fps")]
use smithay::reexports::calloop;
use smithay::reexports::{
    calloop::{EventLoop, LoopHandle},
    wayland_server::Display,
};

use tracing::{debug, error, info};
use tracing_appender::non_blocking::WorkerGuard;
use tracing_subscriber::filter::EnvFilter;

pub mod args;
pub mod policy;
pub mod sudbury;

mod drm_kms;
mod input;
mod session;
mod shell;
mod wayland;

use input::SudburyInput;

use crate::{
    args::sudbury_args,
    sudbury::{GlobalState, Sudbury},
};

#[allow(unused)]
fn fps_display(handle: LoopHandle<'static, GlobalState>) {
    #[cfg(feature = "fps")]
    {
        let fps_display = calloop::timer::Timer::from_duration(Duration::from_secs(5));
        handle
            .insert_source(fps_display, |_, _, data| {
                println!("FPS: {:?}", data.sudbury.drm_kms.fps(),);
                calloop::timer::TimeoutAction::ToDuration(Duration::from_secs(5))
            })
            .map_err(|e| error!("Failed to start FPS ticker {e}"))
            .ok();
    }
}

fn setup_logging(file: &Option<PathBuf>) -> WorkerGuard {
    // Guard is needed to make sure final logs are flushed. Don't set this to _.
    let (non_blocking, guard) = if let Some(sud_log) = file {
        let sud_log = File::create(sud_log).expect("Couldn't create {sud_log}");
        tracing_appender::non_blocking(sud_log)
    } else {
        tracing_appender::non_blocking(stdout())
    };

    let builder = tracing_subscriber::fmt()
        .pretty()
        .with_env_filter(EnvFilter::from_default_env())
        .with_writer(non_blocking);

    // Debug builds will use env filter

    #[cfg(feature = "disable-logging")]
    let builder = builder.with_max_level(tracing::Level::ERROR);

    let subscriber = builder.finish();

    #[cfg(feature = "profiler")]
    let subscriber = tracing_subscriber::prelude::__tracing_subscriber_SubscriberExt::with(
        subscriber,
        tracing_tracy::TracyLayer::new(),
    );

    tracing::subscriber::set_global_default(subscriber)
        .expect("Couldn't register global subscriber");
    guard
}

fn main() -> Result<()> {
    #[cfg(feature = "chromeos")]
    libchromeos::panic_handler::install_memfd_handler();

    #[cfg(feature = "profiler")]
    {
        profiling::tracy_client::Client::start();
        profiling::register_thread!("Main");
    }

    let args = sudbury_args().run();

    let _guard = setup_logging(&args.sudbury_log);

    // The display represents the single global Wayland display object ID 1.
    let display: Display<Sudbury> = Display::new().with_context(|| "Failed to init display")?;
    let dh = display.handle();

    let mut event_loop: EventLoop<GlobalState> =
        EventLoop::try_new().with_context(|| "Failed to initialize event loop")?;

    let signal = event_loop.get_signal();

    let mut sudbury = Sudbury::new(
        display.handle().clone(),
        (event_loop.handle(), signal.clone()),
        &args,
        false,
    );

    sudbury.init_dispatch(display);

    if SudburyInput::new(&mut sudbury)
        .map(|input| sudbury::attach_input(&mut sudbury, input))
        .is_none()
    {
        info!("No input system available");
    }

    std::env::set_var("WAYLAND_DISPLAY", sudbury.socket());
    info!("Advertising Wayland socket {}", sudbury.socket());

    // Handle ctrl+c
    ctrlc::set_handler(move || {
        info!("ctrl+c received. Stopping...");
        signal.stop();
        signal.wakeup();
    })
    .expect("Error setting Ctrl-C handler");

    // Package up the callback data. EventLoop now owns Sudbury
    let mut event_data = GlobalState { dh, sudbury };

    #[cfg(feature = "fps")]
    if args.fps {
        fps_display(event_loop.handle());
    }

    debug!("Starting event loop");
    // With no timeout, the event loop should only do something if a dispatch of any sort has occurred.
    // After any dispatching, it's important to flush buffers back to the client.
    event_loop.run(None, &mut event_data, |data| {
        if let Err(e) = data.dh.flush_clients() {
            error!("Failed to flush buffers to clients {e}");
        }
    })?;
    debug!("Ending event loop");

    std::mem::drop(event_loop);

    Ok(())
}
