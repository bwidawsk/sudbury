use std::{
    env,
    os::fd::{AsRawFd, RawFd},
    sync::{
        atomic::{AtomicBool, Ordering},
        mpsc, Mutex, OnceLock,
    },
    time::Duration,
};

use smithay::{
    backend::input::ButtonState,
    input::pointer::{ButtonEvent, MotionEvent},
    reexports::calloop,
    utils::SERIAL_COUNTER,
    wayland::seat::WaylandFocus,
};
use tracing::{debug, error, instrument, trace};
use wayland_server::{Client, Display, Resource};

use crate::{
    args::SudburyArgs,
    input::{PointerMotion, SudburyInput},
    shell::focus::FocusSurface,
    sudbury::{self, ClearColor, GlobalState, Sudbury},
    wlcs::{SudburyEvent, SudburyRequest},
};

static CLIENT_IDS: OnceLock<Mutex<Vec<(RawFd, Client)>>> = OnceLock::new();
pub fn add_id(fd: RawFd, client: Client) {
    CLIENT_IDS.get().unwrap().lock().unwrap().push((fd, client));
}
fn match_client(fd: i32, client: &Client) -> bool {
    CLIENT_IDS
        .get()
        .unwrap()
        .lock()
        .unwrap()
        .iter()
        .find(|(f, _)| f.as_raw_fd() == fd)
        .map(|(_, c)| c == client)
        .is_some()
}

thread_local! {
    static DONE: AtomicBool = AtomicBool::new(false);
}

#[instrument(level = "trace", skip(state))]
fn handle_event(event: SudburyRequest, state: &mut Sudbury) {
    trace!("");
    match event {
        SudburyRequest::Quit => {
            DONE.with(|x| x.store(true, Ordering::Relaxed));
            state.eventloop_signal().stop()
        }
        SudburyRequest::PositionWindow {
            client_id,
            surface_id,
            location,
        } => {
            let toplevel = state.current_space().elements().find(|w| {
                if let Some(surface) = w.wl_surface() {
                    state
                        .display_handle()
                        .get_client(surface.id())
                        .is_ok_and(|ref c| {
                            match_client(client_id, c) && surface.id().protocol_id() == surface_id
                        })
                } else {
                    false
                }
            });
            if let Some(toplevel) = toplevel.cloned() {
                let mut needs_enter = false;
                if toplevel.geometry().loc != location {
                    needs_enter = false;
                }
                // set its location
                state
                    .current_space_mut()
                    .map_element(toplevel, location, false);

                if needs_enter {
                    // Smithay doesn't send an enter event if the surface was moved under a cursor.
                    // WLCS checks for this, and it seems from the spec that it should behave this way.
                    // The only way to emit the enter event is to move the pointer, so do that.
                    let element = state
                        .current_space()
                        .element_under(state.seat.get_pointer().unwrap().current_location())
                        .map(|(w, p)| (FocusSurface::Window(w.clone()), p));
                    // Since this window just "moved" we assume it's at the top.
                    state.seat.get_pointer().unwrap().motion(
                        state,
                        element,
                        &MotionEvent {
                            location: state.seat.get_pointer().unwrap().current_location(),
                            serial: SERIAL_COUNTER.next_serial(),
                            time: state.now_us() as u32 / 1000,
                        },
                    );
                }
            } else {
                error!("Couldn't find the client {client_id} {surface_id}");
            }
        }
        SudburyRequest::PointerMoveAbs { did: _, location } => {
            let serial = SERIAL_COUNTER.next_serial();
            let Some(input) = &state.input else {
                return;
            };
            let pointer = state.seat.get_pointer().unwrap();
            let cur_loc = input.borrow().pointer.loc().0;
            let new_point = SudburyInput::pointer_motion(state, PointerMotion::Absolute(location));
            trace!("focus: {new_point}");
            let delta = new_point.end_loc() - cur_loc;
            let focus = new_point
                .window()
                .map(|(surf, point)| (FocusSurface::Window(surf.clone()), point));
            let loc = new_point.end_loc();
            let now = state.now_us() as u64;
            state.dispatch_motion(serial, loc, focus, delta, delta, now);
            SudburyInput::keyboard_refocus(state, serial);
            SudburyInput::finish_pointer_event(state, &pointer, None, serial);
        }
        SudburyRequest::PointerMoveRel { did: _, delta } => {
            let serial = SERIAL_COUNTER.next_serial();
            let pointer = state.seat.get_pointer().unwrap();
            let new_point = SudburyInput::pointer_motion(state, PointerMotion::Delta(delta));
            trace!("focus: {new_point}");
            let focus = new_point
                .window()
                .map(|(surf, point)| (FocusSurface::Window(surf.clone()), point));
            let loc = new_point.end_loc();
            let now = state.now_us() as u64;
            state.dispatch_motion(serial, loc, focus, delta, delta, now);
            SudburyInput::keyboard_refocus(state, serial);
            SudburyInput::finish_pointer_event(state, &pointer, None, serial);
        }
        SudburyRequest::PointerButtonDown { did: _, button_id } => {
            let serial = SERIAL_COUNTER.next_serial();
            let pointer = state.seat.get_pointer().unwrap();
            if !pointer.is_grabbed() {
                SudburyInput::keyboard_refocus(state, serial);
            }
            let now = state.now_us();
            pointer.button(
                state,
                &ButtonEvent {
                    button: button_id as u32,
                    state: ButtonState::Pressed,
                    serial,
                    time: now as u32,
                },
            );
        }
        SudburyRequest::PointerButtonUp { did: _, button_id } => {
            let serial = SERIAL_COUNTER.next_serial();
            let now = state.now_us();
            state.seat.get_pointer().unwrap().button(
                state,
                &ButtonEvent {
                    button: button_id as u32,
                    state: ButtonState::Released,
                    serial,
                    time: now as u32,
                },
            );
        }
    };
}

/// Simplified version of Sudbury's main
#[instrument(skip_all)]
pub fn main(
    requests: calloop::channel::Channel<SudburyRequest>,
    sender: mpsc::Sender<SudburyEvent>,
) {
    let mut eloop =
        calloop::EventLoop::<GlobalState>::try_new().expect("Failed to start fake server");
    let mut display: Display<Sudbury> = Display::new().expect("Failed to initialize the display");
    let handle = display.handle();

    let dev = env::var("SBRY_DRM_DEV").expect("Error: SBRY_DRM_DEV not found");
    let ron = env::var("SBRY_CFG_PATH").unwrap_or("/vng-sudbury/wlcs-config.ron".to_string());
    let args = SudburyArgs {
        sudbury_log: None,
        drm_dev: dev,
        wayland_display: None,
        compositor_split: 0.65,
        callback_timeout: None,
        clear_color: ClearColor::default(),
        scaling: None,
        #[cfg(feature = "fps")]
        fps: false,
        sudbury_cfg: ron.into(),
    };
    let mut sudbury = Sudbury::new(
        handle.clone(),
        (eloop.handle(), eloop.get_signal()),
        &args,
        true,
    );

    SudburyInput::new(&mut sudbury)
        .map(|input| sudbury::attach_input(&mut sudbury, input))
        .expect("Input missing");

    sender
        .send(SudburyEvent::ServerStarted {
            socket: sudbury.socket().to_owned(),
        })
        .expect("Failed to send Sudbury shim our socket");
    debug!("Started Wlcs server on {:?}", sudbury.socket());

    CLIENT_IDS.get_or_init(|| Mutex::new(vec![]));

    eloop
        .handle()
        .insert_source(requests, move |event, &mut (), data| {
            match event {
                calloop::channel::Event::Msg(evt) => {
                    handle_event(evt, &mut data.sudbury);
                    // input tests need pointer focus so if a surface was moved we want to flush any
                    // enter leave events to the client now.
                    //                data.dh.flush_clients().unwrap();
                }
                calloop::channel::Event::Closed => {
                    handle_event(SudburyRequest::Quit, &mut data.sudbury)
                }
            }
            data.sudbury.eventloop_signal().wakeup();
            std::thread::yield_now();
        })
        .unwrap();

    std::env::set_var("WAYLAND_DISPLAY", sudbury.socket());

    let mut event_data = GlobalState {
        dh: handle,
        sudbury,
    };

    let mut done = false;
    while !done {
        if let Err(e) = eloop.run(Duration::from_millis(5), &mut event_data, |data| {
            if let Err(e) = display.flush_clients() {
                error!("Failed to flush buffers to clients {e}");
            }
            if let Err(e) = display.dispatch_clients(&mut data.sudbury) {
                error!("Failed to dispatch clients {e}");
            }
        }) {
            eprintln!("Error running main loop {e}");
            break;
        }
        done = DONE.with(|x| x.load(Ordering::Relaxed));
    }

    debug!("Fake server done");
}
