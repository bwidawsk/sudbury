use core::time;
use std::{
    env,
    io::ErrorKind,
    os::{
        fd::{AsRawFd, OwnedFd},
        unix::net::UnixStream,
    },
    sync::{
        atomic::{AtomicU32, Ordering},
        mpsc,
    },
    thread::{self, JoinHandle},
    time::{Duration, Instant},
};

use smithay::{
    reexports::calloop::{
        self,
        channel::{self, Sender},
    },
    utils::{Logical, Point},
};
use tracing::{debug, error, instrument, trace, warn};

use wayland_sys::{
    client::{self, wl_display_get_fd, wl_proxy_get_id},
    common::wl_fixed_to_double,
    ffi_dispatch,
};
use wlcs::{
    extension_list,
    ffi_display_server_api::{
        WlcsExtensionDescriptor, WlcsIntegrationDescriptor, WlcsServerIntegration,
    },
    ffi_wrappers::wlcs_server,
    wlcs_server_integration, Pointer, Touch, Wlcs,
};

use crate::sudbury;

mod fake_server;

wlcs_server_integration!(SudburyHandle);

#[derive(Debug)]
pub enum SudburyRequest {
    Quit,
    PositionWindow {
        client_id: i32,
        surface_id: u32,
        location: Point<i32, Logical>,
    },
    PointerMoveAbs {
        did: u32,
        location: Point<f64, Logical>,
    },
    PointerMoveRel {
        did: u32,
        delta: Point<f64, Logical>,
    },
    PointerButtonUp {
        did: u32,
        button_id: i32,
    },
    PointerButtonDown {
        did: u32,
        button_id: i32,
    },
}

impl std::fmt::Display for SudburyRequest {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SudburyRequest::Quit => write!(f, "Quit Request"),
            SudburyRequest::PositionWindow {
                client_id: _,
                surface_id: _,
                location: _,
            } => write!(f, "Position Window Request"),
            SudburyRequest::PointerMoveAbs {
                did: _,
                location: _,
            } => {
                write!(f, "Pointer Movement (Absolute) Request")
            }
            SudburyRequest::PointerMoveRel { did: _, delta: _ } => {
                write!(f, "Pointer Movement (Relative) Request")
            }
            SudburyRequest::PointerButtonUp {
                did: _,
                button_id: _,
            } => write!(f, "Pointer button up"),
            SudburyRequest::PointerButtonDown {
                did: _,
                button_id: _,
            } => {
                write!(f, "Pointer button down")
            }
        }
    }
}

pub enum SudburyEvent {
    ServerStarted { socket: String },
}

#[derive(Debug)]
struct SudburyHandle {
    proxy: Option<(channel::Sender<SudburyRequest>, JoinHandle<()>)>,
    socket: String,
}

struct PointerHandle {
    device_id: u32,
    sender: Sender<SudburyRequest>,
}

struct TouchHandle {}

static SUPPORTED_EXTENSIONS: &[WlcsExtensionDescriptor] = extension_list!(
    ("wl_compositor", 5),
    ("wl_data_device_manager", 3),
    ("wl_seat", 7),
    ("wl_subcompositor", 1),
    ("wl_shm", 1),
    ("wl_output", 4),
    ("wp_presentation", 1),
    ("wp_viewporter", 1),
    ("xdg_wm_base", 6),
    ("zwp_linux_dmabuf_v1", 4),
    ("content_type_v1", 1),
    ("cursor_shape_v1", 1),
);

static DESCRIPTOR: WlcsIntegrationDescriptor = WlcsIntegrationDescriptor {
    version: 1,
    num_extensions: SUPPORTED_EXTENSIONS.len(),
    supported_extensions: SUPPORTED_EXTENSIONS.as_ptr(),
};

static DEVICE_ID: AtomicU32 = AtomicU32::new(0);

impl Wlcs for SudburyHandle {
    type Pointer = PointerHandle;
    type Touch = TouchHandle;

    fn new() -> Self {
        // Errors can be ignored. It's very likely a reinitializing of tracing
        if let Ok(env_filter) = tracing_subscriber::EnvFilter::try_from_default_env() {
            tracing_subscriber::fmt()
                .compact()
                .with_ansi(false)
                .with_env_filter(env_filter)
                .try_init()
                .ok();
        } else {
            tracing_subscriber::fmt()
                .compact()
                .with_ansi(false)
                .try_init()
                .ok();
        }
        Self {
            proxy: None,
            socket: String::default(),
        }
    }

    fn start(&mut self) {
        let (shim, server) = calloop::channel::channel();
        let (tx, rx) = mpsc::channel();
        let thread = std::thread::spawn(move || fake_server::main(server, tx));

        let socket = match rx.recv() {
            Ok(o) => match o {
                SudburyEvent::ServerStarted { socket } => socket,
            },
            Err(e) => {
                println!("Failed to get started event {e}");
                return;
            }
        };

        debug!("{socket} will be used");

        self.proxy.replace((shim, thread));
        self.socket = format!(
            "{}/{}",
            env::var("XDG_RUNTIME_DIR").unwrap_or("/tmp".into()),
            socket
        );
        trace!("Started server on {:?}", self.socket);
    }

    #[instrument(skip_all)]
    fn stop(&mut self) {
        let Some((shim, thread)) = self.proxy.take() else {
            println!("Server was already requested to shutdown");
            return;
        };

        if let Err(e) = shim.send(SudburyRequest::Quit) {
            eprintln!("Failed to send the Quit request {e}");
        } else {
            if thread.join().is_err() {
                eprintln!("Failed to collect thread");
            }
            return;
        }

        for _ in 1..=100000 {
            if !thread.is_finished() {
                thread::yield_now();
                continue;
            }

            if thread.join().is_err() {
                eprintln!("Failed to collect thread");
            }
            return;
        }
        warn!("Server failed shut down");
    }

    #[instrument(skip_all)]
    fn create_client_socket(&self) -> std::io::Result<OwnedFd> {
        let Some((ref _shim, ref _thread)) = self.proxy else {
            error!("Lost the proxy");
            return Err(ErrorKind::NotFound.into());
        };
        let stream = UnixStream::connect(&self.socket)?;

        // Sad hack: In order to not rewrite the unix socket handling, sudbury will return the last
        // `Client` that was created. Since googletest is singlethreaded, this isn't too terrible,
        // however, we must wait for the server to actually receive the connection and create the
        // client.
        let timeout = Duration::from_secs(1); // MAGIC number
        let start = Instant::now();
        let client = loop {
            if let Some(client) = unsafe { sudbury::last_client() } {
                break Some(client);
            }
            thread::sleep(time::Duration::from_millis(10));
            if Instant::now() - start > timeout {
                break None;
            }
        };
        let Some(client) = client else {
            error!("No last client");
            return Err(ErrorKind::ConnectionRefused.into());
        };
        fake_server::add_id(stream.as_raw_fd(), client);
        Ok(stream.into())
    }

    fn position_window_absolute(
        &self,
        display: *mut client::wl_display,
        surface: *mut client::wl_proxy,
        x: i32,
        y: i32,
    ) {
        let client_id = unsafe { ffi_dispatch!(WAYLAND_CLIENT_HANDLE, wl_display_get_fd, display) };
        let surface_id = unsafe { ffi_dispatch!(WAYLAND_CLIENT_HANDLE, wl_proxy_get_id, surface) };

        let Some((ref shim, _)) = self.proxy else {
            return;
        };
        shim.send(SudburyRequest::PositionWindow {
            client_id,
            surface_id,
            location: (x, y).into(),
        })
        .expect("Failed to send message");
    }

    fn create_pointer(&mut self) -> Option<Self::Pointer> {
        let Some(ref shim) = self.proxy else {
            return None;
        };
        Some(PointerHandle {
            device_id: DEVICE_ID.fetch_add(1, Ordering::Relaxed),
            sender: shim.0.clone(),
        })
    }

    fn create_touch(&mut self) -> Option<Self::Touch> {
        // Even though touch isn't supported, return something or else WLCS will segfault and halt
        // the run.
        let Some(ref _shim) = self.proxy else {
            return None;
        };
        Some(TouchHandle {})
    }

    fn get_descriptor(&self) -> &WlcsIntegrationDescriptor {
        &DESCRIPTOR
    }
}

impl Pointer for PointerHandle {
    fn move_absolute(
        &mut self,
        x: wayland_sys::common::wl_fixed_t,
        y: wayland_sys::common::wl_fixed_t,
    ) {
        self.sender
            .send(SudburyRequest::PointerMoveAbs {
                did: self.device_id,
                location: (wl_fixed_to_double(x), wl_fixed_to_double(y)).into(),
            })
            .expect("Failed to send pointer move event");
    }

    fn move_relative(
        &mut self,
        dx: wayland_sys::common::wl_fixed_t,
        dy: wayland_sys::common::wl_fixed_t,
    ) {
        self.sender
            .send(SudburyRequest::PointerMoveRel {
                did: self.device_id,
                delta: (wl_fixed_to_double(dx), wl_fixed_to_double(dy)).into(),
            })
            .expect("Failed to send pointer move event");
    }

    fn button_up(&mut self, button: i32) {
        self.sender
            .send(SudburyRequest::PointerButtonUp {
                did: self.device_id,
                button_id: button,
            })
            .expect("Failed to send button up event");
    }

    fn button_down(&mut self, button: i32) {
        self.sender
            .send(SudburyRequest::PointerButtonDown {
                did: self.device_id,
                button_id: button,
            })
            .expect("Failed to send button up event");
    }
}

impl Touch for TouchHandle {
    fn touch_down(
        &mut self,
        _x: wayland_sys::common::wl_fixed_t,
        _y: wayland_sys::common::wl_fixed_t,
    ) {
        todo!()
    }

    fn touch_move(
        &mut self,
        _x: wayland_sys::common::wl_fixed_t,
        _y: wayland_sys::common::wl_fixed_t,
    ) {
        todo!()
    }

    fn touch_up(&mut self) {
        todo!()
    }
}
