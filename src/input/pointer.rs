use std::any::Any;

use smithay::{
    backend::{
        allocator::Fourcc,
        renderer::{
            element::{
                self,
                surface::{render_elements_from_surface_tree, WaylandSurfaceRenderElement},
                texture::{TextureBuffer, TextureRenderElement},
                AsRenderElements,
            },
            ImportAll, ImportDmaWl, ImportMem, ImportMemWl, Renderer,
        },
    },
    input::pointer::{CursorIcon, CursorImageStatus, PointerHandle},
    output::{Output, WeakOutput},
    render_elements,
    utils::{Logical, Physical, Point, Scale, Transform},
};

use crate::sudbury::Sudbury;

static CURSOR: &[u8] = include_bytes!("chrome.rgba");

#[derive(Debug)]
pub struct SudburyPointer {
    handle: PointerHandle<Sudbury>,
    status: CursorImageStatus,
    texture: Option<Box<dyn Any>>,
    last_output: WeakOutput,
}

impl SudburyPointer {
    #[allow(dead_code)]
    pub fn new(handle: PointerHandle<Sudbury>, output: &Output) -> Self {
        Self {
            handle,
            status: CursorImageStatus::Named(CursorIcon::Default),
            texture: None,
            last_output: output.downgrade(),
        }
    }

    #[allow(dead_code)]
    pub fn icon<R>(&mut self, renderer: &mut R)
    where
        R: Renderer + ImportMem,
        <R as Renderer>::TextureId: 'static,
    {
        let texture = TextureBuffer::from_memory(
            renderer,
            CURSOR,
            Fourcc::Argb8888,
            (64i32, 64i32),
            false,
            1,
            Transform::Normal,
            None,
        )
        .expect("Couldn't obtain the texture buffer");
        self.texture = Some(Box::new(texture));
    }

    pub fn status(&self) -> &CursorImageStatus {
        &self.status
    }

    pub fn set_status(&mut self, status: CursorImageStatus) {
        self.status = status;
    }

    fn last_output(&self) -> Option<Output> {
        self.last_output.upgrade()
    }

    pub fn loc(&self) -> (Point<f64, Logical>, Option<Output>) {
        (self.handle.current_location(), self.last_output())
    }

    pub fn set_last_output(&mut self, last_output: &Output) {
        self.last_output = last_output.downgrade();
    }
}

// A pointer can either be a texture (derived from the compositor itself), or a Wayland surface.
render_elements! {
    pub SudburyPointerElement<R> where R: ImportAll;
    Client=WaylandSurfaceRenderElement<R>,
    Server=TextureRenderElement<<R as Renderer>::TextureId>,
}

impl<R> AsRenderElements<R> for SudburyPointer
where
    R: Renderer + ImportMem + ImportDmaWl + ImportMemWl,
    <R as Renderer>::TextureId: 'static + Clone,
{
    type RenderElement = SudburyPointerElement<R>;

    fn render_elements<E: From<SudburyPointerElement<R>>>(
        &self,
        renderer: &mut R,
        location: Point<i32, Physical>,
        scale: Scale<f64>,
        alpha: f32,
    ) -> Vec<E> {
        match &self.status() {
            CursorImageStatus::Hidden => vec![],
            // Server side pointer
            CursorImageStatus::Named(_) => {
                if let Some(texture) = self.texture.as_ref().and_then(|t| t.downcast_ref()) {
                    let t = TextureRenderElement::from_texture_buffer(
                        location.to_f64(),
                        texture,
                        None,
                        None,
                        None,
                        element::Kind::Cursor,
                    );
                    let pointer = SudburyPointerElement::from(t);
                    vec![pointer.into()]
                } else {
                    panic!("No cursor!");
                }
            }
            // Client side pointer
            CursorImageStatus::Surface(surface) => render_elements_from_surface_tree(
                renderer,
                surface,
                location,
                scale,
                alpha,
                element::Kind::Cursor,
            )
            .into_iter()
            .map(E::from)
            .collect(),
        }
    }
}
