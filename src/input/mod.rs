use smithay::{
    backend::input::KeyState,
    desktop::{Space, Window},
    input::{
        keyboard::{
            xkb::keysym_get_name, FilterResult, KeyboardHandle, Keysym, KeysymHandle,
            ModifiersState, XkbConfig,
        },
        pointer::{MotionEvent, RelativeMotionEvent},
    },
    output::Output,
    utils::{Logical, Point, Rectangle, Serial},
};
use tracing::{debug, error, instrument, trace};

#[cfg(feature = "libinput")]
use crate::input::libinput::initialize_libinput;
use crate::{input::pointer::SudburyPointer, shell::focus::FocusSurface, sudbury::Sudbury};
#[cfg(feature = "libinput")]
use smithay::{
    backend::input::{Axis, AxisRelativeDirection},
    input::pointer::AxisFrame,
};

use smithay::wayland::input_method::InputMethodSeat;

#[cfg(feature = "libinput")]
mod libinput;
pub mod pointer;

#[derive(Debug)]
pub struct SudburyInput {
    _keyboard_handle: KeyboardHandle<Sudbury>,
    pub pointer: SudburyPointer,
}

#[derive(Debug)]
pub enum SudburyAction {
    Inaction,
    Exit,
    CycleFocus,
    Tint,
    FullscreenToggle,
}

fn to_action(modifiers: ModifiersState, keysym: Keysym) -> Option<SudburyAction> {
    let mod_shift = (modifiers.logo, modifiers.shift, modifiers.alt);
    match (mod_shift, keysym) {
        ((false, _, false), Keysym::F3) => Some(SudburyAction::FullscreenToggle),
        ((true, true, _), Keysym::E) => Some(SudburyAction::Exit),
        ((true, true, _), Keysym::T) => Some(SudburyAction::Tint),
        ((false, false, true), Keysym::Tab) => Some(SudburyAction::CycleFocus),
        _ => None,
    }
}

/// Handle keyboard actions.
///
/// Simple compositor actions are handled within the filter. Longer running actions, or actions
/// which require the `KbdInternal` lock are returned and handled later. The latter is a Smithay
/// design choice and could change in the future.
///
/// # Errors
///
/// This function will return an error if .
#[allow(dead_code)]
#[profiling::function]
fn handle_keyboard(
    sudbury: &mut Sudbury,
    state: KeyState,
    modifiers: &ModifiersState,
    keyhand: KeysymHandle,
) -> FilterResult<SudburyAction> {
    debug!(
        "{state:?} {modifiers:?} {}",
        keysym_get_name(keyhand.modified_sym())
    );

    let Some(action) = to_action(*modifiers, keyhand.modified_sym()) else {
        return FilterResult::Forward;
    };

    // If a key was previously intercepted, don't forward release (we didn't forward the press)
    if state == KeyState::Released {
        return FilterResult::Intercept(SudburyAction::Inaction);
    }

    // Immediate actions can occur here. Actions which require Kbd locks, or longer processing
    // should be passed back to get handled. Exit is left as an example of how to do this, though
    // it isn't strictly necessary.
    if let SudburyAction::Exit = action {
        sudbury.eventloop_signal().stop();
        FilterResult::Intercept(SudburyAction::Inaction)
    } else {
        FilterResult::Intercept(action)
    }
}

pub enum PointerMotion {
    Delta(Point<f64, Logical>),
    Absolute(Point<f64, Logical>),
}

#[derive(Clone, Default)]
pub struct MouseMovementResult<'a> {
    end_loc: Point<f64, Logical>,
    window: Option<(&'a Window, Point<i32, Logical>)>,
}

impl std::fmt::Display for MouseMovementResult<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Some(window) = self.window {
            write!(
                f,
                "{}x{} -- {:?}@{:?} ",
                self.end_loc.x,
                self.end_loc.y,
                window.0.toplevel().wl_surface(),
                window.1
            )
        } else {
            write!(f, "{}x{} No focus", self.end_loc.x, self.end_loc.y)
        }
    }
}

impl<'a> MouseMovementResult<'a> {
    fn new(
        end_loc: Point<f64, Logical>,
        window: Option<(&'a Window, Point<i32, Logical>)>,
    ) -> Self {
        Self { end_loc, window }
    }

    pub fn end_loc(&self) -> Point<f64, Logical> {
        self.end_loc
    }

    pub fn window(&self) -> Option<(&Window, Point<i32, Logical>)> {
        self.window
    }
}

impl Sudbury {
    #[instrument(level = "trace", skip(self, focus))]
    pub fn dispatch_motion<'a>(
        &mut self,
        serial: Serial,
        location: Point<f64, Logical>,
        focus: Option<(FocusSurface, Point<i32, Logical>)>,
        delta: Point<f64, Logical>,
        delta_unaccel: Point<f64, Logical>,
        utime: u64,
    ) {
        let Some(pointer) = self.seat.get_pointer() else {
            return;
        };
        let relevt = RelativeMotionEvent {
            delta,
            delta_unaccel,
            utime,
        };
        let evt = MotionEvent {
            location,
            serial,
            time: utime as u32 / 1000,
        };

        trace!("Relative movement");
        pointer.relative_motion(self, focus.clone(), &relevt);

        trace!("Absolute movement");
        pointer.motion(self, focus, &evt);
    }
}

impl SudburyInput {
    /// .
    #[allow(unused_variables)]
    pub fn new(sudbury: &mut Sudbury) -> Option<Self> {
        #[cfg(feature = "libinput")]
        initialize_libinput(
            sudbury.drm_kms.session().clone(),
            sudbury.eventloop_handle(),
        )
        .ok()?;

        let kh = sudbury
            .seat
            .add_keyboard(XkbConfig::default(), 200, 25)
            .ok()?;
        let h = sudbury.seat.add_pointer();
        Some(SudburyInput {
            _keyboard_handle: kh,
            pointer: SudburyPointer::new(h, sudbury.pointer_output_or_default()),
        })
    }

    /// Updates window focus to the pointer location
    ///
    /// Smithay default:
    /// Client only requests a grab, the compositor decides if it grants it or dismisses the popup.
    /// Pointer will continue to send enter/leave for surfaces of the client that the popup surface
    /// belongs to. Keyboard will stay exclusively on popup surface. Clicking on a surface not
    /// equal the client will dismiss the popup compositor side. Clicking on a different surface
    /// from same client will typically destroy popup client side.
    pub fn keyboard_refocus(sudbury: &mut Sudbury, serial: Serial) {
        let Some(keyboard) = sudbury.seat.get_keyboard() else {
            return;
        };
        // If there is no pointer, it's up to alt+tab switcher to change focus
        let Some(pointer) = sudbury.seat.get_pointer() else {
            return;
        };

        let inputh = sudbury.seat.input_method().clone();

        // grabbed should be set be one of: movement, DND, or grab. For any of those, keyboard focus
        // shouldn't be changed.
        // XXX: The input logic is inverted from Anvil. Not sure why Anvil works that way.
        if keyboard.is_grabbed() || inputh.keyboard_grabbed() {
            return;
        }
        if pointer.is_grabbed() {
            return;
        }

        // Set keyboard focus to the topmost surface under the pointer.
        //TODO: Check if the client is fullscreen on the output?
        let Some(input) = &sudbury.input else {
            error!("Input unexpectedly vanished");
            return;
        };
        let (loc, _) = input.borrow().pointer.loc();
        let element = sudbury
            .current_space()
            .element_under(loc)
            .map(|(w, _)| w.clone());
        sudbury.activate_window(element.as_ref());
        if let Some(ref e) = element {
            sudbury.current_space_mut().raise_element(&e.clone(), true);
        }
        keyboard.set_focus(sudbury, element.map(FocusSurface::Window), serial);
    }

    /// Truncate a location to the [`Space`].
    ///
    /// Relative mouse movements can push a pointer outside of the space's logical dimensions. This
    /// function will correct this.
    fn clipper(
        &self,
        space: &Space<Window>,
        loc: Point<f64, Logical>,
    ) -> (Point<f64, Logical>, Option<Output>) {
        if let Some(output) = space.outputs().find(|output| {
            let Some(rect) = space.output_geometry(output) else {
                return false;
            };
            // Convert the pointer into a 1x1 rectangle so the utils::Rectangle helps can assist in
            // determining if the location is going to fit on the output.
            let pointer = Rectangle::from_loc_and_size(loc, (1f64, 1f64));
            rect.to_f64().contains_rect(pointer)
        }) {
            // If the location is in an output's logical region, nothing to do
            (loc, Some(output.clone()))
        } else {
            // Otherwise, clip to the last known output
            if let Some(rect) = self
                .pointer
                .loc()
                .1
                .as_ref()
                .and_then(|o| space.output_geometry(o))
            {
                // The pointer can't live outside the monitor geometry, IOW, 1920x1080 is really
                // 1919x1079
                let rect =
                    Rectangle::from_loc_and_size(rect.loc, (rect.size.w - 1, rect.size.h - 1));
                (loc.constrain(rect.to_f64()), self.pointer.loc().1)
            } else {
                debug_assert!(false, "Output not found for {loc:#?}");
                ((-1.0f64, -1.0f64).into(), None)
            }
        }
    }

    /// Handle pointer potion given the delta
    ///
    /// Aside from handling the pointer location this function will schedule an update for
    /// composition if position has changed.
    ///
    /// Returns the clipped location of the point and the [`Option<Surface, Point>`] for Smithay to
    /// update.
    #[profiling::function]
    pub fn pointer_motion(sudbury: &mut Sudbury, motion: PointerMotion) -> MouseMovementResult {
        let Some(input) = &sudbury.input else {
            error!("Input unexpectedly vanished");
            debug_assert!(false);
            return MouseMovementResult::default();
        };
        let (clipped_loc, out) = {
            let space = sudbury.current_space();
            let input: &SudburyInput = &input.borrow();
            let loc = match motion {
                PointerMotion::Delta(delta) => input.pointer.loc().0 + delta,
                PointerMotion::Absolute(abs) => abs,
            };
            input.clipper(space, loc)
        };

        trace!(
            "Moving to {clipped_loc:?} {:#?}",
            out.clone().map(|o| o.name())
        );
        let input: &mut SudburyInput = &mut input.borrow_mut();
        if let Some(output) = out {
            let (last_loc, last_out) = input.pointer.loc();
            input.pointer.set_last_output(&output);
            if Some(&output) != last_out.as_ref() || last_loc != clipped_loc {
                sudbury.drm_kms.schedule_idle_composition(&output)
            }
        } else {
            debug!("No valid output after pointer motion?");
            debug_assert!(false);
        }

        MouseMovementResult::new(
            clipped_loc,
            sudbury.current_space().element_under(clipped_loc),
        )
    }

    /// Must be called after a pointer motion event to adjust keyboard focus
    #[instrument(level = "trace", skip(sudbury))]
    pub fn finish_pointer_event(
        sudbury: &mut Sudbury,
        ph: &smithay::input::pointer::PointerHandle<Sudbury>,
        button: Option<u32>,
        serial: Serial,
    ) {
        use crate::policy::sloppy_focus;

        const BTN_MOUSE: u32 = 0x110;
        const BTN_MIDDLE: u32 = 0x112;

        // Sloppy focus just relies on current pointer location
        if sloppy_focus() {
            SudburyInput::keyboard_refocus(sudbury, serial);
        } else if let Some(BTN_MOUSE..=BTN_MIDDLE) = button {
            SudburyInput::keyboard_refocus(sudbury, serial);
        }
        trace!("Finishing pointer event");
        ph.frame(sudbury);
    }

    /// Handle pointer axis event
    #[cfg(feature = "libinput")]
    fn pointer_axis_frame(
        &self,
        source: smithay::backend::input::AxisSource,
        time: u32,
        movement_pxx: Option<(f64, AxisRelativeDirection)>,
        movement_pxy: Option<(f64, AxisRelativeDirection)>,
        movement_stepx: Option<f64>,
        movement_stepy: Option<f64>,
    ) -> AxisFrame {
        let mut frame = AxisFrame::new(time).source(source);
        let horizontal =
            |frame: &mut AxisFrame, amt: f64, dir: AxisRelativeDirection| -> AxisFrame {
                frame
                    .relative_direction(Axis::Horizontal, dir)
                    .value(Axis::Horizontal, amt)
            };
        let vertical = |frame: &mut AxisFrame, amt: f64, dir: AxisRelativeDirection| -> AxisFrame {
            frame
                .relative_direction(Axis::Vertical, dir)
                .value(Axis::Vertical, amt)
        };
        let both = |frame: &mut AxisFrame,
                    amt: (f64, f64),
                    dir: (AxisRelativeDirection, AxisRelativeDirection)|
         -> AxisFrame {
            frame
                .relative_direction(Axis::Horizontal, dir.0)
                .value(Axis::Horizontal, amt.0)
                .relative_direction(Axis::Vertical, dir.1)
                .value(Axis::Vertical, amt.1)
        };
        match (movement_pxx, movement_pxy, movement_stepx, movement_stepy) {
            // X only movement
            (Some((x, dir)), None, Some(x120), None) => {
                horizontal(&mut frame, x, dir).v120(Axis::Horizontal, x120 as i32)
            }
            (Some((x, dir)), None, None, None) => horizontal(&mut frame, x, dir),
            // Y only movement
            (None, Some((y, dir)), None, Some(y120)) => {
                vertical(&mut frame, y, dir).v120(Axis::Vertical, y120 as i32)
            }
            (None, Some((y, dir)), None, None) => vertical(&mut frame, y, dir),
            // Both
            (Some((x, xdir)), Some((y, ydir)), Some(x120), Some(y120)) => {
                both(&mut frame, (x, y), (xdir, ydir))
                    .v120(Axis::Horizontal, x120 as i32)
                    .v120(Axis::Vertical, y120 as i32)
            }
            (Some((x, xdir)), Some((y, ydir)), None, None) => {
                both(&mut frame, (x, y), (xdir, ydir))
            }

            // Invalid
            (None, _, Some(_), _)
            | (_, None, _, Some(_))
            | (None, None, None, None)
            | (Some(_), Some(_), None, Some(_))
            | (Some(_), Some(_), Some(_), None) => panic!("Invalid axis event"),
        };

        frame
    }
}

impl Sudbury {
    pub fn output_for_pointer(&self) -> Option<&Output> {
        let Some(input) = self.input.as_ref() else {
            return None;
        };
        let input = &input.borrow();
        self.current_space()
            .output_under(input.pointer.loc().0)
            .next()
    }
    pub fn pointer_output_or_default(&self) -> &Output {
        self.output_for_pointer()
            .unwrap_or_else(|| self.current_space().outputs().next().unwrap())
    }
}
