use std::convert;

use anyhow::Context;
use smithay::{
    backend::{
        self,
        input::{
            Axis, Device, Event, InputEvent, KeyboardKeyEvent, PointerAxisEvent,
            PointerButtonEvent, PointerMotionEvent,
        },
        libinput::{LibinputInputBackend, LibinputSessionInterface},
        session::Session,
    },
    input::pointer::ButtonEvent,
    reexports::{calloop::LoopHandle, input::Libinput},
    utils::SERIAL_COUNTER,
};
use tracing::{debug, error};
use wayland_server::protocol::wl_pointer;

use crate::{
    input::{SudburyAction, SudburyInput},
    shell::focus::FocusSurface,
    sudbury::GlobalState,
};

use super::PointerMotion;

fn input_event(event: InputEvent<LibinputInputBackend>, _meta: &mut (), data: &mut GlobalState) {
    let serial = SERIAL_COUNTER.next_serial();
    match event {
        InputEvent::Keyboard { event } => {
            let Some(keyboard) = data.sudbury.seat.get_keyboard() else {
                error!("No keyboard");
                return;
            };
            let (code, state) = (event.key_code(), event.state());

            let action = keyboard.input::<SudburyAction, _>(
                &mut data.sudbury,
                code,
                state,
                serial,
                Event::time_msec(&event),
                move |sudbury, modifiers, keyhand| {
                    super::handle_keyboard(sudbury, state, modifiers, keyhand)
                },
            );
            data.sudbury
                .compositor_action(action.unwrap_or(SudburyAction::Inaction));
        }
        InputEvent::DeviceAdded { device } => {
            debug!(
                "Input device added {:?}",
                device.syspath().unwrap_or_default()
            );
        }
        InputEvent::DeviceRemoved { device: _ } => todo!(),
        InputEvent::PointerMotion { event } => {
            let Some(pointer) = data.sudbury.seat.get_pointer() else {
                return;
            };
            let new_point = SudburyInput::pointer_motion(
                &mut data.sudbury,
                PointerMotion::Delta(event.delta()),
            );
            let focus = new_point
                .window()
                .map(|(surf, point)| (FocusSurface::Window(surf.clone()), point));
            let loc = new_point.end_loc();
            data.sudbury.dispatch_motion(
                serial,
                loc,
                focus,
                event.delta(),
                event.delta_unaccel(),
                event.time(),
            );
            SudburyInput::finish_pointer_event(&mut data.sudbury, &pointer, None, serial);
        }
        InputEvent::PointerMotionAbsolute { event: _ } => todo!(),
        InputEvent::PointerButton { event } => {
            let Some(pointer) = data.sudbury.seat.get_pointer() else {
                return;
            };
            let state = wl_pointer::ButtonState::from(event.state());
            let backend_state = match state {
                wl_pointer::ButtonState::Released => backend::input::ButtonState::Released,
                wl_pointer::ButtonState::Pressed => {
                    SudburyInput::finish_pointer_event(
                        &mut data.sudbury,
                        &pointer,
                        Some(event.button()),
                        serial,
                    );
                    backend::input::ButtonState::Pressed
                }
                _ => todo!(),
            };
            if let Some(pointer) = data.sudbury.seat.get_pointer() {
                let btn = ButtonEvent {
                    serial,
                    time: event.time_msec(),
                    button: event.button_code(),
                    state: backend_state,
                };
                pointer.button(&mut data.sudbury, &btn);
                pointer.frame(&mut data.sudbury);
            }
        }
        InputEvent::PointerAxis { event } => {
            let amt_x = event
                .amount(Axis::Horizontal)
                .zip(Some(event.relative_direction(Axis::Horizontal)));
            let amt_y = event
                .amount(Axis::Vertical)
                .zip(Some(event.relative_direction(Axis::Vertical)));

            let frame = {
                let input: &SudburyInput = &data.sudbury.input.as_ref().unwrap().borrow();
                input.pointer_axis_frame(
                    event.source(),
                    event.time_msec(),
                    amt_x,
                    amt_y,
                    event.amount_v120(Axis::Horizontal),
                    event.amount_v120(Axis::Vertical),
                )
            };

            let pointer = data.sudbury.seat.get_pointer().unwrap();
            pointer.axis(&mut data.sudbury, frame);
            pointer.frame(&mut data.sudbury);
        }
        InputEvent::TouchDown { event: _ } => todo!(),
        InputEvent::TouchMotion { event: _ } => todo!(),
        InputEvent::TouchUp { event: _ } => todo!(),
        InputEvent::TouchCancel { event: _ } => todo!(),
        InputEvent::TouchFrame { event: _ } => todo!(),
        InputEvent::TabletToolAxis { event: _ } => todo!(),
        InputEvent::TabletToolProximity { event: _ } => todo!(),
        InputEvent::TabletToolTip { event: _ } => todo!(),
        InputEvent::TabletToolButton { event: _ } => todo!(),
        InputEvent::Special(_) => todo!(),
        InputEvent::GestureSwipeBegin { event: _ } => (),
        InputEvent::GestureSwipeUpdate { event: _ } => (),
        InputEvent::GestureSwipeEnd { event: _ } => (),
        InputEvent::GesturePinchBegin { event: _ } => (),
        InputEvent::GesturePinchUpdate { event: _ } => (),
        InputEvent::GesturePinchEnd { event: _ } => (),
        InputEvent::GestureHoldBegin { event: _ } => (),
        InputEvent::GestureHoldEnd { event: _ } => (),
    };
}

pub fn initialize_libinput<S>(session: S, eh: &LoopHandle<GlobalState>) -> anyhow::Result<()>
where
    S: Session + 'static + convert::From<S>,
    LibinputSessionInterface<S>: From<S>,
{
    let seat = session.seat();
    let session_interface = LibinputSessionInterface::from(session);
    let mut context = Libinput::new_with_udev(session_interface);
    if let Err(e) = context.udev_assign_seat(&seat) {
        anyhow::bail!("Failed to assign seat {e:?}");
    };
    let backend = LibinputInputBackend::new(context);

    eh.insert_source(backend, input_event)
        .map_err(|e| e.error)
        .with_context(|| "Couldn't enable libinput event generation")?;

    Ok(())
}
