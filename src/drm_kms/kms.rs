use std::{
    collections::{HashMap, HashSet},
    time::Duration,
};

use anyhow::{bail, Context};
use smithay::{
    backend::{
        allocator::{Format, Fourcc},
        drm::{
            compositor::DrmCompositor, DrmDevice, DrmDeviceFd, DrmEventMetadata, DrmEventTime,
            DrmSurface,
        },
    },
    desktop::{utils::OutputPresentationFeedback, Space, Window},
    output::{self, Mode, Output, PhysicalProperties, Subpixel, WeakOutput},
    reexports::{
        drm::{
            self,
            control::{self, connector, crtc, property, Device, PlaneType, ResourceHandle},
        },
        gbm::{self},
        wayland_protocols::wp::{
            linux_dmabuf::zv1::server::zwp_linux_dmabuf_feedback_v1,
            presentation_time::server::wp_presentation_feedback,
        },
    },
    utils::{Monotonic, Size, Time},
    wayland::dmabuf::{DmabufFeedback, DmabufFeedbackBuilder},
};
use tracing::{debug, error, info, trace, warn};

use crate::{
    drm_kms::drm::SudburyDrmCompositor,
    policy::{output_preference, OutputPropertyRequest},
    sudbury::{output_scale, scale_override, MONO_CLOCK},
};

#[cfg(feature = "dumb-allocator")]
use smithay::backend::allocator::dumb::DumbAllocator;

#[cfg(not(feature = "dumb-allocator"))]
use smithay::backend::allocator::gbm::GbmAllocator;

// The allocator and exporter should always match (GBM/dmabuf).
#[cfg(not(feature = "dumb-allocator"))]
pub type InnerDrmCompositor = DrmCompositor<
    GbmAllocator<DrmDeviceFd>,
    gbm::Device<DrmDeviceFd>,
    Option<OutputPresentationFeedback>,
    DrmDeviceFd,
>;
#[cfg(feature = "dumb-allocator")]
pub type InnerDrmCompositor =
    DrmCompositor<DumbAllocator, DrmDeviceFd, Option<OutputPresentationFeedback>, DrmDeviceFd>;

/// Get a raw property value from KMS.
///
/// <https://www.kernel.org/doc/html/latest/gpu/drm-kms.html?highlight=properties#existing-kms-properties>
///
/// # Errors
///
/// This function will return an error if either the device properties, or the `name` cannot be
/// found.
fn get_property_val(
    device: &&(impl Device + std::fmt::Debug),
    handle: impl ResourceHandle,
    name: &str,
) -> anyhow::Result<(property::Handle, property::ValueType, property::RawValue)> {
    let props = device
        .get_properties(handle)
        .with_context(|| format!("Property error for {device:?}"))?;
    let (prop_handles, values) = props.as_props_and_values();
    for (&prop, &val) in prop_handles.iter().zip(values.iter()) {
        let info = device
            .get_property(prop)
            .with_context(|| format!("Property {name}"))?;
        if Some(name) == info.name().to_str().ok() {
            let val_type = info.value_type();
            return Ok((prop, val_type, val));
        }
    }
    anyhow::bail!("No prop found {name}")
}

// Get a boolean property from KMS.
#[allow(clippy::just_underscores_and_digits)]
fn get_prop_bool(
    device: &(impl Device + std::fmt::Debug),
    handle: impl ResourceHandle,
    name: &str,
) -> bool {
    match get_property_val(&device, handle, name) {
        Ok((_0, _1, _2)) => match _1.convert_value(_2) {
            property::Value::Boolean(a) => a,
            _ => unreachable!(),
        },
        Err(e) => {
            error!("{e}");
            false
        }
    }
}

/// Get a blob property from KMS.
#[allow(clippy::just_underscores_and_digits)]
fn get_prop_blob(
    device: &(impl Device + std::fmt::Debug),
    handle: impl ResourceHandle,
    name: &str,
) -> Option<u64> {
    match get_property_val(&device, handle, name) {
        Ok((_0, _1, _2)) => match _1.convert_value(_2) {
            property::Value::Blob(a) => Some(a),
            _ => unreachable!(),
        },
        Err(e) => {
            error!("{e}");
            None
        }
    }
}

type HardwareCompositor = HashMap<WeakOutput, InnerDrmCompositor>;

/// KMS related abstraction
#[derive(Debug)]
pub struct SudburyKms {
    /// For each output, maps relationship of [`Output`] to a renderer/surface pair
    compositor: HardwareCompositor,

    #[cfg(feature = "fps")]
    fps: fps_ticker::Fps,
}

impl SudburyKms {
    #[cfg(feature = "fps")]
    pub fn fps(&self) -> &fps_ticker::Fps {
        &self.fps
    }
}

struct KmsData {
    pub crtc: crtc::Handle,
}

impl KmsData {
    /// Returns the CRTC of this [`KmsData`].
    ///
    /// This data is stored in the [`Output`] [`smithay::utils::user_data::UserDataMap`]
    pub fn crtc(&self) -> u32 {
        self.crtc.into()
    }
}

/// Get all possible CRTCs for a connector
fn crtcs(kms_device: &DrmDevice, connector: &connector::Info) -> HashSet<crtc::Handle> {
    HashSet::from_iter(
        connector
            .encoders()
            .iter()
            .flat_map(|eh| kms_device.get_encoder(*eh))
            .flat_map(|encoder_info| match kms_device.resource_handles() {
                Ok(handles) => handles.filter_crtcs(encoder_info.possible_crtcs()),
                Err(e) => {
                    error!("{e}");
                    vec![]
                }
            }),
    )
}

/// Get all connectors for the [`DrmDevice`]
fn connectors(kms_device: &DrmDevice) -> HashMap<connector::Handle, connector::Info> {
    let mut connectors: HashMap<connector::Handle, connector::Info> = HashMap::new();

    match kms_device.resource_handles() {
        Ok(rhandles) => rhandles.connectors().iter().for_each(|h| {
            if let Ok(info) = kms_device.get_connector(*h, false) {
                connectors.insert(*h, info);
            } else {
                debug!("Skipping connector handle {}", u32::from(*h));
            }
        }),
        Err(e) => {
            error!("{e}");
            return connectors;
        }
    };

    connectors
}

/// Returns the active mode for the connector if the connector is active
fn active_crtc_mode(kms_device: &DrmDevice, conn: &connector::Info) -> Option<Mode> {
    let eh = conn.current_encoder()?;
    let Ok(encoder) = kms_device.get_encoder(eh) else {
        return None;
    };
    let Ok(handles) = kms_device.resource_handles() else {
        return None;
    };
    let crtcs: Vec<crtc::Info> = handles
        .filter_crtcs(encoder.possible_crtcs())
        .iter()
        .filter(|&c| get_prop_bool(kms_device, *c, "ACTIVE"))
        .flat_map(|h| kms_device.get_crtc(*h).into_iter()) // This will silently drop any
        // failures.
        .collect();
    if crtcs.len() > 1 {
        warn!("Unexpected number of active CRTCS {crtcs:?}");
    }
    if !crtcs.is_empty() {
        let drm_mode = crtcs[0].mode()?;
        return Some(Mode {
            size: Size::from((drm_mode.size().0 as i32, drm_mode.size().1 as i32)),
            refresh: 1000 * drm_mode.vrefresh() as i32,
        });
    }
    None
}

/// Returns name of manufacturer based on EDID spec.
///
/// <https://edid.tv/manufacturer/>
fn manufacturer(vendor: &[char; 3]) -> &'static str {
    match vendor {
        ['H', 'W', 'P'] => "Hewlett Packard",
        _ => "Unknown",
    }
}

/// Obtain EDID make and model for the connector
///
/// # Errors
///
/// This function will return an error if KMS EDID property couldn't be retrieved
fn make_model(kms_device: &DrmDevice, h: connector::Handle) -> anyhow::Result<(&str, &str)> {
    let edid_prop_blob =
        get_prop_blob(kms_device, h, "EDID").with_context(|| "Failed to get EDID")?;
    let blob = kms_device.get_property_blob(edid_prop_blob)?;
    // Little endian
    let vid = ((blob[1] as u16) << 8) | blob[0] as u16;
    let vendor: [char; 3] = [
        char::from_u32((vid & 0b0111110000000000 >> 10) as u32).unwrap_or_default(),
        char::from_u32((vid & 0b0000001111100000 >> 5) as u32).unwrap_or_default(),
        char::from_u32((vid & 0b0000000000011111) as u32).unwrap_or_default(),
    ];

    Ok((manufacturer(&vendor), "TODO"))
}

/// Get a name for the connector
fn connector_name(interface: connector::Interface, id: u32) -> String {
    let if_name = match interface {
        connector::Interface::DisplayPort => "DP".to_owned(),
        connector::Interface::HDMIA => "HDMI".to_owned(),
        connector::Interface::HDMIB => "HDMI".to_owned(),
        connector::Interface::EmbeddedDisplayPort => "eDP".to_owned(),
        connector::Interface::DSI => "DSI".to_owned(),
        connector::Interface::Virtual => "VKMS".to_owned(),
        _ => todo!(),
    };

    format!("{if_name}-{id}")
}

fn create_output(
    kms_device: &DrmDevice,
    connector: (&connector::Handle, &connector::Info),
) -> Output {
    let name = connector_name(connector.1.interface(), connector.1.interface_id());

    let (phys_w, phys_h) = connector.1.size().unwrap_or((0, 0));

    let (make, model) = make_model(kms_device, *connector.0).unwrap_or(("unknown", "unknown"));
    let output = Output::new(
        name,
        PhysicalProperties {
            size: (phys_w as i32, phys_h as i32).into(),
            subpixel: Subpixel::None, // Sub-pixel is old KMS found by the GETCONNECTOR ioctl.
            make: make.to_string(),
            model: model.to_string(),
        },
    );

    for mode in connector.1.modes() {
        let m = output::Mode {
            size: (mode.size().0 as i32, mode.size().1 as i32).into(),
            refresh: 1000 * mode.vrefresh() as i32,
        };
        output.add_mode(m);
        if mode
            .mode_type()
            .contains(drm::control::ModeTypeFlags::PREFERRED)
        {
            output.set_preferred(m);
        }
    }

    output
}

fn find_crtc(
    kms_device: &DrmDevice,
    mode: Mode,
    connector: (&connector::Handle, &connector::Info),
) -> anyhow::Result<(crtc::Handle, DrmSurface)> {
    // Find a good CRTC for the added connector
    let all_crtcs = crtcs(kms_device, connector.1);
    let mut failed_crtcs: HashSet<crtc::Handle> = HashSet::new();
    for crtc in &all_crtcs {
        let hz = |m: &control::Mode| -> f64 {
            m.clock() as f64 * 1000.0 / (m.hsync().2 as f64 * m.vsync().2 as f64)
        };
        let current_refresh: f64 = f64::from(mode.refresh);
        // This will find the *actual* FPS that's closest to the requested for the given size
        if let Some(mode) = connector
            .1
            .modes()
            .iter()
            .filter(|m| m.size().0 as i32 == mode.size.w && m.size().1 as i32 == mode.size.h)
            .reduce(|a, b| {
                if (hz(a) - current_refresh).abs() < (hz(b) - current_refresh).abs() {
                    a
                } else {
                    b
                }
            })
        {
            match kms_device.create_surface(*crtc, *mode, &[*connector.0]) {
                Ok(surf) => return Ok((*crtc, surf)), //surface.link(signaler);
                Err(e) => {
                    error!("{e}");
                    continue;
                }
            }
        } else {
            failed_crtcs.insert(*crtc);
        }
    }

    debug_assert_eq!(all_crtcs, failed_crtcs);
    bail!("No valid crtc found for connector {:?}", connector.0);
}

fn try_requested_mode(
    output: &Output,
    prop_request: &OutputPropertyRequest,
) -> anyhow::Result<Option<Mode>> {
    // Does output support this mode at all?
    let modes = output.modes();
    let requested_mode = modes.iter().find(|mode| prop_request.is_match(**mode));
    let requested_scale = (prop_request.scale() != 0.0)
        .then(|| output::Scale::Fractional(prop_request.scale() as f64));

    if requested_mode.is_none() && requested_scale.is_none() {
        bail!("Empty user mode request for {}", output.name())
    }

    let new_mode = requested_mode.and_then(|m| {
        output.set_preferred(*m);
        requested_mode.copied()
    });
    output.change_current_state(new_mode, None, requested_scale, None);

    Ok(new_mode)
}

fn set_mode(
    drm: &DrmDevice,
    output: &Output,
    info: &connector::Info,
    request: Option<OutputPropertyRequest>,
) {
    if let Some(request) = request {
        match try_requested_mode(output, &request) {
            Ok(m) => {
                info!("Mode changed to {m:?} per user request.");
                return;
            }
            Err(e) => warn!(
                "Couldn't match user requested mode: {e} {}: {request}",
                output.name()
            ),
        }
    }

    // If the CRTC is active though, use that mode
    if let Some(active_mode) = active_crtc_mode(drm, info) {
        if output.modes().contains(&active_mode) {
            output.set_preferred(active_mode);
        } else {
            error!("Current mode {:?}, not in list of modes", active_mode);
        }
    }

    // This should only happen on inactive CRTCs.
    if output.preferred_mode() != output.current_mode() || scale_override() {
        output.change_current_state(
            output.preferred_mode(),
            None,
            output_scale(&output.name()),
            None,
        );
    }
}

#[allow(dead_code)]
fn biggest_mode(_drm: &DrmDevice, output: &Output, _info: &connector::Info) {
    let pmode = output.preferred_mode().unwrap();
    let m = output
        .modes()
        .iter()
        .fold(pmode, |acc, x| if acc.size.w < x.size.w { *x } else { acc });

    debug!("Setting mode to {m:?}");
    output.change_current_state(Some(m), None, None, None);
}

fn map_output(space: &mut Space<Window>, output: &Output) {
    // If there are multiple outputs, stride it horizontally from left to right.
    let width = space.outputs().fold(0, |acc, out| {
        acc + out
            .current_mode()
            .unwrap_or(Mode {
                size: (0, 0).into(),
                refresh: 0,
            })
            .size
            .w
    });
    space.map_output(output, (width, 0));
    output.change_current_state(None, None, None, Some((width, 0).into()));
}

#[cfg(not(feature = "dumb-allocator"))]
fn allocator_exporter<'a>(
    _dev: &DrmDevice,
    gbm: Option<&'a gbm::Device<DrmDeviceFd>>,
) -> (
    GbmAllocator<DrmDeviceFd>,
    &'a gbm::Device<DrmDeviceFd>,
    Option<&'a gbm::Device<DrmDeviceFd>>,
) {
    use smithay::reexports::gbm::BufferObjectFlags;

    assert!(gbm.is_some());
    let gbm = gbm.unwrap();
    (
        GbmAllocator::new(
            gbm.clone(),
            BufferObjectFlags::RENDERING | BufferObjectFlags::SCANOUT,
        ),
        gbm,
        Some(gbm),
    )
    //        (DumbAllocator::new(dev.device_fd().clone()), dev.clone())
}

#[cfg(feature = "dumb-allocator")]
fn allocator_exporter<'a, 'b>(
    dev: &'a DrmDevice,
    gbm: Option<&'b gbm::Device<DrmDeviceFd>>,
) -> (
    DumbAllocator,
    &'a DrmDeviceFd,
    Option<&'b gbm::Device<DrmDeviceFd>>,
) {
    assert!(gbm.is_none());
    (
        DumbAllocator::new(dev.device_fd().clone()),
        dev.device_fd(),
        None,
    )
}

/// Creates a compositor managing all outputs and planes
fn enumerate_outputs(
    kms_device: &DrmDevice,
    gbm_device: Option<&gbm::Device<DrmDeviceFd>>,
    space: &mut Space<Window>,
    formats: &HashSet<Format>,
) -> anyhow::Result<HardwareCompositor> {
    let mut connected = connectors(kms_device);

    connected.retain(|_h, i| i.state() == connector::State::Connected);
    trace!(connectors = ?connected);

    let mut compositor = HashMap::with_capacity(2);
    for (connector_handle, connector_info) in connected {
        // Create an output for the connected device
        let output = create_output(kms_device, (&connector_handle, &connector_info));

        // First try to satisfy any user request
        let prefs = output_preference(output.name().as_str());

        set_mode(kms_device, &output, &connector_info, prefs);

        map_output(space, &output);

        let scanout = match find_crtc(
            kms_device,
            output.current_mode().with_context(|| "Couldn't get mode")?,
            (&connector_handle, &connector_info),
        ) {
            Ok((crtc, surf)) => {
                output.user_data().insert_if_missing(|| KmsData { crtc });
                surf
            }
            Err(e) => {
                error!("{e}");
                continue;
            }
        };

        let planes = scanout.planes().clone();
        let (allocator, exporter, gbm_allocator) = allocator_exporter(kms_device, gbm_device);

        let drm_compositor = match DrmCompositor::new(
            &output,
            scanout,
            Some(planes.clone()),
            allocator,
            exporter.clone(),
            &[
                Fourcc::Abgr2101010,
                Fourcc::Argb2101010,
                Fourcc::Abgr8888,
                Fourcc::Argb8888,
                Fourcc::Xrgb8888,
            ],
            formats.to_owned(),
            kms_device.cursor_size(),
            gbm_allocator.cloned(),
        ) {
            Ok(r) => r,
            Err(e) => {
                error!("{e}");
                continue;
            }
        };

        compositor.insert(output.downgrade(), drm_compositor);
    }

    Ok(compositor)
}

impl SudburyKms {
    /// .
    ///
    /// Recall that a CRTC was assigned to an output/compositor when it was enumerated. That CRTC
    /// is what has the format limitations and so formats end up effectively being per
    /// output/compositor.
    ///
    /// # Panics
    ///
    /// Panics if .
    fn formats(
        &self,
        output: &Output,
        plane_type: PlaneType,
        zpos: Option<i32>,
    ) -> HashSet<Format> {
        let Some(compositor) = self.compositor.get(&output.downgrade()) else {
            return HashSet::from([]);
        };

        let surface = compositor.surface();
        let planes = surface.planes();

        match plane_type {
            PlaneType::Overlay => {
                if let Some(z) = zpos {
                    let Some(overlay) = planes.overlay.get(z as usize) else {
                        return HashSet::from([]);
                    };
                    overlay.formats.clone()
                } else {
                    planes
                        .overlay
                        .iter()
                        .flat_map(|p| p.formats.clone())
                        .collect()
                }
            }
            PlaneType::Primary => planes.primary.formats.clone(),
            PlaneType::Cursor => planes
                .cursor
                .as_ref()
                .map_or(HashSet::from([]), |c| c.formats.clone()),
        }
    }

    /// Returns all scanout formats of this [`SudburyKms`].
    pub fn scanout_formats(&self) -> HashSet<Format> {
        let mut formats = HashSet::new();
        for o in self.outputs() {
            formats.extend(self.formats(&o, PlaneType::Primary, None));
            formats.extend(self.formats(&o, PlaneType::Overlay, None));
            formats.extend(self.formats(&o, PlaneType::Cursor, None));
        }

        formats
    }

    fn kms_device(&self) -> &DrmDeviceFd {
        self.compositor
            .iter()
            .nth(0)
            .unwrap()
            .1
            .surface()
            .device_fd()
    }

    pub fn dmabuf_feedback(&self, dfb: DmabufFeedbackBuilder) -> DmabufFeedback {
        // Some of this is overkill since only one device is supported. However if render node
        // support is added, or multi-gpu, the chosen device matters.
        // The scanout tranche should use the same device as specified
        dfb.add_preference_tranche(
            self.kms_device().dev_id().unwrap(),
            Some(zwp_linux_dmabuf_feedback_v1::TrancheFlags::Scanout),
            self.scanout_formats(),
        )
        .build()
        .unwrap()
    }

    pub(super) fn new(drm: &SudburyDrmCompositor, space: &mut Space<Window>) -> Self {
        #[cfg(feature = "dumb-allocator")]
        let gbmdev: Option<gbm::Device<DrmDeviceFd>> = None;
        #[cfg(not(feature = "dumb-allocator"))]
        let gbmdev = gbm::Device::new(drm.drm_device().device_fd().clone())
            .map_err(|e| error!("Failed to get GBM device ({e}). Hardware cursor will be disabled"))
            .ok();
        let compositor = match enumerate_outputs(
            drm.drm_device(),
            gbmdev.as_ref(),
            space,
            &drm.renderer().render_formats(),
        ) {
            Ok(o) => o,
            Err(_) => HashMap::new(),
        };
        Self {
            compositor,
            #[cfg(feature = "fps")]
            fps: fps_ticker::Fps::with_window_len(300),
        }
    }

    /// Initiates a swap for this [`SudburyKms`].
    ///
    /// # Errors
    ///
    /// This function will return an error if .
    #[profiling::function]
    pub fn swap(
        &mut self,
        output: &Output,
        feedback: OutputPresentationFeedback,
    ) -> anyhow::Result<()> {
        let scanout = self.scanout_data(output);
        scanout
            .queue_frame(Some(feedback))
            .with_context(|| "Swap fail")
    }

    /// Returns the outputs of this [`SudburyKms`].
    pub(super) fn outputs(&self) -> Vec<Output> {
        let mut x: Vec<Output> = self
            .compositor
            .iter()
            .filter_map(|(o, _)| o.upgrade())
            .collect();
        x.sort_by(|a, b| {
            a.user_data()
                .get::<KmsData>()
                .map(|kmsdata| kmsdata.crtc())
                .cmp(&b.user_data().get::<KmsData>().map(|kmsdata| kmsdata.crtc()))
        });
        x
    }

    /// Returns the compositors of this [`SudburyKms`].
    pub fn compositors_mut(&mut self) -> Vec<&mut InnerDrmCompositor> {
        self.compositor.values_mut().collect()
    }

    #[allow(dead_code)]
    fn get_output(&self, crtc: crtc::Handle) -> Option<Output> {
        self.compositor
            .iter()
            .filter_map(|(o, _)| o.upgrade())
            .find(|y| {
                y.user_data()
                    .get::<KmsData>()
                    .filter(|kmsdata| kmsdata.crtc() == u32::from(crtc))
                    .is_some()
            })
    }

    pub(super) fn scanout_data(&mut self, output: &Output) -> &mut InnerDrmCompositor {
        self.compositor.get_mut(&output.downgrade()).unwrap()
    }

    fn present(
        output: &Output,
        feedback: Option<OutputPresentationFeedback>,
        flip_info: &DrmEventMetadata,
    ) {
        let Some(mut presentation) = feedback else {
            error!("Presentation Feedback missing");
            return;
        };

        let (c, f) = if let DrmEventTime::Monotonic(m) = flip_info.time {
            (
                Time::<Monotonic>::from(m),
                wp_presentation_feedback::Kind::Vsync
                    | wp_presentation_feedback::Kind::HwClock
                    | wp_presentation_feedback::Kind::HwCompletion,
            )
        } else {
            MONO_CLOCK.with(|clock| (clock.now(), wp_presentation_feedback::Kind::Vsync))
        };

        let refresh = output
            .current_mode()
            .map(|mode| Duration::from_secs_f64(1_000f64 / mode.refresh as f64))
            .unwrap_or_default();

        presentation.presented(c, refresh, flip_info.sequence as u64, f);
    }

    #[profiling::function]
    pub(super) fn vblank(&mut self, crtc: crtc::Handle, meta: &DrmEventMetadata) -> Option<Output> {
        #[cfg(feature = "fps")]
        self.fps.tick();
        if let Some(mut output) = self.compositor.iter_mut().find(|(weakout, _)| {
            weakout
                .upgrade()
                .and_then(|output| {
                    output
                        .user_data()
                        .get::<KmsData>()
                        .map(|kdata| kdata.crtc() == crtc.into())
                })
                .is_some()
        }) {
            // Let Smithay know that we're done with the frame
            let gbm = &mut output.1;
            match gbm.frame_submitted() {
                Ok(pres) => {
                    if let Some(out) = output.0.upgrade() {
                        let pres = pres.flatten();
                        Self::present(&out, pres, meta);
                        return Some(out);
                    }
                }
                Err(e) => {
                    error!("Frame submission: {e} (missed frame_queue)");
                    return None;
                }
            };
        } else {
            warn!("Couldn't find CRTC to handle vblank")
        }
        None
    }
}
