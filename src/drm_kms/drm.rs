//! The [`SudburyDrmCompositor`] puts surfaces onto a displayable surface via GPU copy, or direct
//! scanout.
use std::{
    cell::RefCell,
    collections::{HashMap, HashSet},
    os::fd::{FromRawFd, IntoRawFd, OwnedFd},
    path::PathBuf,
    rc::Rc,
    sync::Mutex,
    time::{Duration, SystemTime},
};

use anyhow::Context;
use smithay::{
    backend::{
        allocator::{self, dmabuf::Dmabuf},
        drm::{
            compositor::{self, RenderFrameError, RenderFrameResult},
            DrmDevice, DrmDeviceFd, DrmDeviceNotifier, DrmEventTime, DrmNode, Framebuffer,
        },
        egl::{native::EGLSurfacelessDisplay, EGLContext, EGLDevice, EGLDisplay},
        renderer::{
            element::{
                surface::WaylandSurfaceRenderElement, AsRenderElements, RenderElementStates,
            },
            gles::{Capability, GlesError, GlesRenderer},
            sync::SyncPoint,
            Bind, ImportAll, ImportDma,
        },
        session::Session,
    },
    desktop::{
        space::{self, SpaceRenderElements},
        Space, Window,
    },
    input::pointer::{CursorImageAttributes, CursorImageStatus},
    output::{Output, WeakOutput},
    reexports::rustix::fs::OFlags,
    render_elements,
    utils::{DeviceFd, Logical, Point, Scale},
    wayland,
};
use tracing::{info, trace};

use crate::{
    drm_kms::kms::{InnerDrmCompositor, SudburyKms},
    input::{
        pointer::{SudburyPointer, SudburyPointerElement},
        SudburyInput,
    },
    sudbury::{clear_color, MONO_CLOCK},
};

#[cfg(feature = "pixman-renderer")]
use smithay::backend::renderer::pixman::{PixmanError, PixmanRenderer};

/// Helper to determine if a client is fullscreen on [`Output`]
///
/// This used to be used for different rendering path but is no longer needed there. Since it may
/// be useful again the future, it remains.
#[allow(dead_code)]
fn find_fullscreen<'a>(spaces: &'a [&Space<Window>], output: &Output) -> Option<&'a Window> {
    for space in spaces.iter() {
        for window in space.elements() {
            if space.outputs_for_element(window).contains(output) {
                let toplevel = window.toplevel();
                if toplevel.current_state().fullscreen_output.is_some() {
                    return Some(window);
                }
            }
        }
    }
    None
}

/// Calculates the time from the `DrmEvent` until now.
pub fn duration_since(time: &DrmEventTime) -> Duration {
    match time {
        DrmEventTime::Monotonic(m) => MONO_CLOCK.with(|clock| {
            let now: Duration = clock.now().into();
            now.saturating_sub(*m)
        }),

        DrmEventTime::Realtime(r) => SystemTime::now().duration_since(*r).unwrap_or_default(),
    }
}

/// "Abstraction" over different types of renderers supported by Sudbury.
#[derive(Debug)]
pub enum SudburyRenderer {
    Gles(GlesRenderer),
    #[cfg(feature = "pixman-renderer")]
    #[allow(dead_code)]
    Pixman(PixmanRenderer),
}

impl std::fmt::Display for SudburyRenderer {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SudburyRenderer::Gles(_) => write!(f, "Gles Renderer"),
            #[cfg(feature = "pixman-renderer")]
            SudburyRenderer::Pixman(_) => write!(f, "Pixman Renderer"),
        }
    }
}

struct SudburyComposition {
    damage: bool,
    states: RenderElementStates,
    sync: Option<SyncPoint>,
}

impl SudburyComposition {
    fn new<B, F, E>(result: RenderFrameResult<B, F, E>) -> Self
    where
        B: allocator::Buffer,
        F: Framebuffer,
    {
        let sync = match result.primary_element {
            compositor::PrimaryPlaneElement::Swapchain(pse) => Some(pse.sync),
            compositor::PrimaryPlaneElement::Element(_) => None,
        };
        SudburyComposition {
            damage: result.damage.is_some(),
            states: result.states,
            sync,
        }
    }
}

macro_rules! sbry_render_frame {
    ($rtype:ty, $rndrr:ident, $compositor:ident, $space:ident, $output:ident, $pointer:ident, $scale:ident) => {{
        let mut elements: Vec<SudburyRenderElements<$rtype, _>> =
            $pointer.map_or(vec![], |($pointer, loc)| {
                $pointer
                    .render_elements::<SudburyRenderElements<$rtype, WaylandSurfaceRenderElement<_>>>(
                        $rndrr,
                        loc.to_physical_precise_round($scale),
                        $scale,
                        1.0)
        });

        elements.extend(
            space::space_render_elements::<$rtype, Window, _>(
                $rndrr,
                [$space],
                $output,
                1.0,
            )
            .unwrap_or_default()
            .into_iter()
            .map(SudburyRenderElements::Space),
        );
        $compositor
            .render_frame::<_, _>($rndrr, &elements, clear_color())
            .map(SudburyComposition::new)
            .map_err(|err| match err {
                RenderFrameError::PrepareFrame(err) => todo!("{err}"),
                RenderFrameError::RenderFrame(err) => todo!("{err}"),
            })
    }};
}

#[derive(Debug, thiserror::Error)]
pub enum DmabufImportError {
    /// Unable to import buffer into Gles
    #[error("Failed to import dmabuf into GLES")]
    DmabufImportGlesError(#[source] GlesError),
    /// Unable to import buffer into Pixman
    #[cfg(feature = "pixman-renderer")]
    #[error("Failed to import dmabuf into Pixman")]
    DmabufImportPixmanError(#[source] PixmanError),
}

impl SudburyRenderer {
    /// Wrapper for importing a dmabuf into the renderer
    ///
    /// # Errors
    ///
    /// This function will return an error if importing fails.
    pub fn import_dmabuf(&mut self, dmabuf: &Dmabuf) -> Result<(), DmabufImportError> {
        match self {
            SudburyRenderer::Gles(g) => g
                .import_dmabuf(dmabuf, None)
                .map(|_| ())
                .map_err(DmabufImportError::DmabufImportGlesError),
            #[cfg(feature = "pixman-renderer")]
            SudburyRenderer::Pixman(p) => p
                .import_dmabuf(dmabuf, None)
                .map(|_| ())
                .map_err(DmabufImportError::DmabufImportPixmanError),
        }
    }

    /// Wrapper for rendering elements on the space for the given output.
    ///
    /// This effectively wraps [`render_elements`] and [`render_frame`]
    ///
    /// # Errors
    ///
    /// This function will return an error if [`DrmCompositor`] fails.
    fn render_frame(
        &mut self,
        compositor: &mut InnerDrmCompositor,
        space: &Space<Window>,
        output: &Output,
        scale: Scale<f64>,
        pointer: Option<(&SudburyPointer, Point<f64, Logical>)>,
    ) -> anyhow::Result<SudburyComposition> {
        match self {
            SudburyRenderer::Gles(ref mut g) => {
                sbry_render_frame!(GlesRenderer, g, compositor, space, output, pointer, scale)
            }
            #[cfg(feature = "pixman-renderer")]
            SudburyRenderer::Pixman(ref mut p) => {
                sbry_render_frame!(PixmanRenderer, p, compositor, space, output, pointer, scale)
            }
        }
    }

    /// Wrapper to initialize the pointer's texture
    pub fn set_icon(&mut self, pointer: &mut SudburyPointer) {
        match self {
            SudburyRenderer::Gles(ref mut g) => pointer.icon(g),
            #[cfg(feature = "pixman-renderer")]
            SudburyRenderer::Pixman(ref mut p) => pointer.icon(p),
        }
    }

    /// Returns the formats this [`SudburyRenderer`] can render to.
    pub fn render_formats(&self) -> HashSet<allocator::Format> {
        match self {
            SudburyRenderer::Gles(g) => {
                <GlesRenderer as Bind<Dmabuf>>::supported_formats(g).unwrap()
            }
            #[cfg(feature = "pixman-renderer")]
            SudburyRenderer::Pixman(p) => {
                <PixmanRenderer as Bind<Dmabuf>>::supported_formats(p).unwrap()
            }
        }
    }

    /// Returns the formats this [`SudburyRenderer`] can texture from.
    pub fn texture_formats(&self) -> HashSet<allocator::Format> {
        match self {
            SudburyRenderer::Gles(g) => g.dmabuf_formats().collect(),
            #[cfg(feature = "pixman-renderer")]
            SudburyRenderer::Pixman(p) => p.dmabuf_formats().collect(),
        }
    }
}

/// State for the compositor (GPU rendered or scanned out) used by [`Sudbury`]
#[derive(Debug)]
pub struct SudburyDrmCompositor {
    renderer: SudburyRenderer,
    damage: HashMap<WeakOutput, RenderElementStates>,
    input: Option<Rc<RefCell<SudburyInput>>>,
    drm_device: DrmDevice,
}

/// Create a handle to a DRM device.
///
/// Failure is determined mostly by the session. For null sessions, failure is most likely due to
/// not being first and therefore not getting DRM master.
fn create_drm_device<T: Session>(
    session: &mut T,
    drm_node: PathBuf,
) -> anyhow::Result<(DrmDevice, DrmDeviceNotifier)>
where
    <T as Session>::Error: std::marker::Send,
    <T as Session>::Error: std::error::Error,
    <T as Session>::Error: Sync,
    <T as Session>::Error: 'static,
{
    let fd = session
        .open(
            &drm_node,
            OFlags::RDWR | OFlags::CLOEXEC | OFlags::NOCTTY | OFlags::NONBLOCK,
        )
        .with_context(|| format!("Failed to open {:?}", drm_node))?
        .into_raw_fd();

    let device_fd: DeviceFd = unsafe { DeviceFd::from_raw_fd(fd) };
    let device_fd = DrmDeviceFd::new(device_fd);

    let drm = DrmDevice::new(device_fd, true)
        .with_context(|| format!("Couldn't access {:?}", drm_node))?;

    // AFAIK, there's no reason to make atomic drivers exclusive.
    if !drm.0.is_atomic() {
        unimplemented!("Legacy modestting implementations aren't supported");
    }

    Ok(drm)
}

/// Create an EGL context and GBM device for doing compositor GPU operations
///
/// Optionally create a handle to a render node. Render nodes are currently unused by Sudbury.
fn create_egl_context(render_node: bool) -> anyhow::Result<(EGLContext, Option<DrmNode>)> {
    // Create an `EGLDisplay` using
    // `https://registry.khronos.org/EGL/extensions/MESA/EGL_MESA_platform_surfaceless.txt`
    // When debugging on Linux, this can be changed to use GBM.
    let egl_display = EGLDisplay::new::<EGLSurfacelessDisplay>(EGLSurfacelessDisplay)
        .with_context(|| "Couldnt create an EGLDisplay")?;

    // Obtain the [Render Node](https://www.kernel.org/doc/html/latest/gpu/drm-uapi.html#render-nodes).
    // Sudbury doesn't currently use the render node.
    let render_node = if render_node {
        let egl_device = EGLDevice::device_for_display(&egl_display)
            .with_context(|| "Not EGLDisplay for device")?;
        trace!(device = ?egl_device);

        let render_node = egl_device.try_get_render_node()?;
        trace!(?render_node);
        render_node
    } else {
        None
    };

    let context = EGLContext::new(&egl_display).with_context(|| "Couldn't create EGL context")?;
    trace!(?context);

    Ok((context, render_node))
}

render_elements! {
    SudburyRenderElements<R, E> where R: ImportAll;
    Space=SpaceRenderElements<R, E>,
    Window=WaylandSurfaceRenderElement<R>,
    Pointer=SudburyPointerElement<R>,
}

fn create_gles_renderer(context: EGLContext) -> Option<GlesRenderer> {
    match unsafe { GlesRenderer::supported_capabilities(&context) } {
        Ok(mut caps) => {
            // Disable color transformations until HDR support
            info!("Disabling color transformations");
            caps.retain(|c| *c != Capability::ColorTransformations);
            unsafe { GlesRenderer::with_capabilities(context, caps) }.ok()
        }
        Err(e) => {
            info!("Failed to get renderer capabilities ({e})");
            unsafe { GlesRenderer::new(context) }.ok()
        }
    }
}

impl SudburyDrmCompositor {
    /// Handler for new DRM devices
    ///
    /// Enumeration creates and instantiates all the relevant state for rendering the compositor's
    /// frames. The general operations is:
    /// 1. Create a [`DrmDevice`] from the current session
    /// 2. Create a Surfaceless [`EGLDisplay`]. This is used for `eglSwapBuffer`.
    /// 3. Create an [`EGLContext`] from the [`EGLDisplay`]. `EGLContexts` are used for rendering
    ///    operations.
    /// 4. Create a [`GlesRenderer`] or [`PixmanRenderer`] with the initialized [`EGLContext`]
    /// 5. Enumerate and initialize all existing outputs
    /// 6. Set up dispatcher for DRM events ("VBlank")
    /// 7. Create the global [`DmabufState`] and bind it to the renderer.
    pub fn new<T: Session>(
        session: &mut T,
        path: PathBuf,
        pixman: bool,
    ) -> (Self, DrmDeviceNotifier)
    where
        <T as Session>::Error: std::marker::Send,
        <T as Session>::Error: std::error::Error,
        <T as Session>::Error: Sync,
        <T as Session>::Error: 'static,
    {
        let device = match create_drm_device(session, path) {
            Ok(device) => device,
            Err(e) => {
                panic!("Failed to create the DRM device {e}");
            }
        };
        trace!(?device);

        let renderer = match pixman {
            #[cfg(feature = "pixman-renderer")]
            true => SudburyRenderer::Pixman(PixmanRenderer::new().unwrap()),
            _ => {
                let Ok((egl_context, _)) = create_egl_context(false) else {
                    panic!("Couldn't create EGL context");
                };
                trace!(?egl_context);

                // The formats that can be textured from are useful for sampling from clients. The formats
                // that can be rendered to are useful for the composition step and what can be fed to the
                // display engine. The [`DrmCompositor`] will create the proper intersection of render
                // formats and scanout formats. Therefore, only the texture formats need to be used.
                trace!(
                    "Texture formats {:?}",
                    egl_context.display().dmabuf_texture_formats()
                );
                trace!(
                    "Render formats {:?}",
                    egl_context.display().dmabuf_render_formats()
                );
                SudburyRenderer::Gles(
                    create_gles_renderer(egl_context).expect("Failed to create GLES renderer "),
                )
            }
        };

        (
            Self {
                renderer,
                damage: HashMap::with_capacity(3), // Reasonable #outputs
                input: None,
                drm_device: device.0,
            },
            device.1,
        )
    }

    /// Composites surfaces for [`Output`]. May copy or directly scanout.
    ///
    /// # Errors
    ///
    /// This function will return an error if .
    pub(super) fn composite(
        &mut self,
        kms: &mut SudburyKms,
        space: &Space<Window>,
        output: &Output,
    ) -> anyhow::Result<(bool, RenderElementStates, Option<OwnedFd>)> {
        let compositor = kms.scanout_data(output);

        let scale: Scale<f64> = output.current_scale().fractional_scale().into();

        let damage = if let Some(input) = &self.input {
            let pointer = &input.borrow().pointer;
            // Hotspot is handled for us by Smithay
            let hotspot = if let CursorImageStatus::Surface(ref surface) = pointer.status() {
                wayland::compositor::with_states(surface, |states| {
                    states
                        .data_map
                        .get::<Mutex<CursorImageAttributes>>()
                        .unwrap()
                        .lock()
                        .unwrap()
                        .hotspot
                })
            } else {
                (0, 0).into()
            };

            // This logic is copied from Anvil.
            let loc = pointer.loc().0
                - space.output_geometry(output).unwrap().loc.to_f64()
                - hotspot.to_f64();

            self.renderer
                .render_frame(compositor, space, output, scale, Some((pointer, loc)))
        } else {
            self.renderer
                .render_frame(compositor, space, output, scale, None)
        };

        damage.map(|result| {
            (
                result.damage,
                result.states,
                result.sync.and_then(|s| s.export()),
            )
        })
    }

    pub fn set_input(&mut self, input: Rc<RefCell<SudburyInput>>) {
        self.input = Some(input);
    }

    pub fn renderer_mut(&mut self) -> &mut SudburyRenderer {
        &mut self.renderer
    }

    pub fn drm_device(&self) -> &DrmDevice {
        &self.drm_device
    }

    pub fn renderer(&self) -> &SudburyRenderer {
        &self.renderer
    }

    pub fn take_damage(&mut self, woutput: &WeakOutput) -> Option<RenderElementStates> {
        self.damage.remove(woutput)
    }

    pub fn add_damage(&mut self, woutput: WeakOutput, states: RenderElementStates) {
        if let Some(old_damage) = self.damage.insert(woutput, states) {
            trace!("Lost track of damage {old_damage:#?}");
        }
    }
}
