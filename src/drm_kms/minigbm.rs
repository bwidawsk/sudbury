//! Wrappers for minigbm
//!
//! Because Smithay depends on GBM, and ChromeOS uses minigbm it's sometimes required to support
//! certain GBM functions.

use std::ffi::c_void;

use gbm_sys::{
    gbm_bo_get_height, gbm_bo_get_modifier, gbm_bo_get_stride, gbm_bo_get_width, gbm_bo_map,
    gbm_bo_transfer_flags, gbm_bo_unmap,
};
use smithay::reexports::gbm::Modifier;

#[no_mangle]
pub extern "C" fn gbm_bo_write(bo: *mut gbm_sys::gbm_bo, buf: *const c_void, count: usize) -> i32 {
    unsafe {
        // Non-linear buffers are unsupported
        if gbm_bo_get_modifier(bo) != <Modifier as Into<u64>>::into(Modifier::Linear) {
            return -1;
        }

        // This is needed to avoid the clippy null ptr warning
        let mut address: *mut c_void = 0x0 as _;

        // unwrap is safe since we'll never be on a 16b platform
        let width: usize = gbm_bo_get_width(bo).try_into().unwrap();
        let height: usize = gbm_bo_get_height(bo).try_into().unwrap();
        let stride: usize = gbm_bo_get_stride(bo).try_into().unwrap();

        let bo_map = gbm_bo_map(
            bo,
            0,
            0,
            gbm_bo_get_width(bo),
            gbm_bo_get_height(bo),
            gbm_bo_transfer_flags::GBM_BO_TRANSFER_WRITE,
            &mut gbm_bo_get_stride(bo) as *mut _,
            &mut address as *mut _,
        );

        if bo_map.is_null() {
            gbm_bo_unmap(bo, address);
            return -1;
        }

        if stride != 0 && stride != width {
            let mut src = buf;
            let mut map = bo_map;
            for _r in 0..height {
                std::ptr::copy_nonoverlapping(src, map, width);
                src = src.add(stride);
                map = map.add(stride);
            }
        } else {
            std::ptr::copy_nonoverlapping(buf, bo_map, count);
        }
        gbm_bo_unmap(bo, address);
    }
    0
}
