//! An abstraction over a renderer based on DRM with KMS operations.
//!
//! The DRM renderer encapsulates GLES or Vulkan based GPU rendering, while KMS would be either
//! EGL or Vulkan WSI. The GLES renderer has several extensions as dependencies. When possible,
//! composition will occur directly and without the need for copying.
//!
//! TODO: Support Vulkan
use anyhow::Context;
#[cfg(feature = "libseat")]
use smithay::backend::session::libseat::LibSeatSession;
use smithay::{
    backend::{
        allocator::dmabuf::Dmabuf,
        drm::{DrmError, DrmEvent, DrmEventMetadata},
        renderer::{
            element::{self, RenderElementStates},
            DebugFlags,
        },
        session::Session,
        udev::{UdevBackend, UdevEvent},
    },
    desktop::{
        utils::{
            self, surface_presentation_feedback_flags_from_states, surface_primary_scanout_output,
            OutputPresentationFeedback,
        },
        Space, Window,
    },
    output::{Output, WeakOutput},
    reexports::{
        calloop::{self, timer::Timer, RegistrationToken},
        wayland_server::DisplayHandle,
    },
    wayland::dmabuf::{DmabufFeedback, DmabufFeedbackBuilder, DmabufState},
};
use std::{cell::RefCell, path::PathBuf, rc::Rc, time::Duration};
use tracing::{debug_span, error, info, instrument, trace, warn};

#[cfg(not(feature = "libseat"))]
use crate::session::null::NullSession;
use crate::{
    drm_kms::kms::SudburyKms,
    input::SudburyInput,
    sudbury::{composition_split, frame_throttle, Sudbury},
    sudbury::{GlobalState, MONO_CLOCK},
};
#[cfg(feature = "libseat")]
use smithay::backend::session::Event::{ActivateSession, PauseSession};

use self::drm::SudburyDrmCompositor;

mod drm;
mod kms;
#[cfg(feature = "minigbm")]
mod minigbm;

/// State of [`SudburyDrmRenderer`] device, and [`SudburyKms`] controls
///
/// It's kind of a mid-layer. Look the other way. Eventually this could become a trait if it were
/// desirable
#[derive(Debug)]
#[allow(dead_code)]
pub struct SudburyDrmKms {
    /// Since input is currently optional, this module owns the session, but it will need to be
    /// shared for input/seat.
    #[cfg(feature = "libseat")]
    session: LibSeatSession,
    #[cfg(not(feature = "libseat"))]
    session: NullSession,

    /// EventLoop handle created by [`Sudbury`]
    loop_handle: calloop::LoopHandle<'static, GlobalState>,

    /// The DRM devices
    ///
    /// The DRM device provides an abstraction for GPU composition. Sudbury currently only supports
    /// a single DRM device.
    compositor: SudburyDrmCompositor,
    /// KMS functionality
    kms: SudburyKms,

    dmabuf_state: DmabufState,

    feedback: (DmabufFeedback, DmabufFeedback),
    /// Dispatcher for DRM events.
    token: calloop::RegistrationToken,

    /// Token for the idled composition
    ///
    /// When there are active clients, VBlank is driving repaints. However, when clients become
    /// inactive something needs to restart the compositors state machine. This token is [`Some`]
    /// when a client has committed a buffer after this idle period. Once the event loop idles, it
    /// will repaint and the state will go back to VBlank driven.
    ///
    /// FIXME: This probably needs to be a per output thing.
    idle: Option<calloop::Idle<'static>>,

    /// Token for current scheduled composting
    ///
    /// All composition is scheduled in a deferred fashion. There are at least 3 ways in which
    /// composition will be scheduled.
    /// 1. VBlank - some time after VBlank (giving clients a chance to run)
    /// 2. After idle - When all clients go idle, a commit request will schedule composition.
    /// 3. Mouse pointer movement
    ///
    /// Since the 3 events are asynchronous with respect to the origin, this token is used to
    /// avoid over or under compositing.
    compositing: Option<RegistrationToken>,
}

/// Calculates microseconds until compositor should try to composite based on scale.
///
/// * refresh - Current refresh in hertz
/// * divide - normalized split for when the compositor should begin its work.
/// * offset - Time since the actual last VBlank occurred
fn next_composite(refresh: usize, divide: f32, last_vblank: Option<Duration>) -> Duration {
    let refresh_ns = 1000000000000 / refresh;

    // If the vblank callback happened significantly later than the actual vblank, scheduling the
    // next composition must be adjusted accordingly.
    let offset: i32 = match last_vblank {
        Some(v) => {
            // Chop off seconds...
            let nanos_since = v.subsec_nanos() as usize;
            // If a frame was missed, adjust accordingly by the vsync period
            (nanos_since % refresh_ns) as i32
        }
        None => 0,
    };

    Duration::new(
        0,
        divide.mul_add(refresh_ns as f32, offset.checked_neg().unwrap() as f32) as u32,
    )
}

/// Number of microseconds until the estimated next VBlank
macro_rules! next_frame {
    ($refresh:expr) => {
        calloop::timer::TimeoutAction::ToDuration(next_composite($refresh, 1.0, None))
    };
}

impl SudburyDrmKms {
    /// Callback for Udev events.
    ///
    /// Dynamic GPU changes are currently unsupported. eGPU will change this need.
    #[instrument]
    fn udev_callback(event: UdevEvent, _meta: &mut (), _data: &mut GlobalState) {
        match event {
            UdevEvent::Added {
                device_id: _,
                path: _,
            } => {
                unimplemented!("dynamic GPU addition unsupported")
            }
            UdevEvent::Changed { device_id: _ } => todo!(),
            UdevEvent::Removed { device_id: _ } => {
                unimplemented!("dynamic GPU removal unsupported")
            }
        }
    }

    /// Send frame callbacks to elements in the [`Space`] for the given [`Output`]
    ///
    /// [`RenderElementStates`] contains whether or not the given element is occluded, and frame
    /// callbacks shall only be sent to the ones which have visible area.
    ///
    /// See: [`Window::send_frame`]
    /// See: [`Window::send_dmabuf_feedback`]
    #[profiling::function]
    fn send_client_feedback(
        &self,
        space: &Space<Window>,
        output: &Output,
        states: Option<&RenderElementStates>,
    ) {
        for window in space.elements() {
            // TODO: Consider a different `FnOnce` for selecting based on the slowest output
            if space.outputs_for_element(window).contains(output) {
                let throttle = frame_throttle();
                MONO_CLOCK.with(|clock| {
                    window.send_frame(
                        output,
                        clock.now(),
                        throttle,
                        utils::surface_primary_scanout_output,
                    );
                });
                if let Some(states) = states {
                    window.send_dmabuf_feedback(
                        output,
                        utils::surface_primary_scanout_output,
                        |surface, _| {
                            element::utils::select_dmabuf_feedback(
                                surface,
                                states,
                                &self.feedback.0,
                                &self.feedback.1,
                            )
                        },
                    )
                }
            }
            let Some(states) = states else {
                continue;
            };
            window.with_surfaces(|surface, surface_data| {
                utils::update_surface_primary_scanout_output(
                    surface,
                    output,
                    surface_data,
                    states,
                    element::default_primary_scanout_output_compare,
                );
            });
        }
    }

    /// Issue composition, presentation feedback, and swap
    ///
    /// This is the first order entry point and holistic function for composition. It will:
    /// 1. Use the GPU to composite all windows for the given [`Output`].
    /// 2. If the composition caused damage, queue a swap to the next buffer
    /// 3. Refresh internal Smithay state.
    ///
    /// The way that the profiler checks the fence means it is unsafe to have a profile span open
    /// while calling this function.
    fn __composite(&mut self, space: &mut Space<Window>, output: &Output) -> anyhow::Result<bool> {
        trace!("Render");
        #[cfg(feature = "profiler")]
        let mut fence_span = Some(tracing_tracy::client::span!("Composite"));

        // The lowest level function never shall mess with the token.
        assert!(self.compositing.is_none());

        let (damage, element_states, fence_fd) =
            self.compositor.composite(&mut self.kms, space, output)?;

        #[cfg(not(feature = "profiler"))]
        drop(fence_fd);

        if damage {
            let mut feedback = OutputPresentationFeedback::new(output);
            for window in space.elements() {
                window.take_presentation_feedback(
                    &mut feedback,
                    surface_primary_scanout_output,
                    |s, _| surface_presentation_feedback_flags_from_states(s, &element_states),
                );
            }
            trace!("Swapbuffer");
            self.kms.swap(output, feedback)?;
            #[cfg(feature = "profiler")]
            if let Some(fd) = fence_fd {
                let _ = self.loop_handle.insert_source(
                    calloop::generic::Generic::new(
                        fd,
                        calloop::Interest::READ,
                        calloop::Mode::OneShot,
                    ),
                    move |_, _, _| {
                        drop(fence_span.take());
                        Ok(calloop::PostAction::Remove)
                    },
                );
            }
            self.compositor
                .add_damage(output.downgrade(), element_states);
        }

        space.refresh();
        Ok(damage)
    }

    /// Schedule a VBlank for what would be the next VBlank.
    ///
    /// VBlank events are only generated when there is work (even though in hardware a VBLANK will
    /// occur every frame). For going into idle, a last VBlank is scheduled for the future even
    /// though DRM will not be generating one.
    ///
    /// Returns the registration token for the fake VBlank event along with the [`TimeoutAction`]
    #[instrument(name = "fake vblank", level = "trace", skip_all, fields(output = output.name()))]
    fn fake_vblank(
        sudbury: &mut Sudbury,
        output: &Output,
    ) -> (Option<RegistrationToken>, calloop::timer::TimeoutAction) {
        let refresh = output.current_mode().map_or(60, |m| m.refresh);
        let balance = composition_split();
        let fblank = Timer::from_duration(next_composite(refresh as usize, 1.0 - balance, None));
        trace!(
            "Scheduling fake vblank ({refresh}fps {:?})",
            next_composite(refresh as usize, 1.0 - balance, None)
        );
        let cloned_out = output.clone();

        // XXX: At this point there should be no idle events. If a client commits a new surface,
        // a new idle event will be scheduled unnecessarily, see [`schedule_idle_composition`]. The
        // result is an extra frame will be damaged and composited; so it should be fixed but it's
        // not a big deal.

        // Per above, clients are going to go idle now, so clear the idle "mutex"
        if let Some(idle) = sudbury.drm_kms.idle.take() {
            trace!("Idling compositor");
            idle.cancel();
        }

        // The fake VBlank scheduling had to have been called by a real event that tried to
        // composite. That code path will make sure the compositing token is cleaned up before
        // getting here.
        assert!(sudbury.drm_kms.compositing.is_none());

        // Schedule a composition on what would roughly be [roughly] the next VBlank.
        match sudbury.drm_kms.loop_handle.clone().insert_source(
            fblank,
            move |_, _, data: &mut GlobalState| {
                // When this fake VBlank event was scheduled, someone had to have the token.
                // Take it here and let a new one get setup
                data.sudbury.drm_kms.begin_busy();

                let sudbury = &mut data.sudbury;
                let space = sudbury.current_space_no();
                match sudbury
                    .drm_kms
                    .__composite(&mut sudbury.spaces[space], &cloned_out)
                {
                    // This will start normal VBlank driven compositing back up.
                    Ok(damaged) => {
                        trace!("Fake vblank damage: {damaged}");
                        // Since we're about to idle, it's important to send the last frame
                        // callbacks now.
                        // TODO: This shouldn't be necessary. The last vblank should have handled
                        // it, but it is necessary for WLCS
                        // FrameSubmission.post_one_frame_at_a_time. Look ino that.
                        for output in sudbury.spaces[space].outputs() {
                            let damage =
                                sudbury.drm_kms.compositor.take_damage(&output.downgrade());
                            sudbury.drm_kms.send_client_feedback(
                                &sudbury.spaces[space],
                                output,
                                damage.as_ref(),
                            );
                        }
                        calloop::timer::TimeoutAction::Drop
                    }
                    // Try again.
                    // TODO: Should limit number of tries.
                    Err(e) => {
                        error!("Failed to composite on fake vblank {e}");
                        next_frame!(refresh as usize)
                    }
                }
            },
        ) {
            // The last composition is done. Drop the timer and wait for new events.
            Ok(token) => (Some(token), calloop::timer::TimeoutAction::Drop),

            // An error occurred, try again
            Err(e) => {
                error!("{e}: Rescheduling fake vblank");
                (None, next_frame!(refresh as usize))
            }
        }
    }

    /// Try to composite the specified output
    #[instrument(level = "trace", skip_all)]
    fn composite(
        sudbury: &mut Sudbury,
        output: &WeakOutput,
        refresh: usize,
    ) -> calloop::timer::TimeoutAction {
        let span = debug_span!("composite");
        let _enter = span.enter();
        let Some(output) = output.upgrade() else {
            return next_frame!(refresh);
        };

        sudbury.drm_kms.begin_busy();

        // The most common case we enter this when one or more clients have causing damage
        // and this is called as part of the VBlank handling. This is the `else` case.
        //
        // The less common case is composition is scheduled while the compositor is idle.
        // This is called in response to [`core_interfaces::compositor::commit`]. Generally a
        // client will:
        // 1. attach buffer
        // 2. damage buffer
        // 3. request frame callback
        // 4. commit
        //
        // In that case, the scheduled composition will have damage and things will look much
        // the same as the common case (going to `else`). However, a frame callback may be
        // requested without an attached buffer/damage. This case will have no damage
        // reported. This case will go to the `if`. This turns out to be the case for
        // all/most XDG aware clients as they request a frame callback with no buffer
        // attached in order to obtain the XDG configure event. BUT, if the client does
        // attach a buffer before this handler runs (which is common), we're back to the
        // `else` case. Additionally a client's last commit will also potentially yield no
        // damage.
        //
        // In any of the above cases, `idle` acts as a mutex. It's "locked" (non-None) when the
        // compositor is currently idle and scheduling new work and it is released (Some) when the
        // last composition from a client yielded no damage.
        let space = sudbury.current_space_no();
        let (token, time) = if sudbury
            .drm_kms
            .__composite(&mut sudbury.spaces[space], &output)
            .map_or(false, |d| !d)
        {
            Self::fake_vblank(sudbury, &output)
        } else {
            // Timers are always one shot
            (None, calloop::timer::TimeoutAction::Drop)
        };

        sudbury.drm_kms.busy(token);
        time
    }

    /// Normal composition scheduling
    ///
    /// In order to provide clients and opportunity to render within the given raster period, the
    /// compositor is deferred by some amount, see XXX. This is the handler when the compositor can
    /// actually go ahead and composite.
    ///
    /// It should only be scheduled if there was damage in the last period and is generally going
    /// to be scheduled after a VBlank, or, if the compositor is entirely idle the `wl_compositor`
    /// commit request will.
    ///
    /// Fails if the composition fails to get scheduled
    fn defer_composition(&mut self, output: WeakOutput, offset: Duration) -> anyhow::Result<()> {
        // If there is a request to defer composition, but there is already a composition scheduled
        // just let that take its course
        if self.is_busy() {
            trace!("Skip deferred composition");
            return Ok(());
        }

        let balance = composition_split();
        let refresh = output
            .upgrade()
            .map_or(60, |o| o.current_mode().map_or(60, |m| m.refresh));
        let timer = Timer::from_duration(next_composite(refresh as usize, balance, Some(offset)));

        trace!(
            "Scheduling composition ({}fps {:?})",
            refresh / 1000,
            next_composite(refresh as usize, balance, Some(offset))
        );

        // Sets up composition for some point in the future. The token returned here is that of the
        // composition scheduled work, which may end up doing nothing and instead schedule a fake
        // VBlank in the future.
        let token = self
            .loop_handle
            .insert_source(timer, move |_, _, data: &mut GlobalState| {
                Self::composite(&mut data.sudbury, &output, refresh as usize)
            })
            .map_err(|e| e.error)?;

        self.busy(Some(token));

        Ok(())
    }

    /// Schedule composition for some future time for [`Output`]
    #[instrument(level = "trace", skip_all)]
    pub fn schedule_idle_composition(&mut self, output: &Output) {
        // If clients haven't been doing anything, there are no timers active for doing
        // composition. On the next idle, try to composite.
        //
        // Note that the idle token is set to NULL on the very next composition, but this event
        // will only trigger when everything is idle.
        let weak_out = output.downgrade();
        if self.idle.is_none() && !self.is_busy() {
            let span = debug_span!("idle");
            let _guard = span.enter();
            let idle = self.loop_handle.insert_idle(move |data: &mut GlobalState| {
                trace!("Waking up compositor");
                if let Err(e) = data
                    .sudbury
                    .drm_kms
                    .defer_composition(weak_out, Duration::ZERO)
                {
                    error!("Can't handle. Things may stop working {e}");
                }
            });
            self.idle.replace(idle);
        } else {
            trace!("Skipping composition, idle already scheduled");
        }
    }

    /// Schedule composition for the very next instant in time.
    ///
    /// Due to the way the event loop works, this is not guaranteed to run before any other event
    /// except an idle event.
    #[profiling::function]
    #[instrument(level = "trace", skip_all)]
    pub fn immediate_composition(&mut self, output: &Output) {
        // A composition is already scheduled.
        if self.compositing.is_some() {
            // TODO; A request was made to immediately composite but the current schedule may be
            // further in the future. This should probably replace that scheduling with a new,
            // shorter token
            return;
        }

        let weak_out = output.downgrade();
        _ = self
            .loop_handle
            .insert_source(Timer::immediate(), move |_, _, data| {
                // It's possible an event was already queued and executed in before the immediate
                // timer in which case, there's nothing to do.
                if data.sudbury.drm_kms.compositing.is_some() {
                    return calloop::timer::TimeoutAction::Drop;
                }
                if let Some(output) = weak_out.upgrade() {
                    let space = data.sudbury.current_space_no();
                    if let Err(e) = data
                        .sudbury
                        .drm_kms
                        .__composite(&mut data.sudbury.spaces[space], &output)
                    {
                        error!("Failed to composite {e}");
                    }
                }
                calloop::timer::TimeoutAction::Drop
            });
    }

    /// Handler for [`DrmEvent`]
    ///
    /// VBlank events will schedule composition for the next frame.
    fn drm_event(event: DrmEvent, meta: &mut Option<DrmEventMetadata>, data: &mut GlobalState) {
        match event {
            DrmEvent::VBlank(crtc) => {
                profiling::finish_frame!();
                trace!("VBLANK");

                let Some(output) = ({
                    let kms = &mut data.sudbury.drm_kms.kms;
                    kms.vblank(crtc, meta.as_mut().unwrap())
                }) else {
                    return;
                };

                let weak_out = output.downgrade();
                trace!("Issue frame callbacks");
                let spaces = &data.sudbury.spaces;
                let current = data.sudbury.current_space_no();
                let damage = data.sudbury.drm_kms.compositor.take_damage(&weak_out);
                data.sudbury.drm_kms.send_client_feedback(
                    &spaces[current],
                    &output,
                    damage.as_ref(),
                );

                // Attempt to find out if the vblank time is approximately the current time, or if
                // there was some delay in getting to this point. If there was a delay, the next
                // schedled composition will need to be adjusted accordingly.
                let shift = drm::duration_since(&meta.as_ref().unwrap().time);
                if !shift.is_zero() {
                    trace!(
                        "Adjusting time by {}μs",
                        drm::duration_since(&meta.as_ref().unwrap().time).as_micros()
                    );
                }

                // Schedule composition of the next frame
                match data.sudbury.drm_kms.defer_composition(weak_out, shift) {
                    Ok(_) => {
                        trace!("Scheduling composition at {:?}", meta.as_ref().unwrap())
                    }
                    Err(e) => {
                        error!("Failed to schedule composition {e}");
                        todo!("Need to issue frame callbacks");
                        // If the composition failed to schedule a client is likely to be waiting for a
                        // frame callback, so send them now even though we didn't successfully
                        // composite the last frame.
                    }
                };
            }
            DrmEvent::Error(e) => match e {
                DrmError::DrmMasterFailed => todo!(),
                DrmError::UnableToGetDeviceId(_) => todo!(),
                DrmError::DeviceInactive => todo!(),
                DrmError::ModeNotSuitable(_) => todo!(),
                DrmError::CrtcAlreadyInUse(_) => todo!(),
                DrmError::SurfaceWithoutConnectors(_) => todo!(),
                DrmError::PlaneNotCompatible(_, _) => todo!(),
                DrmError::NonPrimaryPlane(_) => todo!(),
                DrmError::NoSuitableEncoder {
                    connector: _,
                    crtc: _,
                } => todo!(),
                DrmError::UnknownProperty { handle: _, name: _ } => todo!(),
                DrmError::TestFailed(_) => todo!(),
                // The following should only happen with legacy modesetting which we disallow for
                // now.
                DrmError::NoPlane => panic!("How"),
                DrmError::NoFramebuffer(_) => panic!("Are We"),
                DrmError::UnsupportedPlaneConfiguration(_) => panic!("Here?"),
                DrmError::Access { .. } => todo!(),
            },
        };
    }

    #[cfg(feature = "libseat")]
    fn create_session(lh: &calloop::LoopHandle<'static, GlobalState>) -> LibSeatSession {
        if let Ok((session, notifier)) = LibSeatSession::new() {
            if let Err(e) = lh.insert_source(notifier, move |event, &mut (), _data| match event {
                PauseSession => todo!(),
                ActivateSession => todo!(),
            }) {
                panic!("Failed to insert notifier ({e})");
            }
            return session;
        }
        panic!("Failed to create session");
    }

    #[cfg(not(feature = "libseat"))]
    fn create_session(_lh: &calloop::LoopHandle<'static, GlobalState>) -> NullSession {
        NullSession::new()
    }

    pub fn new(
        display_handle: &DisplayHandle,
        loop_handle: &calloop::LoopHandle<'static, GlobalState>,
        path: PathBuf,
        space: &mut Space<Window>,
        pixman: bool,
    ) -> Self {
        let mut session = Self::create_session(loop_handle);

        let Ok(udev_be) = UdevBackend::new(session.seat()) else {
            panic!("Failed to create Udev backend");
        };

        let dispatch = calloop::Dispatcher::new(udev_be, Self::udev_callback);
        if let Err(e) = loop_handle.register_dispatcher(dispatch) {
            // FIXME?: This is potentially not fatal.
            panic!("Failed to create udev dispatcher {e}");
        };

        let (compositor, notifier) = SudburyDrmCompositor::new(&mut session, path, pixman);

        let kms = SudburyKms::new(&compositor, space);
        for o in kms.outputs() {
            o.create_global::<Sudbury>(display_handle);
        }

        // The main formats for dmabuf feedback will just be the renderer's formats. This should be
        // all formats that the GPU can sample from. The scanout preference tranche will be the
        // same, but also can add the combination of all supported plane formats since that
        // represents all formats that can be scanned out.
        //
        // Note that since render node and multigpu is not supported, the device for the
        // tranches/feedback are always the same.
        let render_builder = DmabufFeedbackBuilder::new(
            compositor.drm_device().device_id(),
            compositor.renderer().texture_formats(),
        );
        let scanout_feedback = kms.dmabuf_feedback(render_builder.clone());
        let render_feedback = render_builder.build().unwrap();
        let mut dmabuf_state = DmabufState::new();

        let _global = dmabuf_state
            .create_global_with_default_feedback::<Sudbury>(display_handle, &render_feedback);

        let Ok(token) = loop_handle.insert_source(notifier, SudburyDrmKms::drm_event) else {
            panic!("Failed to enable DRM event source");
        };

        Self {
            session,
            loop_handle: loop_handle.clone(),
            compositor,
            kms,
            dmabuf_state,
            feedback: (render_feedback, scanout_feedback),
            token,
            compositing: None,
            idle: None,
        }
    }

    #[cfg(feature = "libseat")]
    pub fn session(&self) -> &LibSeatSession {
        &self.session
    }

    #[cfg(not(feature = "libseat"))]
    pub fn session(&self) -> &NullSession {
        &self.session
    }

    /// Returns the outputs of this [`SudburyDrmKms`].
    pub fn outputs(&self) -> Vec<Output> {
        self.kms.outputs()
    }

    /// Import a dmabuf as an EGL texture for use by the renderer.
    ///
    /// # Errors
    ///
    /// This function will return an error if .
    pub fn import_dmabuf(&mut self, dmabuf: &Dmabuf) -> anyhow::Result<()> {
        self.compositor
            .renderer_mut()
            .import_dmabuf(dmabuf)
            .with_context(|| "Bad dmabuf import")?;
        Ok(())
    }

    pub fn set_input(&mut self, input: Rc<RefCell<SudburyInput>>) {
        self.compositor
            .renderer_mut()
            .set_icon(&mut input.borrow_mut().pointer);
        //        input.borrow_mut().pointer.icon(self.dev.renderer_mut());
        self.compositor.set_input(input);
    }

    #[cfg(feature = "fps")]
    pub fn fps(&self) -> (f64, f64, f64) {
        (
            self.kms.fps().avg(),
            self.kms.fps().min(),
            self.kms.fps().max(),
        )
    }

    /// Toggle whether or not the compositor will tint blitted frames
    pub fn tint(&mut self) {
        for compositor in self.kms.compositors_mut() {
            info!("Tinting");
            let mut flags = compositor.debug_flags();
            flags.toggle(DebugFlags::TINT);
            compositor.set_debug_flags(flags);
        }

        for output in self.kms.outputs() {
            self.immediate_composition(&output)
        }
    }

    /// Prepares state for new composition
    ///
    /// # Panics
    ///
    /// Panics if compositing token state has been corrupted.
    fn begin_busy(&mut self) {
        assert!(self.compositing.take().is_some());
    }

    /// Sets the new render token (or None)
    ///
    /// # Panics
    ///
    /// Panics if compositing token state has been corrupted.
    fn busy(&mut self, token: Option<RegistrationToken>) {
        assert!(self.compositing.is_none());
        self.compositing = token;
    }

    pub fn is_busy(&self) -> bool {
        self.compositing.is_some()
    }

    pub fn dmabuf_state_mut(&mut self) -> &mut DmabufState {
        &mut self.dmabuf_state
    }
}
